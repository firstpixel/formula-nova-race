//
//  AICarTopDownSilly.h
//  SillyRace
//
//  Created by Gil Beyruth on 6/14/11.
//  Copyright 2011 Sapus Media. All rights reserved.
//
#import "AI.h"
#import "SimpleAudioEngine.h"

// forward declarations
@class GameNode;
@class BodyNode;

// The car
@interface Aicartopdown : AI {
    
	// weak ref
	b2Body	*leftWheel_;
	b2Body	*rightWheel_;
	b2Body	*leftRearWheel_;
	b2Body	*rightRearWheel_;
    b2Body	*carBody;
    
	b2RevoluteJoint *leftJoint_;
	b2RevoluteJoint *rightJoint_;
	
	float	engineSpeed_;
	float32	steeringAngle_;
    
    BOOL isCrashing;
    BOOL isCrashed;
    
    float  maxVelocity,maxVelocityDefault, maxVelocityCrashed,maxSteerAngle,  maxVelocityTurbo, maxVelocityTurboCrashed, steerSpeedLowVelocity, steerSpeedMediumVelocity, steerSpeedHighVelocity, horsePowers,accelerationFactor;
    int   carType,nextWayPoint,myLap,racePosition,crashPoints;
    int carid;
    NSString *carColor;
    float lw;
    float rw;
    float lrw;
    float rrw;
    
    CCSpriteFrame *frame;
}

@property (nonatomic, readwrite) float maxVelocity,maxVelocityDefault, maxVelocityCrashed,maxSteerAngle,  maxVelocityTurbo, maxVelocityTurboCrashed, steerSpeedLowVelocity, steerSpeedMediumVelocity, steerSpeedHighVelocity, horsePowers;

@property (nonatomic, readwrite) int carType,nextWayPoint,myLap,carid,racePosition,crashPoints,totalCrashesAllowed;

-(void)skidmarkMefrt:(BOOL)frt flt:(BOOL)flt rrt:(BOOL)rrt rlt:(BOOL)rlt;
-(float)getVelocity;
-(void) carFixed;
-(void) carCrashed;

-(void) setParameters:(NSDictionary*)params;
-(CGFloat)nextWayPointDistance;
@end
