//
//  AICarTopDownSilly.mm
//  SillyRace
//
//  Created by Gil Beyruth on 6/14/11.
//  Copyright 2011 Sapus Media. All rights reserved.
//


#import "AICarTopDown.h"
#import "BodyNode.h"
#import "GameNode.h"
#import "GlobalSingleton.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#import "WayPoint.h"




#pragma mark -
#pragma mark Aicartopdown

// Forces & Impulses

// Modify this value to create a bigger/smaller car
const float CAR_SCALE = 0.2f;

const b2Vec2 leftRearWheelPosition = b2Vec2(-1.2f * CAR_SCALE, 1.9f * CAR_SCALE);
const b2Vec2 rightRearWheelPosition = b2Vec2(1.2f * CAR_SCALE, 1.9f * CAR_SCALE);
const b2Vec2 leftFrontWheelPosition = b2Vec2(-1.2f * CAR_SCALE, -1.9f * CAR_SCALE);
const b2Vec2 rightFrontWheelPosition = b2Vec2(1.2f * CAR_SCALE, -1.9f * CAR_SCALE);
//3.0
/*const float MAX_STEER_ANGLE = (float)M_PI /3.0f ;
//2.5
float STEER_SPEED = 2.1f;
float STEER_MEDIUM_SPEED_VELOCITY = 1.2f;
float STEER_HIGH_SPEED_VELOCITY = 0.3f;

const float HORSEPOWERS = 8 * CAR_SCALE;
*/

//
// HeroCarTopDown: The main character of the game.
//

@implementation Aicartopdown
@synthesize maxVelocity, maxVelocityDefault, maxVelocityCrashed, maxVelocityTurbo, maxVelocityTurboCrashed, maxSteerAngle,  steerSpeedLowVelocity, steerSpeedMediumVelocity, steerSpeedHighVelocity, horsePowers;
@synthesize carType,nextWayPoint,myLap,carid,racePosition,crashPoints,totalCrashesAllowed;
-(id) initWithBody:(b2Body*)body game:(GameNode*)aGame
{
	if( (self=[super initWithBody:body game:aGame] ) ) {
        carBody = body;
        //
		// Set up vars
		//
        myLap = 0;
        nextWayPoint = 0;
        
        //
		// Set up the right texture
		//
        int carColorRand = arc4random() % 5;
        switch(carColorRand){
            case 0:
                carColor = @"red";
                break;
            case 1:
                carColor = @"blue";
                break;
            case 2:
                carColor = @"gray";
                break;
            case 3:
                carColor = @"white";
                break;
            case 4:
                carColor = @"yellow";
                break;
        }

		
		// Set the default frame from singleton class
        GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
        if([mySingleton gameLevel]==0){
            carType = arc4random() % 2;
        }else if([mySingleton gameLevel]==1){
            carType = arc4random() % 4;
        }else if([mySingleton gameLevel]==2){
            carType = arc4random() % 7;
        }
        //MAX SPEED
        float maxSteerAngleLevel1 =(float)M_PI /2.5f;
        float maxSteerAngleLevel2 = (float)M_PI /2.5f;
        float maxSteerAngleLevel3 = (float)M_PI /2.6f;
        float maxSteerAngleLevel4 = (float)M_PI /2.6f;
        
        float steerSpeedLowVelocityLevel1 = 2.6f;
        float steerSpeedLowVelocityLevel2 = 2.1f;
        float steerSpeedLowVelocityLevel3 = 1.9f;
        
        float steerSpeedMediumVelocityLevel1 = 1.8f;
        float steerSpeedMediumVelocityLevel2 = 1.7f;
        float steerSpeedMediumVelocityLevel3 = 1.6f;
        
        float steerSpeedHighVelocityLevel1 = 1.6f;
        float steerSpeedHighVelocityLevel2 = 1.4f;
        float steerSpeedHighVelocityLevel3 = 1.2f;
        
        float horsePowersLevel1 = 11 * CAR_SCALE;
        float horsePowersLevel2 = 12 * CAR_SCALE;
        float horsePowersLevel3 = 13 * CAR_SCALE;
        float horsePowersLevel4 = 14 * CAR_SCALE;
        
        switch(carType){
            case 0:
                //MUSTANG
                totalCrashesAllowed = 4;
                maxSteerAngle = maxSteerAngleLevel1;
                
                maxVelocityDefault = 165.0f;
                maxVelocityCrashed = 155.0f;
                maxVelocityTurbo = 185.0f;
                maxVelocityTurboCrashed = 165.0f;
                accelerationFactor = 7;
                
                steerSpeedLowVelocity = steerSpeedLowVelocityLevel1;
                steerSpeedMediumVelocity = steerSpeedMediumVelocityLevel1;
                steerSpeedHighVelocity = steerSpeedHighVelocityLevel1;
                
                horsePowers = horsePowersLevel1;
                
                break;
            case 1:
                //CAMARO
                totalCrashesAllowed = 3;
                maxSteerAngle = maxSteerAngleLevel2;
                
                maxVelocityDefault = 165.0f;
                maxVelocityCrashed = 160.0f;
                maxVelocityTurbo = 190.0f;
                maxVelocityTurboCrashed = 165.0f;
                accelerationFactor = 6;
                
                steerSpeedLowVelocity = steerSpeedLowVelocityLevel2;
                steerSpeedMediumVelocity = steerSpeedMediumVelocityLevel2;
                steerSpeedHighVelocity = steerSpeedHighVelocityLevel2;
                horsePowers = horsePowersLevel2;
                
                break;
            case 2:
                //P CARRERA
                totalCrashesAllowed = 3;
                maxSteerAngle = maxSteerAngleLevel3;
                
                maxVelocityDefault = 170.0f;
                maxVelocityCrashed = 165.0f;
                maxVelocityTurbo = 200.0f;
                maxVelocityTurboCrashed = 170.0f;
                accelerationFactor = 5;
                
                steerSpeedLowVelocity = steerSpeedLowVelocityLevel1;
                steerSpeedMediumVelocity = steerSpeedMediumVelocityLevel1;
                steerSpeedHighVelocity = steerSpeedHighVelocityLevel1;
                horsePowers = horsePowersLevel3;
                break;
                
            case 3:
                //VIPER
                totalCrashesAllowed = 2;
                maxSteerAngle = maxSteerAngleLevel4;
                
                maxVelocityDefault = 175.0f;
                maxVelocityCrashed = 170.0f;
                maxVelocityTurbo = 204.0f;
                maxVelocityTurboCrashed = 175.0f;
                accelerationFactor = 5;
                
                steerSpeedLowVelocity = steerSpeedLowVelocityLevel2;
                steerSpeedMediumVelocity = steerSpeedMediumVelocityLevel2;
                steerSpeedHighVelocity = steerSpeedHighVelocityLevel2;
                horsePowers = horsePowersLevel4;
                break;
            case 4:
                //FERRARI
                totalCrashesAllowed = 2;
                maxSteerAngle = maxSteerAngleLevel4;
                
                maxVelocityDefault = 185.0f;
                maxVelocityCrashed = 170.0f;
                maxVelocityTurbo = 210.0f;
                maxVelocityTurboCrashed = 185.0f;
                accelerationFactor = 4;
                
                steerSpeedLowVelocity = steerSpeedLowVelocityLevel3;
                steerSpeedMediumVelocity = steerSpeedMediumVelocityLevel3;
                steerSpeedHighVelocity = steerSpeedHighVelocityLevel3;
                horsePowers = horsePowersLevel3;
                break;
            case 5:
                //PORSCHE TURBO
                totalCrashesAllowed = 3;
                maxSteerAngle = maxSteerAngleLevel4;
                
                maxVelocityDefault = 185.0f;
                maxVelocityCrashed = 160.0f;
                maxVelocityTurbo = 212.0f;
                maxVelocityTurboCrashed = 185.0f;
                accelerationFactor = 3;
                
                steerSpeedLowVelocity = steerSpeedLowVelocityLevel2;
                steerSpeedMediumVelocity = steerSpeedMediumVelocityLevel2;
                steerSpeedHighVelocity = steerSpeedHighVelocityLevel2;
                horsePowers = horsePowersLevel4;
                break;
            case 6:
                //LAMBORGHINI DIABLO
                totalCrashesAllowed = 2;
                maxSteerAngle = maxSteerAngleLevel4;
                
                maxVelocityDefault = 188.0f;
                maxVelocityCrashed = 170.0f;
                maxVelocityTurbo = 215.0f;
                maxVelocityTurboCrashed = 188.0f;
                accelerationFactor = 3;
                
                steerSpeedLowVelocity = steerSpeedLowVelocityLevel3;
                steerSpeedMediumVelocity = steerSpeedMediumVelocityLevel3;
                steerSpeedHighVelocity = steerSpeedHighVelocityLevel3;
                horsePowers = horsePowersLevel4;
                break;
        }
        maxVelocity = maxVelocityDefault;
        
        frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"car%i_%@.png",carType,carColor]];
        [self setDisplayFrame:frame];
        
		preferredParent_ = BN_PREFERRED_PARENT_SPRITES_PNG;
        
        //		self.isTouchable = YES;
		//
		// box2d stuff: Create the "correct" fixture
		//
		// 1. destroy already created fixtures
		[self destroyAllFixturesFromBody:body];
		
		//
		// Car model based on:
		// http://www.emanueleferonato.com/2009/04/06/two-ways-to-make-box2d-cars/
		//
        
		//
		// Bodies
		//
		carBody->SetLinearDamping(1);
		carBody->SetAngularDamping(1);
		carBody->SetType(b2_dynamicBody);
		
		float originalAngle = carBody->GetAngle();
		  
		carBody->SetTransform( carBody->GetPosition(), 0 );
        
		//left wheel
		b2BodyDef leftWheelDef;
		leftWheelDef.position = carBody->GetPosition() + leftFrontWheelPosition;
        //		leftWheelDef.angle = body->GetAngle();
		leftWheel_ = world_->CreateBody(&leftWheelDef);
		leftWheel_->SetType(b2_dynamicBody);
        
        
		//right wheel
		b2BodyDef rightWheelDef;
		rightWheelDef.position = carBody->GetPosition() + rightFrontWheelPosition;
        //		rightWheelDef.angle = body->GetAngle();
		rightWheel_ = world_->CreateBody(&rightWheelDef);
		rightWheel_->SetType(b2_dynamicBody);
		
		//left rear wheel
		b2BodyDef leftRearWheelDef;
		leftRearWheelDef.position = carBody->GetPosition() + leftRearWheelPosition;
        //		leftRearWheelDef.angle = body->GetAngle();
		leftRearWheel_ = world_->CreateBody(&leftRearWheelDef);
		leftRearWheel_->SetType(b2_dynamicBody);
        
		
		//right rear wheel
		b2BodyDef rightRearWheelDef;
		rightRearWheelDef.position = carBody->GetPosition() + rightRearWheelPosition;
        //		rightRearWheelDef.angle = body->GetAngle();
		rightRearWheel_ = world_->CreateBody(&rightRearWheelDef);
		rightRearWheel_->SetType(b2_dynamicBody);
		
		
		//
		// Shapes
		//
		
		// car shape
		b2PolygonShape boxDef;
		boxDef.SetAsBox(1.2f * CAR_SCALE, 2.5f * CAR_SCALE);
		carBody->CreateFixture(&boxDef, 1);
        
        b2MassData data;
        carBody->GetMassData(&data);
        
        b2CircleShape circleBDef;
        circleBDef.m_radius = 0.25f;
        circleBDef.m_p.Set(0.0f, 0.34f);
        carBody->CreateFixture(&circleBDef, 1);
        
        b2CircleShape circleFDef;
        circleFDef.m_radius = 0.25f;
        circleFDef.m_p.Set(0.0f, -0.34f);
        carBody->CreateFixture(&circleFDef, 1);
        carBody->SetMassData( &data );
		
		//Left Wheel shape
		b2PolygonShape leftWheelShapeDef;
		leftWheelShapeDef.SetAsBox(0.2f * CAR_SCALE, 0.5f * CAR_SCALE);
		leftWheel_->CreateFixture(&leftWheelShapeDef, 1);
		
		//Right Wheel shape
		b2PolygonShape rightWheelShapeDef;
		rightWheelShapeDef.SetAsBox(0.2f * CAR_SCALE, 0.5f * CAR_SCALE);
		rightWheel_->CreateFixture( &rightWheelShapeDef, 1);
		
		//Left Wheel shape
		b2PolygonShape leftRearWheelShapeDef;
		leftRearWheelShapeDef.SetAsBox(0.2f * CAR_SCALE, 0.5f * CAR_SCALE);
		leftRearWheel_->CreateFixture(&leftRearWheelShapeDef,1);
		
		//Right Wheel shape
		b2PolygonShape rightRearWheelShapeDef;
		rightRearWheelShapeDef.SetAsBox(0.2f * CAR_SCALE, 0.5f * CAR_SCALE);
		rightRearWheel_->CreateFixture(&rightRearWheelShapeDef,1);
        
		
		//
		// Joints
		//
		b2RevoluteJointDef leftJointDef;
		leftJointDef.Initialize(carBody, leftWheel_, leftWheel_->GetWorldCenter());
		leftJointDef.enableMotor = true;
        
        //100
		leftJointDef.maxMotorTorque = 40;
		
		b2RevoluteJointDef rightJointDef;
		rightJointDef.Initialize(carBody, rightWheel_, rightWheel_->GetWorldCenter());
		rightJointDef.enableMotor = true;
		//100
        rightJointDef.maxMotorTorque = 40;
		
		leftJoint_ = (b2RevoluteJoint*) world_->CreateJoint(&leftJointDef);
		rightJoint_ = (b2RevoluteJoint*) world_->CreateJoint(&rightJointDef);
		
		b2PrismaticJointDef leftRearJointDef;
		leftRearJointDef.Initialize(carBody, leftRearWheel_, leftRearWheel_->GetWorldCenter(), b2Vec2(1,0));
		leftRearJointDef.enableLimit = true;
		leftRearJointDef.lowerTranslation = leftRearJointDef.upperTranslation = 0;
		
		b2PrismaticJointDef rightRearJointDef;
		rightRearJointDef.Initialize(carBody, rightRearWheel_, rightRearWheel_->GetWorldCenter(), b2Vec2(1,0));
		rightRearJointDef.enableLimit = true;
		rightRearJointDef.lowerTranslation = rightRearJointDef.upperTranslation = 0;
		
		world_->CreateJoint(&leftRearJointDef);
		world_->CreateJoint(&rightRearJointDef);
		
		
		// steering angle, speed
		steeringAngle_ = 0;
		engineSpeed_ = 0;
		
		carBody->SetTransform( carBody->GetPosition(), originalAngle );
        
        
        isCrashing = NO;
        isCrashed = NO;
		
        crashPoints = 0;
        
    }
	return self;
}
-(CGFloat)nextWayPointDistance
{
    NSValue* p = [game_ getWayPointByTag:nextWayPoint];
    CGPoint point = [p CGPointValue];
    return ccpDistance(CGPointMake(carBody->GetPosition().x,carBody->GetPosition().y), point);
}
//
//    [CAR]________
//      \   |.|
//       \    |
//        \   |
//         \  |
//          \ |
//           \| NEXTWAYPOINT
//

-(float32) angleDifference:(float32)arg1 arg2:(float32)arg2
{
    int argu1 = CC_RADIANS_TO_DEGREES(arg1*-1);
    int argu2 = CC_RADIANS_TO_DEGREES(arg2*-1);
    
    return CC_DEGREES_TO_RADIANS((((argu2 - argu1 + 540) % 360) - 180));

}


-(void) setParameters:(NSDictionary *)params
{
	[super setParameters:params];
    carid = [[params objectForKey:@"carid"] intValue];
    if(carid==1){
        game_.ai1=self;
    }else if(carid==2){
        game_.ai2=self;
    }else if(carid==3){
        [game_ setAi3:self];
    }else if(carid==4){
        [game_ setAi4:self];
    }else if(carid==5){
        [game_ setAi5:self];
        
    }
}

-(float32) getAngleToNextWayPoint:(int)wp
{
    NSValue* p = [game_ getWayPointByTag:wp];
    CGPoint point = [p CGPointValue];
   //randomize the spot so cars dont drive all on same path
   // CCLOG(@"point x ,%f  : y ,%f",point.x,point.y);
    float randx =  (arc4random()%2);
    float px = nil;
    if(point.x>=0)px = point.x + randx;
    if(point.x<0)px = point.x - randx;
    
    float randy =  (arc4random()%2);
    float py = nil;
    if(point.y>=0)py = point.y + randy;
    if(point.y<0)py = point.y - randy;
    
    CGPoint pointNew = CGPointMake(px ,py);
    float angleRadians = atan2( ( carBody->GetPosition().y - pointNew.y ), ( carBody->GetPosition().x - pointNew.x ) )-M_PI/2;

    return angleRadians;
}


-(void)cleanCrashing{
    isCrashing = NO;
}

-(void)carCrashed{
    if(isCrashing == NO){
        isCrashing = YES;

        id action = [CCCallFunc actionWithTarget:self selector:@selector(cleanCrashing)];
        
        id action2 = [CCCallFunc actionWithTarget:self selector:@selector(carFixed)];
        
        //create sequence to have a delay, and then run the callblock action:
        id sequence = [CCSequence actions:[CCDelayTime actionWithDuration:3.0],action, [CCDelayTime actionWithDuration:30.0],action2,nil];
        
        //run action:
        [self runAction:sequence];
        
        //[[SimpleAudioEngine sharedEngine] playEffect: @"carhit.mp3"];
        
        crashPoints++;
        if(crashPoints==3){
            isCrashed = YES;
          //  [game_ initExplosionEffect];
            maxVelocity = maxVelocityCrashed;
            frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"car%i_crashed_%@.png",carType,carColor]];
            [self setDisplayFrame:frame];
        }
    }
}

-(void)carFixed{
    isCrashed = NO;    
    crashPoints = 0;
    maxVelocity = maxVelocityDefault;
    frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"car%i_%@.png",carType,carColor]];
    [self setDisplayFrame:frame];
}

//This function applies a "friction" in a direction orthogonal to the body's axis.
-(void) killOrthogonalVelocity:(b2Body*)targetBody
{
    b2Vec2 localPoint = b2Vec2(0,0);
	b2Vec2 velocity = targetBody->GetLinearVelocityFromLocalPoint(localPoint);
	b2Vec2 sidewaysAxis = targetBody->GetTransform().q.GetYAxis();
	sidewaysAxis *= b2Dot(velocity,sidewaysAxis);
	targetBody->SetLinearVelocity(sidewaysAxis);//targetBody.GetWorldPoint(localPoint));
 
}

-(void)skidmarkMefrt:(BOOL)frt flt:(BOOL)flt rrt:(BOOL)rrt rlt:(BOOL)rlt 
{
    
    if(frt) [game_ addSkidMarkAt:CGPointMake(rightWheel_->GetPosition().x*kPhysicsPTMRatio,rightWheel_->GetPosition().y*kPhysicsPTMRatio) angle:CC_RADIANS_TO_DEGREES(rightWheel_->GetAngle()*-1)];

    if(flt) [game_ addSkidMarkAt:CGPointMake(leftWheel_->GetPosition().x*kPhysicsPTMRatio,leftWheel_->GetPosition().y*kPhysicsPTMRatio) angle:CC_RADIANS_TO_DEGREES(leftWheel_->GetAngle()*-1)];
   
    if(rrt) [game_ addSkidMarkAt:CGPointMake(rightRearWheel_->GetPosition().x*kPhysicsPTMRatio,rightRearWheel_->GetPosition().y*kPhysicsPTMRatio) angle:CC_RADIANS_TO_DEGREES(rightRearWheel_->GetAngle()*-1)];
   
    if(rlt) [game_ addSkidMarkAt:CGPointMake(leftRearWheel_->GetPosition().x*kPhysicsPTMRatio,leftRearWheel_->GetPosition().y*kPhysicsPTMRatio) angle:CC_RADIANS_TO_DEGREES(leftRearWheel_->GetAngle()*-1)];
    
    if(rrt)[game_ addSmokeRightSkidmark:CGPointMake(rightRearWheel_->GetPosition().x*kPhysicsPTMRatio,rightRearWheel_->GetPosition().y*kPhysicsPTMRatio) angle:CC_RADIANS_TO_DEGREES(rightRearWheel_->GetAngle()*-1)];
 
    if(rlt)[game_ addSmokeLeftSkidmark:CGPointMake(leftRearWheel_->GetPosition().x*kPhysicsPTMRatio,leftRearWheel_->GetPosition().y*kPhysicsPTMRatio) angle:CC_RADIANS_TO_DEGREES(leftRearWheel_->GetAngle()*-1)];
    

}

-(float) getVelocity{
    float velo = carBody->GetLinearVelocity().Length();
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    int multiplyer = 28;
    if([mySingleton gameLevel]==0){
        multiplyer = 33 + (arc4random() % 4);
    }else if([mySingleton gameLevel]==1){
        multiplyer = 28 + (arc4random() % 3);
    }else if([mySingleton gameLevel]==2){
        multiplyer = 26;
    }
    
    return velo*multiplyer;
}

-(void) update:(ccTime)dt
{
   [super update:dt];
    
	[self killOrthogonalVelocity:leftWheel_];
	[self killOrthogonalVelocity:rightWheel_];
	[self killOrthogonalVelocity:leftRearWheel_];
	[self killOrthogonalVelocity:rightRearWheel_];
    GameState state = [game_ gameState];
    if( state == kGameStatePlaying ) {
        engineSpeed_ = -1.0f * horsePowers;
    }else{
        engineSpeed_ = 0;
    }
	
    
    //Driving
	//b2Vec2 ldirection = leftWheel_->GetTransform().R.col2;
	//ldirection *= engineSpeed_;
	//b2Vec2 rdirection = rightWheel_->GetTransform().R.col2;
	//rdirection *= engineSpeed_;
    
    b2Vec2 ldirection = leftWheel_->GetTransform().q.GetYAxis();
	b2Vec2 rdirection = rightWheel_->GetTransform().q.GetYAxis();
    float accelerator = accelerationFactor-([self getVelocity]/10);
    if(accelerator>3)accelerator=3;
    if(accelerator>1){
        ldirection *= engineSpeed_/accelerator;
        rdirection *= engineSpeed_/accelerator;
    }else{
        ldirection *= engineSpeed_;
        rdirection *= engineSpeed_;
        
    }
    
    
    if([self getVelocity]<maxVelocity){
        leftWheel_->ApplyForce(ldirection, leftWheel_->GetPosition());
        rightWheel_->ApplyForce(rdirection, rightWheel_->GetPosition());
	}
    
     
    float32 carToPointAngle = [self getAngleToNextWayPoint:nextWayPoint];
   
	//[game_ debugAngle:carToPointAngle];
    
    
    
    steeringAngle_ = (carToPointAngle - (leftWheel_->GetAngle()));
    
    while(steeringAngle_>M_PI/2){
        steeringAngle_ = steeringAngle_-M_PI*2;
    }
    while(steeringAngle_<-M_PI/2){
        steeringAngle_ = steeringAngle_+M_PI*2;
    }
     
     
    if(steeringAngle_>0){ 
        steeringAngle_=1.0f * maxSteerAngle;
        
    }else if(steeringAngle_<0){
        steeringAngle_=-1.0f * maxSteerAngle;
        
    }else{
        steeringAngle_=0.0f;
        
    }
      //steeringAngle_=-0.0f;
    if(steeringAngle_!=-0.0f){
        //Steering
        if([self getVelocity]>120){
            float mspeed = steeringAngle_ - leftJoint_->GetJointAngle();
            leftJoint_->SetMotorSpeed(mspeed * steerSpeedHighVelocity);
            
            mspeed = steeringAngle_ - rightJoint_->GetJointAngle();
            rightJoint_->SetMotorSpeed(mspeed * steerSpeedHighVelocity);
        }else if([self getVelocity]>20 && [self getVelocity]<=120){
            float mspeed = steeringAngle_ - leftJoint_->GetJointAngle();
            leftJoint_->SetMotorSpeed(mspeed * steerSpeedMediumVelocity);
            
            mspeed = steeringAngle_ - rightJoint_->GetJointAngle();
            rightJoint_->SetMotorSpeed(mspeed * steerSpeedMediumVelocity);
        }else {
            float mspeed = steeringAngle_ - leftJoint_->GetJointAngle();
            leftJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
            
            mspeed = steeringAngle_ - rightJoint_->GetJointAngle();
            rightJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
        }
    }else{
        
        //Return Steering
        float mspeed = - leftJoint_->GetJointAngle()*6;
        leftJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
        
        mspeed = - rightJoint_->GetJointAngle()*6;
        rightJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
        
    }
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    if(![[mySingleton deviceType] isEqualToString:@"oldIphone"]){
        if([self getVelocity]>20){
            float speedDiff = (2.8 + (maxVelocity/40)) - ([self getVelocity]/40);
            //SKIDMARK ME
            //FRONT LEFT TIRE
            BOOL flt=(floor(lw*1000) != floor(leftWheel_->GetAngle()*1000) && (floor(lw*100)-floor(leftWheel_->GetAngle()*100)) > 20 )
            || std::abs(((leftWheel_->GetAngle()*-1)- (carBody->GetAngle()*-1))*10) > speedDiff ?YES:NO;
            //FRONT RIGHT TIRE
            BOOL frt=(floor(rw*1000) != floor(rightWheel_->GetAngle()*1000) && (floor(rw*100) - floor(rightWheel_->GetAngle()*100) >10 || floor(rw*100) - floor(rightWheel_->GetAngle()*100) <-20) )
            || std::abs(((rightWheel_->GetAngle()*-1)- (carBody->GetAngle()*-1))*10) > speedDiff?YES:NO;
            //REAR LEFT TIRE
            BOOL rlt=(floor(lrw*1000) != floor(leftRearWheel_->GetAngle()*1000) && (floor(lrw*100) - floor(leftRearWheel_->GetAngle()*100) >20 || floor(lrw*100) - floor(leftRearWheel_->GetAngle()*100) <-20))
            || std::abs(((leftRearWheel_->GetAngle()*-1)- (carBody->GetAngle()*-1))*10) > speedDiff?YES:NO;
            //REAR RIGHT TIRE
            BOOL rrt=(floor(rrw*1000) != floor(rightRearWheel_->GetAngle()*1000) && (floor(rrw*100) - floor(rightRearWheel_->GetAngle()*100) >20 || floor(rrw*100) - floor(rightRearWheel_->GetAngle()*100) <-20))
            || std::abs(((rightRearWheel_->GetAngle()*-1)- (carBody->GetAngle()*-1))*10) > speedDiff?YES:NO;
            
            
            [self skidmarkMefrt:frt flt:flt rrt:rrt rlt:rlt];
            lw = leftWheel_->GetAngle();
            rw = rightWheel_->GetAngle();
            lrw = leftRearWheel_->GetAngle();
            rrw = rightRearWheel_->GetAngle();
        }
    }
}

#pragma mark HeroCar - Movements

-(void) move:(CGPoint)direction
{
    engineSpeed_ = -1 * horsePowers;
}

-(void) teleportTo:(CGPoint)point
{	
}

-(void) onGameOver:(BOOL)winner
{
}
-(void) touchedByHero{

    [[game_ hero] carCrashed];
    [self carCrashed];
    
}

-(void) touchedByAi{

    [self carCrashed];
    
}


//OVERIDE

-(void) updateCollisions
{
	isTouchingLadder_ = NO;
    
	// Traverse the contact results.
	int found = 0;
	for (int32 i = 0; i < kMaxContactPointsAi && found < contactPointCount_; i++)
	{
		ContactPointAi* point = contactPoints_ + i;
		b2Fixture *otherFixture = point->otherFixture;
		
		if( otherFixture ) {
            
			found++;
			b2Body* body = otherFixture->GetBody();
			
			BodyNode *node = (BodyNode*) body->GetUserData();

			
			
			//else if( [node respondsToSelector:@selector(touchedByHero)] )
			//	[node performSelector:@selector(touchedByHero)];
            if( [node respondsToSelector:@selector(touchedByAiWithB2Body:)] ){
				[node performSelector:@selector(touchedByAiWithB2Body:) withObject:(id)self];
            } else if( [node respondsToSelector:@selector(touchedByAi)] ){
				[node performSelector:@selector(touchedByAi)];
            }
		}
	}	
}

@end

