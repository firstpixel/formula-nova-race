//
//  GhostCar.h
//  Formula Nova Race
//
//  Created by Gil Beyruth on 5/25/16.
//  Copyright © 2016 Gil Beyruth. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GhostCar : NSObject {
    NSMutableArray* ghostArray;
    NSMutableArray* ghostLapArray;
    NSMutableArray* ghostBestLap;
   // NSUInteger ghostLap[120];  // c-style array
    
    int   carType,trackType,myLap,racePosition;
    
    NSString *carColor;
    
    NSString* selectedCar;
    NSString* selectedTrack;
    
    NSMutableString* player1Name;
}

@property (nonatomic, retain) NSMutableArray *ghostArray;
@property (nonatomic, retain) NSMutableArray *ghostLapArray;
@property (nonatomic, retain) NSMutableArray *ghostBestLap;
@property (nonatomic, retain) NSString *selectedTrack;
@property (nonatomic, retain) NSString *selectedCar;
@property (nonatomic, retain) NSString *carColor;
@property (nonatomic, readwrite) int carType, trackType, racePosition, myLap;


+ (GhostCar *) sharedInstance;

//record
-(void)resetData;
-(BOOL)saveBestLap:(NSMutableArray *)lap;
-(BOOL)saveRace:(NSMutableArray *)race;
-(NSString*)timeToString:(int)t;

@end