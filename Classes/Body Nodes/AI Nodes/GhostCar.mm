//
//  GhostCar.m
//  Formula Nova Race
//
//  Created by Gil Beyruth on 5/25/16.
//  Copyright © 2016 Gil Beyruth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GhostCar.h"
#import "GameConstants.h"




@implementation GhostCar

static GhostCar *_sharedInstance;

@synthesize selectedTrack,selectedCar,carColor,carType, trackType, ghostArray, ghostLapArray, ghostBestLap, racePosition, myLap;

- (id) init {
    self = [super init];
    if (self != nil){
        
        // custom initialization
        //memset(ghostObject, 0, sizeof(ghostObject));
    }
    return self;
}

+ (GhostCar *) sharedInstance
{
    if (!_sharedInstance)
    {
        _sharedInstance = [[GhostCar alloc] init];
        [_sharedInstance setTrackType:0];
        [_sharedInstance setSelectedTrack:@"usa"];
        [_sharedInstance setCarType:0];
        [_sharedInstance setSelectedCar:@"Mustang"];
        [[_sharedInstance ghostLapArray] initWithCapacity:3];
        [[_sharedInstance ghostArray] init];
    }
    
    return _sharedInstance;
}


-(NSString*)timeToString:(int)t{
    int minutes = t / 60000;
    int seconds = t % 60000 / 1000;
    int milliseconds = t % 1000;
    
    NSString* minutesString;
    NSString* secondsString;
    NSString* millisecondsString;
    
    minutesString = [NSString stringWithFormat:@"%d",minutes];
    if(seconds<=9){
        secondsString = [NSString stringWithFormat:@"0%d",seconds];
    }else{
        secondsString = [NSString stringWithFormat:@"%d",seconds];
    }
    if(milliseconds<=9){
        millisecondsString = [NSString stringWithFormat:@"00%d",milliseconds];
    }else if(milliseconds<=99){
        millisecondsString = [NSString stringWithFormat:@"0%d",milliseconds];
    }else{
        millisecondsString = [NSString stringWithFormat:@"%d",milliseconds];
    }
    // NSLog(@"TIMER TIME - %@ : %@ : %@ ",minutesString, secondsString, millisecondsString);
    return [NSString stringWithFormat:@"%@ : %@ : %@ ",minutesString, secondsString, millisecondsString];
}


-(void) resetData {
    
    carType = 0;
    carColor = kColorRed;
    selectedCar = @"Mustang";
    trackType = 0;
    selectedTrack = @"USA";
    [ghostArray release];
    [ghostLapArray release];
}


-(BOOL)saveBestLap:(NSArray*)lap{
    return YES;
}
-(BOOL)saveRace:(NSArray*)race{

    return YES;
}

- (id)retain

{
    
    return self;
    
}



- (unsigned long)retainCount

{
    
    return UINT_MAX;  //denotes an object that cannot be released
    
}



- (oneway void)release

{
    
    //do nothing
    
}



- (id)autorelease

{
    
    return self;
    
}




@end