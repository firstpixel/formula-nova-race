//
//  GhostStatus.h
//  Formula Nova Race
//
//  Created by Gil Beyruth on 5/27/16.
//  Copyright © 2016 Gil Beyruth. All rights reserved.
//


#import  <Foundation/Foundation.h>

@interface GhostStatus : NSObject <NSCoding> {
    int	index,rotation ,time, car;
    CGPoint	position;
}


@property (nonatomic, readwrite) CGPoint position;
@property (nonatomic, readwrite) int index, rotation, time, car;

@end
