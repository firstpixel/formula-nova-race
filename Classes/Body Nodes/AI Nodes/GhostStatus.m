//
//  GhostStatus.m
//  Formula Nova Race
//
//  Created by Gil Beyruth on 5/27/16.
//  Copyright © 2016 Gil Beyruth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GhostStatus.h"


@implementation GhostStatus

@synthesize car,index,rotation,position, time;

#pragma mark NSCoding

#define kCarTypeKey         @"carType"
#define kIndexKey           @"index"
#define kRotationKey        @"rotation"
#define kPositionKey        @"position"
#define kTimeKey            @"time"

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.car        = [decoder decodeIntegerForKey:kCarTypeKey];
    self.index      = [decoder decodeIntegerForKey:kIndexKey];
    self.rotation   = [decoder decodeIntegerForKey:kRotationKey];
    self.time       = [decoder decodeIntegerForKey:kTimeKey];
    self.position   = [decoder decodeCGPointForKey:kPositionKey];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInteger:self.car forKey:kCarTypeKey];
    [encoder encodeInteger:self.index forKey:kIndexKey];
    [encoder encodeInteger:self.rotation forKey:kRotationKey];
    [encoder encodeInteger:self.time forKey:kTimeKey];
    [encoder encodeCGPoint:self.position forKey:kPositionKey];
}
@end