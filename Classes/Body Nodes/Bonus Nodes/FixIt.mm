//
//  Life.mm
//  LevelSVG
//
//  Created by Ricardo Quesada on 24/03/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//

#import <Box2d/Box2D.h>
#import "cocos2d.h"

#import "GameNode.h"
#import "HUD.h"
#import "GameConstants.h"
#import "Fixit.h"
#import "Hero.h"
#import "SimpleAudioEngine.h"

//
// Life: a sensor with the shape of a heart
// When touched, it will increase the life in 1
//

@implementation Fixit

-(id) initWithBody:(b2Body*)body game:(GameNode*)game
{
	if( (self=[super initWithBody:body game:game]) ) {
		
		CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"fixit.png"];
		[self setDisplayFrame:frame];
		
	}
	return self;
}

-(void) touchedByHero
{
	[super touchedByHero];
    [[game_ hero] carFixed];
    [[game_ hero] setTotalFixUsed:[[game_ hero] totalFixUsed]+1];
    
    [[game_ hud] displayCCSpriteMessage:@"ms-fixed.png"];
	[[SimpleAudioEngine sharedEngine] playEffect: @"fixIt.mp3"];
    [[SimpleAudioEngine sharedEngine] playEffect: @"new_life.wav"];
}

@end
