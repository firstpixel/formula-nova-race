//
//  CarTopDown.h
//  FormulaNova
//
//  Created by Gil Beyruth on 6/14/11.
//  Copyright 2011 Sapus Media. All rights reserved.
//
#import "Hero.h"
#import "SimpleAudioEngine.h"

// The car
@interface Cartopdown : Hero {
    
	// weak ref
	b2Body	*leftWheel_;
	b2Body	*rightWheel_;
	b2Body	*leftRearWheel_;
	b2Body	*rightRearWheel_;
    b2Body	*carBody;
    
	b2RevoluteJoint *leftJoint_;
	b2RevoluteJoint *rightJoint_;
	
	float	engineSpeed_;
	float	steeringAngle_;
    
    BOOL isCrashing;
    BOOL isTurboActive;
    BOOL isCrashed;
    int   carType,nextWayPoint,myLap,racePosition,crashPoints;
    NSString *carColor;
    float lw;
    float rw;
    float lrw;
    float rrw;
    SimpleAudioEngine* sae;
    CDSoundSource* sound1;
    
    CCSpriteFrame *frame;
    CCParticleSystemQuad *carExplosion;
    CCParticleSystemQuad *carTurbo;
    
    float steerSpeedLowVelocity;
    float steerSpeedMediumVelocity;
    float steerSpeedHighVelocity;
    float maxVelocity,maxVelocityDefault;
    float horsePowers;
    float maxVelocityCrashed, maxVelocityTurbo, maxVelocityTurboCrashed,maxSteerAngle,accelerationFactor;
    
 
}

@property (nonatomic, readwrite) float maxVelocity,maxVelocityDefault, maxVelocityCrashed,maxSteerAngle,  maxVelocityTurbo, maxVelocityTurboCrashed, steerSpeedLowVelocity, steerSpeedMediumVelocity, steerSpeedHighVelocity, horsePowers;

@property (nonatomic, readwrite) int carType,nextWayPoint,myLap,racePosition,crashPoints,totalCrashesAllowed;

-(float)getAngle;
-(void)skidmarkMefrt:(BOOL)frt flt:(BOOL)flt rrt:(BOOL)rrt rlt:(BOOL)rlt;
-(float)getVelocity;
-(CGFloat)nextWayPointDistance;
-(void) activateTurbo;
-(void) carFixed;
-(void) carCrashed;
@end
