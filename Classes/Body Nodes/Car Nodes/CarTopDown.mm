//
//  CarTopDown.mm
//  FormulaNova
//
//  Created by Gil Beyruth on 6/14/11.
//  Copyright 2011 Sapus Media. All rights reserved.
//

#import "CarTopDown.h"
#import "SimpleAudioEngine.h"

#import "GameConfiguration.h"
#import "GameConstants.h"
#import "GameNode.h"
#import "GlobalSingleton.h"

#include <stdio.h>
#include <stdlib.h>

#pragma mark -
#pragma mark Cartopdown

// Forces & Impulses

// Modify this value to create a bigger/smaller car
const float CAR_SCALE = 0.2f;
const b2Vec2 leftRearWheelPosition = b2Vec2(-1.2f * CAR_SCALE, 1.9f * CAR_SCALE);
const b2Vec2 rightRearWheelPosition = b2Vec2(1.2f * CAR_SCALE, 1.9f * CAR_SCALE);
const b2Vec2 leftFrontWheelPosition = b2Vec2(-1.2f * CAR_SCALE, -1.9f * CAR_SCALE);
const b2Vec2 rightFrontWheelPosition = b2Vec2(1.2f * CAR_SCALE, -1.9f * CAR_SCALE);
//3.0
/*const float MAX_STEER_ANGLE = (float)M_PI /3.0f ;
 //2.5
 const float STEER_SPEED = 2.2f;
 const float STEER_MEDIUM_SPEED_VELOCITY = 1.2f;
 const float STEER_HIGH_SPEED_VELOCITY = 0.3f;
 const float HORSEPOWERS = 8 * CAR_SCALE;
 */
//
// CarTopDown: The main character of the game.
//

@implementation Cartopdown
@synthesize maxVelocity, maxVelocityDefault, maxVelocityCrashed, maxVelocityTurbo, maxVelocityTurboCrashed, maxSteerAngle,  steerSpeedLowVelocity, steerSpeedMediumVelocity, steerSpeedHighVelocity, horsePowers;
@synthesize carType,nextWayPoint,myLap,racePosition,crashPoints,totalCrashesAllowed;
-(id) initWithBody:(b2Body*)body game:(GameNode*)aGame
{
    if( (self=[super initWithBody:body game:aGame] ) ) {
        
        carBody = body;
        //
        // Set up the right texture
        //
        
        // Set the default frame from singleton class
        GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
        carColor = [mySingleton carColorP1];
        
        //NSLog(@"car color :%@",carColor);
        
        carType = [mySingleton carTypeP1];
        nextWayPoint = 0;
        myLap = 0;
        
        //MAX SPEED
        //MAX SPEED
        float maxSteerAngleLevel1 =(float)M_PI /2.6f;
        float maxSteerAngleLevel2 = (float)M_PI /2.7f;
        float maxSteerAngleLevel3 = (float)M_PI /2.8f;
        float maxSteerAngleLevel4 = (float)M_PI /2.9f;
        
        float steerSpeedLowVelocityLevel1 = 1.8f;
        float steerSpeedLowVelocityLevel2 = 1.7f;
        float steerSpeedLowVelocityLevel3 = 1.6f;
        
        float steerSpeedMediumVelocityLevel1 = 1.4f;
        float steerSpeedMediumVelocityLevel2 = 1.3f;
        float steerSpeedMediumVelocityLevel3 = 1.2f;
        
        float steerSpeedHighVelocityLevel1 = 0.6f;
        float steerSpeedHighVelocityLevel2 = 0.5f;
        float steerSpeedHighVelocityLevel3 = 0.4f;
        
        float horsePowersLevel1 = 11 * CAR_SCALE;
        float horsePowersLevel2 = 12 * CAR_SCALE;
        float horsePowersLevel3 = 13 * CAR_SCALE;
        float horsePowersLevel4 = 14 * CAR_SCALE;
        
        NSString*carAudio;
        switch(carType){
            case 0:
                //MUSTANG
                carAudio = @"engine0.wav";
                totalCrashesAllowed = 4;
                maxSteerAngle = maxSteerAngleLevel1;
                
                maxVelocityDefault = 165.0f;
                maxVelocityCrashed = 155.0f;
                maxVelocityTurbo = 185.0f;
                maxVelocityTurboCrashed = 165.0f;
                accelerationFactor = 7;
                
                steerSpeedLowVelocity = steerSpeedLowVelocityLevel1;
                steerSpeedMediumVelocity = steerSpeedMediumVelocityLevel1;
                steerSpeedHighVelocity = steerSpeedHighVelocityLevel1;
                
                horsePowers = horsePowersLevel1;
                
                break;
            case 1:
                //CAMARO
                carAudio = @"engine0.wav";
                totalCrashesAllowed = 3;
                maxSteerAngle = maxSteerAngleLevel2;
                
                maxVelocityDefault = 165.0f;
                maxVelocityCrashed = 160.0f;
                maxVelocityTurbo = 190.0f;
                maxVelocityTurboCrashed = 165.0f;
                accelerationFactor = 6;
                
                steerSpeedLowVelocity = steerSpeedLowVelocityLevel2;
                steerSpeedMediumVelocity = steerSpeedMediumVelocityLevel2;
                steerSpeedHighVelocity = steerSpeedHighVelocityLevel2;
                horsePowers = horsePowersLevel2;
                
                break;
            case 2:
                //P CARRERA
                carAudio = @"engine1.wav";
                totalCrashesAllowed = 3;
                maxSteerAngle = maxSteerAngleLevel3;
                
                maxVelocityDefault = 170.0f;
                maxVelocityCrashed = 165.0f;
                maxVelocityTurbo = 200.0f;
                maxVelocityTurboCrashed = 170.0f;
                accelerationFactor = 5;
                
                steerSpeedLowVelocity = steerSpeedLowVelocityLevel1;
                steerSpeedMediumVelocity = steerSpeedMediumVelocityLevel1;
                steerSpeedHighVelocity = steerSpeedHighVelocityLevel1;
                horsePowers = horsePowersLevel3;
                break;
                
            case 3:
                //VIPER
                carAudio = @"engine1.wav";
                totalCrashesAllowed = 2;
                maxSteerAngle = maxSteerAngleLevel4;
                
                maxVelocityDefault = 175.0f;
                maxVelocityCrashed = 170.0f;
                maxVelocityTurbo = 204.0f;
                maxVelocityTurboCrashed = 175.0f;
                accelerationFactor = 5;
                
                steerSpeedLowVelocity = steerSpeedLowVelocityLevel2;
                steerSpeedMediumVelocity = steerSpeedMediumVelocityLevel2;
                steerSpeedHighVelocity = steerSpeedHighVelocityLevel2;
                horsePowers = horsePowersLevel4;
                break;
            case 4:
                //FERRARI
                carAudio = @"engine2.wav";
                totalCrashesAllowed = 2;
                maxSteerAngle = maxSteerAngleLevel4;
                
                maxVelocityDefault = 185.0f;
                maxVelocityCrashed = 170.0f;
                maxVelocityTurbo = 210.0f;
                maxVelocityTurboCrashed = 185.0f;
                accelerationFactor = 4;
                
                steerSpeedLowVelocity = steerSpeedLowVelocityLevel3;
                steerSpeedMediumVelocity = steerSpeedMediumVelocityLevel3;
                steerSpeedHighVelocity = steerSpeedHighVelocityLevel3;
                horsePowers = horsePowersLevel3;
                break;
            case 5:
                //PORSCHE TURBO
                carAudio = @"engine2.wav";
                totalCrashesAllowed = 3;
                maxSteerAngle = maxSteerAngleLevel4;
                
                maxVelocityDefault = 185.0f;
                maxVelocityCrashed = 160.0f;
                maxVelocityTurbo = 212.0f;
                maxVelocityTurboCrashed = 185.0f;
                accelerationFactor = 3;
                
                steerSpeedLowVelocity = steerSpeedLowVelocityLevel2;
                steerSpeedMediumVelocity = steerSpeedMediumVelocityLevel2;
                steerSpeedHighVelocity = steerSpeedHighVelocityLevel2;
                horsePowers = horsePowersLevel4;
                break;
            case 6:
                //LAMBORGHINI DIABLO
                carAudio = @"engine3.wav";
                totalCrashesAllowed = 2;
                maxSteerAngle = maxSteerAngleLevel4;
                
                maxVelocityDefault = 188.0f;
                maxVelocityCrashed = 170.0f;
                maxVelocityTurbo = 215.0f;
                maxVelocityTurboCrashed = 188.0f;
                accelerationFactor = 3;
                
                steerSpeedLowVelocity = steerSpeedLowVelocityLevel3;
                steerSpeedMediumVelocity = steerSpeedMediumVelocityLevel3;
                steerSpeedHighVelocity = steerSpeedHighVelocityLevel3;
                horsePowers = horsePowersLevel4;
                break;
        }
        maxVelocity = maxVelocityDefault;
        frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"car%i_%@.png",carType,carColor]];
        [self setDisplayFrame:frame];
        
        preferredParent_ = BN_PREFERRED_PARENT_SPRITES_PNG;
        
        //		self.isTouchable = YES;
        //
        // box2d stuff: Create the "correct" fixture
        //
        // 1. destroy already created fixtures
        [self destroyAllFixturesFromBody:body];
        
        //
        // Car model based on:
        // http://www.emanueleferonato.com/2009/04/06/two-ways-to-make-box2d-cars/
        //
        
        //
        // Bodies
        //
        carBody->SetLinearDamping(1);
        carBody->SetAngularDamping(1);
        carBody->SetType(b2_dynamicBody);
        
        float originalAngle = carBody->GetAngle();
        
        carBody->SetTransform( carBody->GetPosition(), 0 );
        
        //left wheel
        b2BodyDef leftWheelDef;
        leftWheelDef.position = carBody->GetPosition() + leftFrontWheelPosition;
        //		leftWheelDef.angle = body->GetAngle();
        leftWheel_ = world_->CreateBody(&leftWheelDef);
        leftWheel_->SetType(b2_dynamicBody);
        
        
        //right wheel
        b2BodyDef rightWheelDef;
        rightWheelDef.position = carBody->GetPosition() + rightFrontWheelPosition;
        //		rightWheelDef.angle = body->GetAngle();
        rightWheel_ = world_->CreateBody(&rightWheelDef);
        rightWheel_->SetType(b2_dynamicBody);
        
        //left rear wheel
        b2BodyDef leftRearWheelDef;
        leftRearWheelDef.position = carBody->GetPosition() + leftRearWheelPosition;
        //		leftRearWheelDef.angle = body->GetAngle();
        leftRearWheel_ = world_->CreateBody(&leftRearWheelDef);
        leftRearWheel_->SetType(b2_dynamicBody);
        
        //right rear wheel
        b2BodyDef rightRearWheelDef;
        rightRearWheelDef.position = carBody->GetPosition() + rightRearWheelPosition;
        //		rightRearWheelDef.angle = body->GetAngle();
        rightRearWheel_ = world_->CreateBody(&rightRearWheelDef);
        rightRearWheel_->SetType(b2_dynamicBody);
        
        //
        // Shapes
        //
        // car shape
        b2PolygonShape boxDef;
        boxDef.SetAsBox(1.2f * CAR_SCALE, 2.5f * CAR_SCALE);
        carBody->CreateFixture(&boxDef, 1);
        
        b2MassData data;
        carBody->GetMassData(&data);

        b2CircleShape circleBDef;
        circleBDef.m_radius = 0.25f;
        circleBDef.m_p.Set(0.0f, 0.34f);
        carBody->CreateFixture(&circleBDef, 1);

        b2CircleShape circleFDef;
        circleFDef.m_radius = 0.25f;
        circleFDef.m_p.Set(0.0f, -0.34f);
        carBody->CreateFixture(&circleFDef, 1);
        carBody->SetMassData( &data );
        

        
        //Left Wheel shape
        b2PolygonShape leftWheelShapeDef;
        leftWheelShapeDef.SetAsBox(0.2f * CAR_SCALE, 0.5f * CAR_SCALE);
        leftWheel_->CreateFixture(&leftWheelShapeDef, 1);
        
        //Right Wheel shape
        b2PolygonShape rightWheelShapeDef;
        rightWheelShapeDef.SetAsBox(0.2f * CAR_SCALE, 0.5f * CAR_SCALE);
        rightWheel_->CreateFixture( &rightWheelShapeDef, 1);
        
        //Left Wheel shape
        b2PolygonShape leftRearWheelShapeDef;
        leftRearWheelShapeDef.SetAsBox(0.2f * CAR_SCALE, 0.5f * CAR_SCALE);
        leftRearWheel_->CreateFixture(&leftRearWheelShapeDef,1);
        
        //Right Wheel shape
        b2PolygonShape rightRearWheelShapeDef;
        rightRearWheelShapeDef.SetAsBox(0.2f * CAR_SCALE, 0.5f * CAR_SCALE);
        rightRearWheel_->CreateFixture(&rightRearWheelShapeDef,1);
        
        //
        // Joints
        //
        b2RevoluteJointDef leftJointDef;
        leftJointDef.Initialize(carBody, leftWheel_, leftWheel_->GetWorldCenter());
        leftJointDef.enableMotor = true;
        //100
        leftJointDef.maxMotorTorque = 40;
        b2RevoluteJointDef rightJointDef;
        rightJointDef.Initialize(carBody, rightWheel_, rightWheel_->GetWorldCenter());
        rightJointDef.enableMotor = true;
        //100
        rightJointDef.maxMotorTorque = 40;
        leftJoint_ = (b2RevoluteJoint*) world_->CreateJoint(&leftJointDef);
        rightJoint_ = (b2RevoluteJoint*) world_->CreateJoint(&rightJointDef);
        
        
        leftJoint_->EnableLimit(true);
        rightJoint_->EnableLimit(true);
        
        b2PrismaticJointDef leftRearJointDef;
        leftRearJointDef.Initialize(carBody, leftRearWheel_, leftRearWheel_->GetWorldCenter(), b2Vec2(1,0));
        leftRearJointDef.enableLimit = true;
        leftRearJointDef.lowerTranslation = leftRearJointDef.upperTranslation = 0;
        
        b2PrismaticJointDef rightRearJointDef;
        rightRearJointDef.Initialize(carBody, rightRearWheel_, rightRearWheel_->GetWorldCenter(), b2Vec2(1,0));
        rightRearJointDef.enableLimit = true;
        rightRearJointDef.lowerTranslation = rightRearJointDef.upperTranslation = 0;
        
        world_->CreateJoint(&leftRearJointDef);
        world_->CreateJoint(&rightRearJointDef);
        
        // steering angle, speed
        steeringAngle_ = 0;
        engineSpeed_ = 0;
        crashPoints = 0;
        isCrashing = NO;
        isTurboActive = NO;
        isCrashed = NO;
        carBody->SetTransform( carBody->GetPosition(), originalAngle );
        // Tell the game, that this instace is the Hero
        [game_ setHero:self];
        sae = [SimpleAudioEngine sharedEngine];
        [sae preloadEffect:carAudio];
        sound1 = [[sae soundSourceForFile:carAudio] retain];
        sound1.looping = YES;
        sound1.gain = 1.0f;
        sound1.pitch = 0.5f;
        [sound1 play];
    }
    return self;
}
-(void)clearCrashing{
    isCrashing = NO;
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    if(![[mySingleton deviceType] isEqualToString:@"oldIphone"]){
        [game_ removeCrashedEffect];
    }
    [game_ removeExplosionEffect];
}
-(void)carCrashed{
    // NSLog(@"CRASHED : %@, %i, %@",(isCrashing ? @"YES" : @"NO"),crashPoints,carColor);
    if(isCrashing == NO){
        isCrashing = YES;
        GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
        if(![[mySingleton deviceType] isEqualToString:@"oldIphone"]){
            [game_ initCrashedEffect];
        }
        /* id action = [CCCallBlock actionWithBlock:^{
         isCrashing = NO;
         [game_ removeCrashedEffect];
         [game_ removeExplosionEffect];
         }];
         */
        id action = [CCCallFunc actionWithTarget:self selector:@selector(clearCrashing)];
        
        //create sequence to have a delay, and then run the callblock action:
        id sequence = [CCSequence actions:[CCDelayTime actionWithDuration:3.0],action, nil];
        
        //run action:
        [self runAction:sequence];
        
        [[SimpleAudioEngine sharedEngine] playEffect: @"carhit.mp3"];
        
        crashPoints++;
        totalCrashs++;
        if( crashPoints == totalCrashesAllowed ){
            isCrashed = YES;
            maxVelocity = maxVelocityCrashed;
            [game_ initExplosionEffect];
            frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"car%i_crashed_%@.png",carType,carColor]];
            [self setDisplayFrame:frame];
        }
    }
}

-(void)carFixed{
    isCrashed = NO;
    crashPoints = 0;
    maxVelocity = maxVelocityDefault;
    frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"car%i_%@.png",carType,carColor]];
    [self setDisplayFrame:frame];
}



-(void)skidmarkMefrt:(BOOL)frt flt:(BOOL)flt rrt:(BOOL)rrt rlt:(BOOL)rlt
{
    //skidmark
    if(frt) [game_ addSkidMarkAt:CGPointMake(rightWheel_->GetPosition().x*kPhysicsPTMRatio,rightWheel_->GetPosition().y*kPhysicsPTMRatio) angle:CC_RADIANS_TO_DEGREES(rightWheel_->GetAngle()*-1)];
    //skidmark
    if(flt) [game_ addSkidMarkAt:CGPointMake(leftWheel_->GetPosition().x*kPhysicsPTMRatio,leftWheel_->GetPosition().y*kPhysicsPTMRatio) angle:CC_RADIANS_TO_DEGREES(leftWheel_->GetAngle()*-1)];
    //skidmark
    if(rrt) [game_ addSkidMarkAt:CGPointMake(rightRearWheel_->GetPosition().x*kPhysicsPTMRatio,rightRearWheel_->GetPosition().y*kPhysicsPTMRatio) angle:CC_RADIANS_TO_DEGREES(rightRearWheel_->GetAngle()*-1)];
    //skidmark
    if(rlt) [game_ addSkidMarkAt:CGPointMake(leftRearWheel_->GetPosition().x*kPhysicsPTMRatio,leftRearWheel_->GetPosition().y*kPhysicsPTMRatio) angle:CC_RADIANS_TO_DEGREES(leftRearWheel_->GetAngle()*-1)];
    
    //smoke
    if(rrt)[game_ addSmokeRightSkidmark:CGPointMake(rightRearWheel_->GetPosition().x*kPhysicsPTMRatio,rightRearWheel_->GetPosition().y*kPhysicsPTMRatio) angle:CC_RADIANS_TO_DEGREES(rightRearWheel_->GetAngle()*-1)];
    //smoke
    if(rlt)[game_ addSmokeLeftSkidmark:CGPointMake(leftRearWheel_->GetPosition().x*kPhysicsPTMRatio,leftRearWheel_->GetPosition().y*kPhysicsPTMRatio) angle:CC_RADIANS_TO_DEGREES(leftRearWheel_->GetAngle()*-1)];
    
    //audio
    if(rlt && rrt)[[SimpleAudioEngine sharedEngine] playEffect: @"skidmark2.mp3" pitch:1.3f pan:0.0f gain:0.5f];
    
}
-(float)getAngle{
    return carBody->GetAngle();
}
-(CGFloat)nextWayPointDistance
{
    NSValue * p = [game_ getWayPointByTag:nextWayPoint];
    CGPoint point = [p CGPointValue];
    return ccpDistance(CGPointMake(carBody->GetPosition().x,carBody->GetPosition().y), point);
}
-(void)removeTurboEffect{
    [game_ removeTurboEffect];
    //change speed back here
    maxVelocity = maxVelocityDefault;
    self.color = (ccColor3B) {255,255,255};
    isTurboActive = NO;
}

-(void) activateTurbo{
    int turboDuration = 5;
    [game_ initTurboEffect];
    //change speed of action here
    isTurboActive = YES;
    if(isCrashed){
        maxVelocity = maxVelocityTurboCrashed;
    }else{
        maxVelocity = maxVelocityTurbo;
        
    }
    self.color = (ccColor3B) {255,255,0};
    
    id action = [CCCallFunc actionWithTarget:self selector:@selector(removeTurboEffect)];
    //create sequence to have a delay, and then run the callblock action:
    id sequence = [CCSequence actions:[CCDelayTime actionWithDuration:turboDuration],action, nil];
    //run action:
    [self runAction:sequence];
}

-(float32) getAngleToNextWayPoint:(int)wp
{
    NSValue* p = [game_ getWayPointByTag:wp];
    CGPoint point = [p CGPointValue];
    float angleRadians = atan2( ( carBody->GetPosition().y - point.y ), ( carBody->GetPosition().x - point.x ) )-M_PI/2;
    return angleRadians;
    
}

-(float) getVelocity{
    //TODO GEAR BOX HERE
    float velo = carBody->GetLinearVelocity().Length();
    float pitch = (velo*30)/200+1/4;
    //NSLog(@"pitch %f",pitch);
    sound1.pitch = pitch+0.3;
    
    float gain = pitch/2<0.2?0.2:pitch/2>1?0.8:pitch/2;
    sound1.gain = gain+0.8;
    
    return velo*24;
    //   return velo*19;
    
}


//This function applies a "friction" in a direction orthogonal to the body's axis.
-(void) killOrthogonalVelocity:(b2Body*)targetBody
{
    
    b2Vec2 localPoint = b2Vec2(0,0);
    b2Vec2 velocity = targetBody->GetLinearVelocityFromLocalPoint(localPoint);
    //b2Vec2 sidewaysAxis = targetBody->GetTransform().R.col2;
    b2Vec2 sidewaysAxis = targetBody->GetTransform().q.GetYAxis();
    sidewaysAxis *= b2Dot(velocity,sidewaysAxis);
    targetBody->SetLinearVelocity(sidewaysAxis);//targetBody.GetWorldPoint(localPoint));
}

bool noSlide = YES;
int slideCounter = 0;
int maxSlideCounter = 3;
-(void) update:(ccTime)dt
{
    
    // Call super, because the base class needs to update it
    [super update:dt];
    
    
    if([self getVelocity]>130){
        noSlide = NO;
        slideCounter++;
    }else{
        noSlide = YES;
        
    }
    
    if(noSlide || slideCounter>maxSlideCounter){
        [self killOrthogonalVelocity:leftWheel_];
        [self killOrthogonalVelocity:rightWheel_];
        [self killOrthogonalVelocity:leftRearWheel_];
        [self killOrthogonalVelocity:rightRearWheel_];
        slideCounter=0;
    }
    b2Vec2 ldirection = leftWheel_->GetTransform().q.GetYAxis();
    b2Vec2 rdirection = rightWheel_->GetTransform().q.GetYAxis();
    //TODO SPEEDY TEST float accelerator = accelerationFactor-([self getVelocity]/14);
    
    float accelerator = accelerationFactor-([self getVelocity]/10);
    if(accelerator>3)accelerator=3;
    if(accelerator>1){
        ldirection *= engineSpeed_/accelerator;
        rdirection *= engineSpeed_/accelerator;
    }else{
        ldirection *= engineSpeed_;
        rdirection *= engineSpeed_;
        
    }
    
    /*
     if([self getVelocity]<30){
     //Driving
     ldirection *= engineSpeed_/4;
     rdirection *= engineSpeed_/4;
     }else if([self getVelocity]>30 && [self getVelocity]<80){
     ldirection *= engineSpeed_/2;
     rdirection *= engineSpeed_/2;
     }else{
     ldirection *= engineSpeed_;
     rdirection *= engineSpeed_;
     
     }
     */
    //NSLog(@"player engineSpeed_ %f",engineSpeed_);
    
    if([self getVelocity] < maxVelocity){
        leftWheel_->ApplyForce(ldirection, leftWheel_->GetPosition());
        rightWheel_->ApplyForce(rdirection, rightWheel_->GetPosition());
        
        // NSLog(@"APPLY FORCE %f, %f",ldirection.x,ldirection.y);
    }
    
    // NSLog(@"player steeringAngle %f",steeringAngle_);
    float32 carToPointAngle = [self getAngleToNextWayPoint:nextWayPoint];
    
    
    
    float wrongWay = (carToPointAngle - (carBody->GetAngle()));
    while(wrongWay>M_PI/2){
        wrongWay = wrongWay-M_PI*2;
    }
    while(wrongWay<-M_PI/2){
        wrongWay = wrongWay+M_PI*2;
    }
    
    // NSLog(@"%f",wrongWay);
    [game_ wrongWayAlert:wrongWay];
    
    
    //Driving Assistent start
    //NSLog(@"NEXT WAYPOINT:%i",nextWayPoint);
    carToPointAngle = [self getAngleToNextWayPoint:nextWayPoint];
    float angleToPoint_ = (carToPointAngle - (leftWheel_->GetAngle()));
    // NSLog(@"set steeringAngle %f",steeringAngle_);
    while(angleToPoint_>M_PI/2){
        angleToPoint_ = angleToPoint_-M_PI*2;
    }
    while(angleToPoint_<-M_PI/2){
        angleToPoint_ = angleToPoint_+M_PI*2;
    }
    
    float steeringAngleHelper_ = 0;
    if(angleToPoint_>0){
        steeringAngleHelper_=1.0f * maxSteerAngle;
        
    }else if(angleToPoint_<0){
        steeringAngleHelper_=-1.0f * maxSteerAngle;
        
    }else{
        steeringAngleHelper_=0.0f;
        
    }
    //Driving Assistent end
    
    
    if(steeringAngle_!=-0.0f){
        //Steering
        GameConfiguration *config = [GameConfiguration sharedConfiguration];
        ControlType control = [config controlType];
        
        ControlAssistent controlAssistent = [config controlAssistent];
        
        //NSLog(@"controlAssistent : %u", controlAssistent);
        //NSLog(@"controlType : %u", control);
        /***********************************************HIGH********************************************/
        
        if([self getVelocity]>110){
            /******************************************
             *
             *
             *      ACCELEROMETER  -  HIGH VELOCITY
             *
             *
             ******************************************/
            if (control==kControlTypeAccelerometer  || control==kControlTypeDragArrow) {
                float mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                float limit = 0.6;
                
                if(controlAssistent == kControlAssistentOff){
                    if(steeringAngle_>0){
                        leftJoint_->SetLimits(0,0.7);
                        rightJoint_->SetLimits(0,0.7);
                    }else{
                        leftJoint_->SetLimits(-0.7,0);
                        rightJoint_->SetLimits(-0.7,0);
                    }
                    mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    leftJoint_->SetMotorSpeed(mspeed * steerSpeedHighVelocity);
                    float mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    rightJoint_->SetMotorSpeed(mspeed * steerSpeedHighVelocity);
                }else{
                    if(angleToPoint_>0){
                        leftJoint_->SetLimits(-0.001,angleToPoint_+0.001);
                        rightJoint_->SetLimits(-0.001,angleToPoint_+0.001);
                    }else{
                        leftJoint_->SetLimits(angleToPoint_-0.001,0.001);
                        rightJoint_->SetLimits(angleToPoint_-0.001,0.001);
                    }
                    limit = 0.4;
                    mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    mspeed=mspeed/2;
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    leftJoint_->SetMotorSpeed(mspeed * steerSpeedHighVelocity);
                    
                    mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                    mspeed=mspeed/2;
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    rightJoint_->SetMotorSpeed(mspeed * steerSpeedHighVelocity);
                }
                
                
            }else
                if (control==kControlTypeDrag) {
                    
                    /******************************************
                     *
                     *
                     *      DRAG  -  HIGH VELOCITY
                     *
                     *
                     ******************************************/
                    float mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    float limit = 0.6;
                    
                    if(controlAssistent == kControlAssistentOff){
                        if(steeringAngle_>0){
                            leftJoint_->SetLimits(0,0.7);
                            rightJoint_->SetLimits(0,0.7);
                        }else{
                            leftJoint_->SetLimits(-0.7,0);
                            rightJoint_->SetLimits(-0.7,0);
                        }
                        mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                        if(mspeed>limit)mspeed =  limit;
                        if(mspeed<-limit)mspeed = - limit;
                        leftJoint_->SetMotorSpeed(mspeed * steerSpeedHighVelocity);
                        float mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                        if(mspeed>limit)mspeed =  limit;
                        if(mspeed<-limit)mspeed = - limit;
                        rightJoint_->SetMotorSpeed(mspeed * steerSpeedHighVelocity);
                    }else{
                        if(angleToPoint_>0){
                            leftJoint_->SetLimits(-0.001,angleToPoint_+0.001);
                            rightJoint_->SetLimits(-0.001,angleToPoint_+0.001);
                        }else{
                            leftJoint_->SetLimits(angleToPoint_-0.001,0.001);
                            rightJoint_->SetLimits(angleToPoint_-0.001,0.001);
                        }
                        limit = 0.5;
                        mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                        mspeed=mspeed/2;
                        if(mspeed>limit)mspeed =  limit;
                        if(mspeed<-limit)mspeed = - limit;
                        leftJoint_->SetMotorSpeed(mspeed * steerSpeedHighVelocity);
                        
                        mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                        mspeed=mspeed/2;
                        if(mspeed>limit)mspeed =  limit;
                        if(mspeed<-limit)mspeed = - limit;
                        rightJoint_->SetMotorSpeed(mspeed * steerSpeedHighVelocity);
                    }
                    
                }else
                    if (control==kControlTypeHit) {
                        /******************************************
                         *
                         *
                         *      HIT  -  HIGH VELOCITY
                         *
                         *
                         ******************************************/
                        float mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                        float limit = 0.7;
                        
                        if(controlAssistent == kControlAssistentOff){
                            if(steeringAngle_>0){
                                leftJoint_->SetLimits(0,0.7);
                                rightJoint_->SetLimits(0,0.7);
                            }else{
                                leftJoint_->SetLimits(-0.7,0);
                                rightJoint_->SetLimits(-0.7,0);
                            }
                            mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                            if(mspeed>limit)mspeed =  limit;
                            if(mspeed<-limit)mspeed = - limit;
                            leftJoint_->SetMotorSpeed(mspeed * steerSpeedHighVelocity);
                            float mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                            if(mspeed>limit)mspeed =  limit;
                            if(mspeed<-limit)mspeed = - limit;
                            rightJoint_->SetMotorSpeed(mspeed * steerSpeedHighVelocity);
                            
                        }else{
                            if(angleToPoint_>0){
                                leftJoint_->SetLimits(-0.03,angleToPoint_+0.03);
                                rightJoint_->SetLimits(-0.03,angleToPoint_+0.03);
                            }else{
                                leftJoint_->SetLimits(angleToPoint_-0.03,0.03);
                                rightJoint_->SetLimits(angleToPoint_-0.03,0.03);
                            }
                            limit = 0.6;
                            
                            mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                            mspeed=mspeed/2;
                            if(mspeed>limit)mspeed =  limit;
                            if(mspeed<-limit)mspeed = - limit;
                            leftJoint_->SetMotorSpeed(mspeed * steerSpeedHighVelocity);
                            
                            mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                            mspeed=mspeed/2;
                            if(mspeed>limit)mspeed =  limit;
                            if(mspeed<-limit)mspeed = - limit;
                            rightJoint_->SetMotorSpeed(mspeed * steerSpeedHighVelocity);
                        }
                    }
            /***********************************************MEDIUM********************************************/
        }else if([self getVelocity]>30 && [self getVelocity]<=110){
            /******************************************
             *
             *
             *      ACCELEROMETER  -  MEDIUM VELOCITY
             *
             *
             ******************************************/
            
            if (control==kControlTypeAccelerometer  || control==kControlTypeDragArrow) {
                float mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                float limit = 0.8;
                
                if(controlAssistent == kControlAssistentOff){
                    if(steeringAngle_>0){
                        leftJoint_->SetLimits(0,0.9);
                        rightJoint_->SetLimits(0,0.9);
                    }else{
                        leftJoint_->SetLimits(-0.9,0);
                        rightJoint_->SetLimits(-0.9,0);
                    }
                    mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    leftJoint_->SetMotorSpeed(mspeed * steerSpeedMediumVelocity);
                    float mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    rightJoint_->SetMotorSpeed(mspeed * steerSpeedMediumVelocity);
                }else{
                    if(angleToPoint_>0){
                        leftJoint_->SetLimits(-0.2,angleToPoint_);
                        rightJoint_->SetLimits(-0.2,angleToPoint_);
                    }else{
                        leftJoint_->SetLimits(angleToPoint_,0.2);
                        rightJoint_->SetLimits(angleToPoint_,0.2);
                    }
                    limit = 0.5;
                    mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    mspeed=mspeed/2;
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    leftJoint_->SetMotorSpeed(mspeed * steerSpeedMediumVelocity);
                    
                    mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                    mspeed=mspeed/2;
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    rightJoint_->SetMotorSpeed(mspeed * steerSpeedMediumVelocity);
                }
                
                
                
                /******************************************
                 *
                 *
                 *      DRAG  -  MEDIUM VELOCITY
                 *
                 *
                 ******************************************/
            }else if (control==kControlTypeDrag) {
                float mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                float limit = 0.8;
                
                if(controlAssistent == kControlAssistentOff){
                    if(steeringAngle_>0){
                        leftJoint_->SetLimits(0,0.9);
                        rightJoint_->SetLimits(0,0.9);
                    }else{
                        leftJoint_->SetLimits(-0.9,0);
                        rightJoint_->SetLimits(-0.9,0);
                    }
                    mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    leftJoint_->SetMotorSpeed(mspeed * steerSpeedMediumVelocity);
                    float mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    rightJoint_->SetMotorSpeed(mspeed * steerSpeedMediumVelocity);
                }else{
                    if(angleToPoint_>0){
                        leftJoint_->SetLimits(-0.2,angleToPoint_);
                        rightJoint_->SetLimits(-0.2,angleToPoint_);
                    }else{
                        leftJoint_->SetLimits(angleToPoint_,0.2);
                        rightJoint_->SetLimits(angleToPoint_,0.2);
                    }
                    limit = 0.5;
                    mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    mspeed=mspeed/2;
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    leftJoint_->SetMotorSpeed(mspeed * steerSpeedMediumVelocity);
                    
                    mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                    mspeed=mspeed/2;
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    rightJoint_->SetMotorSpeed(mspeed * steerSpeedMediumVelocity);
                    
                }
                /******************************************
                 *
                 *
                 *      HIT  -  MEDIUM VELOCITY
                 *
                 *
                 ******************************************/
            }else if (control==kControlTypeHit) {
                float mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                float limit = 0.8;
                
                if(controlAssistent == kControlAssistentOff){
                    if(steeringAngle_>0){
                        leftJoint_->SetLimits(0,0.9);
                        rightJoint_->SetLimits(0,0.9);
                    }else{
                        leftJoint_->SetLimits(-0.9,0);
                        rightJoint_->SetLimits(-0.9,0);
                    }
                    mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    leftJoint_->SetMotorSpeed(mspeed * steerSpeedMediumVelocity);
                    float mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    rightJoint_->SetMotorSpeed(mspeed * steerSpeedMediumVelocity);
                }else{
                    if(angleToPoint_>0){
                        leftJoint_->SetLimits(-0.2,angleToPoint_);
                        rightJoint_->SetLimits(-0.2,angleToPoint_);
                    }else{
                        leftJoint_->SetLimits(angleToPoint_,0.2);
                        rightJoint_->SetLimits(angleToPoint_,0.2);
                    }
                    limit = 0.9;
                    mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    mspeed=mspeed/2;
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    leftJoint_->SetMotorSpeed(mspeed * steerSpeedMediumVelocity);
                    
                    mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                    mspeed=mspeed/2;
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    rightJoint_->SetMotorSpeed(mspeed * steerSpeedMediumVelocity);
                    
                }
                //CCLOG(@"HIT MEDIUM END: %f",mspeed);
            }
        }else {
            /***********************************************LOW********************************************/
            /******************************************
             *
             *
             *      ACCELEROMETER  -  LOW VELOCITY
             *
             *
             ******************************************/
            float mspeed = steeringAngle_- leftJoint_->GetJointAngle();
            float limit = 1.0;
            if (control==kControlTypeAccelerometer  || control==kControlTypeDragArrow) {
                if(controlAssistent == kControlAssistentOff){
                    if(steeringAngle_>0){
                        leftJoint_->SetLimits(0,1.2);
                        rightJoint_->SetLimits(0,1.2);
                    }else{
                        leftJoint_->SetLimits(-1.2,0);
                        rightJoint_->SetLimits(-1.2,0);
                    }
                    mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    leftJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
                    float mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    rightJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
                }else{
                    if(angleToPoint_>0){
                        leftJoint_->SetLimits(-0.3,angleToPoint_+0.1);
                        rightJoint_->SetLimits(-0.3,angleToPoint_+0.1);
                    }else{
                        leftJoint_->SetLimits(angleToPoint_-0.1,0.3);
                        rightJoint_->SetLimits(angleToPoint_-0.1,0.3);
                    }
                    limit = 0.9;
                    mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    mspeed=mspeed/2;
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    leftJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
                    
                    mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                    mspeed=mspeed/2;
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    rightJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
                    
                }
                //CCLOG(@"ACCEL LOW: %f",mspeed);
                
            }else if (control==kControlTypeDrag) {
                /******************************************
                 *
                 *
                 *      DRAG  -  LOW VELOCITY
                 *
                 *
                 ******************************************/
                float mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                float limit = 1.2;
                if(controlAssistent == kControlAssistentOff){
                    if(steeringAngle_>0){
                        leftJoint_->SetLimits(0,1.2);
                        rightJoint_->SetLimits(0,1.2);
                    }else{
                        leftJoint_->SetLimits(-1.2,0);
                        rightJoint_->SetLimits(-1.2,0);
                    }
                    mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    leftJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
                    float mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    rightJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
                }else{
                    if(angleToPoint_>0){
                        leftJoint_->SetLimits(-0.3,angleToPoint_+0.1);
                        rightJoint_->SetLimits(-0.3,angleToPoint_+0.1);
                    }else{
                        leftJoint_->SetLimits(angleToPoint_-0.2,0.3);
                        rightJoint_->SetLimits(angleToPoint_-0.2,0.3);
                    }
                    limit = 1.2;
                    mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    mspeed=mspeed/2;
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    leftJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
                    
                    mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                    mspeed=mspeed/2;
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    rightJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity); ;
                }
                
                //CCLOG(@"DRAG LOW: %f",mspeed);
                
                
            }else if (control==kControlTypeHit) {
                /******************************************
                 *
                 *
                 *      HIT  -  LOW VELOCITY
                 *
                 *
                 ******************************************/
                float mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                float limit = 1.4;
                if(controlAssistent == kControlAssistentOff){
                    if(steeringAngle_>0){
                        leftJoint_->SetLimits(0,1.2);
                        rightJoint_->SetLimits(0,1.2);
                    }else{
                        leftJoint_->SetLimits(-1.2,0);
                        rightJoint_->SetLimits(-1.2,0);
                    }
                    mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    leftJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
                    float mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    rightJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
                }else{
                    if(angleToPoint_>0){
                        leftJoint_->SetLimits(-0.3,angleToPoint_+0.1);
                        rightJoint_->SetLimits(-0.3,angleToPoint_+0.1);
                    }else{
                        leftJoint_->SetLimits(angleToPoint_-0.1,0.3);
                        rightJoint_->SetLimits(angleToPoint_-0.1,0.3);
                    }
                    limit = 1.2;
                    mspeed = steeringAngle_- leftJoint_->GetJointAngle();
                    mspeed=mspeed/2;
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    leftJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
                    
                    mspeed = steeringAngle_- rightJoint_->GetJointAngle();
                    mspeed=mspeed/2;
                    if(mspeed>limit)mspeed =  limit;
                    if(mspeed<-limit)mspeed = - limit;
                    rightJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
                }
                
                //CCLOG(@"HIT LOW: %f",mspeed);
            }
        }
        
    }else{
        leftJoint_->SetLimits(-1,1);
        rightJoint_->SetLimits(-1,1);
        
        //Return Steering
        float mspeed = - leftJoint_->GetJointAngle()*4;
        leftJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
        mspeed = - rightJoint_->GetJointAngle()*4;
        rightJoint_->SetMotorSpeed(mspeed * steerSpeedLowVelocity);
        
    }
    
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    if(![[mySingleton deviceType] isEqualToString:@"oldIphone"] ){
        if([self getVelocity]>20){
            float speedDiff = (0.5+ (maxVelocity/40)) - ([self getVelocity]/40);
            //SKIDMARK ME
            //FRONT LEFT TIRE
            BOOL flt=(floor(lw*1000) != floor(leftWheel_->GetAngle()*1000) && (floor(lw*100)-floor(leftWheel_->GetAngle()*100)) > 20 )
            || std::abs(((leftWheel_->GetAngle()*-1)- (carBody->GetAngle()*-1))*10) > speedDiff ?YES:NO;
            //FRONT RIGHT TIRE
            BOOL frt=(floor(rw*1000) != floor(rightWheel_->GetAngle()*1000) && (floor(rw*100) - floor(rightWheel_->GetAngle()*100) >10 || floor(rw*100) - floor(rightWheel_->GetAngle()*100) <-20) )
            || std::abs(((rightWheel_->GetAngle()*-1)- (carBody->GetAngle()*-1))*10) > speedDiff?YES:NO;
            //REAR LEFT TIRE
            BOOL rlt=(floor(lrw*1000) != floor(leftRearWheel_->GetAngle()*1000) && (floor(lrw*100) - floor(leftRearWheel_->GetAngle()*100) >20 || floor(lrw*100) - floor(leftRearWheel_->GetAngle()*100) <-20))
            || std::abs(((leftRearWheel_->GetAngle()*-1)- (carBody->GetAngle()*-1))*10) > speedDiff?YES:NO;
            //REAR RIGHT TIRE
            BOOL rrt=(floor(rrw*1000) != floor(rightRearWheel_->GetAngle()*1000) && (floor(rrw*100) - floor(rightRearWheel_->GetAngle()*100) >20 || floor(rrw*100) - floor(rightRearWheel_->GetAngle()*100) <-20))
            || std::abs(((rightRearWheel_->GetAngle()*-1)- (carBody->GetAngle()*-1))*10) > speedDiff?YES:NO;
            
            
            [self skidmarkMefrt:frt flt:flt rrt:rrt rlt:rlt];
            lw = leftWheel_->GetAngle();
            rw = rightWheel_->GetAngle();
            lrw = leftRearWheel_->GetAngle();
            rrw = rightRearWheel_->GetAngle();
        }
    }
    [game_ displayVelocity:[self getVelocity]];
    [game_ updateSteeringWheel:CC_RADIANS_TO_DEGREES(leftWheel_->GetAngle()*-1)-CC_RADIANS_TO_DEGREES(carBody->GetAngle()*-1)];
    
    
    if(isTurboActive){
        [game_ turboEffectPosition:self.position rotation:CC_RADIANS_TO_DEGREES(carBody->GetAngle()*-1)];
    }
    
    if(isCrashing){
        [game_ crashedEffectPosition:self.position rotation:CC_RADIANS_TO_DEGREES(carBody->GetAngle()*-1)];
    }
    
}

#pragma mark HeroCar - Movements

-(void) move:(CGPoint)direction
{
    engineSpeed_ = -direction.y * horsePowers;
    GameConfiguration *config = [GameConfiguration sharedConfiguration];
    ControlType control = [config controlType];
    if (control==kControlTypeDrag) {
        steeringAngle_ = -direction.x * maxSteerAngle;
    }else if(control==kControlTypeHit){
        steeringAngle_ = -direction.x * maxSteerAngle;
        
        
    }else if(control==kControlTypeAccelerometer){
        
        float drivingAngle = -1* CC_RADIANS_TO_DEGREES(direction.x);
        while(drivingAngle<0)drivingAngle+=360;
        while(drivingAngle>360)drivingAngle-=360;
        
        float carRotation = [self rotation];
        while(carRotation<0)carRotation+=360;
        while(carRotation>360)carRotation-=360;
        
        float angleDifference = carRotation-drivingAngle;
        
        
        while(angleDifference<-180)angleDifference+=360;
        while(angleDifference>180)angleDifference-=360;
        
        // CCLOG(@"Difference - %f",angleDifference);
        
        
        
        float accelx;
        if (angleDifference > 10 ){
            //  accelx = CC_DEGREES_TO_RADIANS(angleDifference-10)/5;
            // CCLOG(@"accelx - %f",accelx);
            // if(accelx > 1) accelx = 1;
            accelx = -1;
        }else if(angleDifference <-10){
            // accelx = CC_DEGREES_TO_RADIANS(angleDifference+10)/5;
            // if(accelx<-1)accelx = -1;
            accelx = 1;
        }else
            accelx = 0;
        
        steeringAngle_ = -accelx;
    }else if(control==kControlTypeDragArrow){
        
        float drivingAngle = -1* CC_RADIANS_TO_DEGREES(direction.x);
        while(drivingAngle<0)drivingAngle+=360;
        while(drivingAngle>360)drivingAngle-=360;
        
        float carRotation = [self rotation];
        while(carRotation<0)carRotation+=360;
        while(carRotation>360)carRotation-=360;
        
        float angleDifference = carRotation-drivingAngle;
        
        
        while(angleDifference<-180)angleDifference+=360;
        while(angleDifference>180)angleDifference-=360;
        
        // CCLOG(@"Difference - %f",angleDifference);
        
        
        
        float accelx;
        if (angleDifference > 10 ){
            //  accelx = CC_DEGREES_TO_RADIANS(angleDifference-10)/5;
            // CCLOG(@"accelx - %f",accelx);
            // if(accelx > 1) accelx = 1;
            accelx = -1;
        }else if(angleDifference <-10){
            // accelx = CC_DEGREES_TO_RADIANS(angleDifference+10)/5;
            // if(accelx<-1)accelx = -1;
            accelx = 1;
        }else
            accelx = 0;
        
        steeringAngle_ = -accelx;
    }
}

-(void) teleportTo:(CGPoint)point
{	
}

-(void) onGameOver:(BOOL)winner
{
}


//OVERIDE


-(void) updateCollisions
{
    isTouchingLadder_ = NO;
    
    // Traverse the contact results.
    int found = 0;
    for (int32 i = 0; i < kMaxContactPoints && found < contactPointCount_; i++)
    {
        ContactPoint* point = contactPoints_ + i;
        b2Fixture *otherFixture = point->otherFixture;
        
        if( otherFixture ) {
            
            found++;
            b2Body* body = otherFixture->GetBody();
            
            BodyNode *node = (BodyNode*) body->GetUserData();
            
            if( [node respondsToSelector:@selector(touchedByHeroWithB2Body:)] )
                [node performSelector:@selector(touchedByHeroWithB2Body:) withObject:(id)self];
            
            else if( [node respondsToSelector:@selector(touchedByHero)] )
                [node performSelector:@selector(touchedByHero)];
            
        }
    }	
}

@end