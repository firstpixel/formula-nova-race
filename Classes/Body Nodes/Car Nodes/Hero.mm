//
//  Hero.mm
//  LevelSVG
//
//  Created by Ricardo Quesada on 03/01/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import <Box2d/Box2D.h>
#import "cocos2d.h"

#import "Joystick.h"
#import "GameNode.h"
#import "GameConfiguration.h"
#import "GameConstants.h"
#import "Hero.h"
#import "BonusNode.h"
#import "AICarTopDown.h"


@interface Hero ()
-(void) readJoystick;
-(void) updateCollisions;
@end

//
// Hero: Base class of the Hero.
// The Hero is the main character, is the sprite that is controlled by the player.
// The base class handles all the collisions, and the input (d-pad or accelerometer)
//
@implementation Hero

@synthesize joystick=joystick_;
@synthesize isBlinking=isBlinking_;
@synthesize totalBoostUsed,totalFixUsed,totalCrashs;

-(id) initWithBody:(b2Body*)body game:(GameNode*)game
{
	if( (self=[super initWithBody:body game:game]) ) {
		
		// listen to beginContact, endContact and presolve
		reportContacts_ = BN_CONTACT_BEGIN | BN_CONTACT_END | BN_CONTACT_PRESOLVE;
		
		// this body can't be dragged
		isTouchable_ = NO;
		
		// It only blinks after touching an enemy
		isBlinking_ = NO;

		// weak ref
		world_ = [game_ world];
		
		
		// hero collisions
		contactPointCount_ = 0;
		
		// hero 
		elapsedTime_ = lastTimeForceApplied_ = 0;
	
		
		// schedule the Hero main loop.
		// Do it "after" the GameNode game loop.
		// This is a workaround so that the bullets are not affected by gravity.
		[self scheduleUpdateWithPriority:10];
	}
	return self;
}

#pragma mark Hero - Contact Listener

//
// To know at any moment the list of contacts of the hero, you should mantain a list based on being/endContact
//
-(void) beginContact:(b2Contact*)contact
{
	// 
	BOOL otherIsA = YES;
	
	b2Fixture* fixtureA = contact->GetFixtureA();
	b2Fixture* fixtureB = contact->GetFixtureB();
	NSAssert( fixtureA != fixtureB, @"Hero: Box2d bug");

	b2WorldManifold worldManifold;
	contact->GetWorldManifold(&worldManifold);
	
	b2Body *bodyA = fixtureA->GetBody();
	b2Body *bodyB = fixtureB->GetBody();
	
	NSAssert( bodyA != bodyB, @"Hero: Box2d bug");
	
	// Box2d doesn't guarantees the order of the fixtures
	otherIsA = (bodyA == body_) ? NO : YES;


	// find empty place
	int emptyIndex;
	for(emptyIndex=0; emptyIndex<kMaxContactPoints;emptyIndex++) {
		if( contactPoints_[emptyIndex].otherFixture == NULL )
			break;
	}
	NSAssert( emptyIndex < kMaxContactPoints, @"LevelSVG: Can't find an empty place in the contacts");
		
	// XXX: should support manifolds
	ContactPoint* cp = contactPoints_ + emptyIndex;
	cp->otherFixture = ( otherIsA ? fixtureA :fixtureB );
	cp->position = b2Vec2_zero;
	cp->normal = otherIsA ? worldManifold.normal : -worldManifold.normal;
	cp->state = b2_addState;
	contactPointCount_++;
}

-(void) endContact:(b2Contact*)contact
{
	b2Fixture* fixtureA = contact->GetFixtureA();
	b2Fixture* fixtureB = contact->GetFixtureB();
	b2Body *body = fixtureA->GetBody();
	
	b2Fixture *otherFixture = (body == body_) ? fixtureB : fixtureA;
	
	int emptyIndex;
	for(emptyIndex=0; emptyIndex<kMaxContactPoints;emptyIndex++) {
		if( contactPoints_[emptyIndex].otherFixture == otherFixture ) {
			contactPoints_[emptyIndex].otherFixture = NULL;
			contactPointCount_--;
			break;
		}
	}	
}

//
// Presolve is needed for one-sided platforms
// If you are not going to use them, you can disable this callback
//
-(void) preSolveContact:(b2Contact*)contact  manifold:(const b2Manifold*) oldManifold
{
	
	
}

#pragma mark Hero - Main Loop
-(void) update:(ccTime)dt
{
	elapsedTime_ += dt;

	GameState state = [game_ gameState];
	if( state == kGameStatePlaying ) {
		[self readJoystick];
		[self updateCollisions];
	}	
}

-(void) readJoystick
{
	if( [joystick_ isPadEnabled] ) {
        CGPoint v = [joystick_ getCurrentNormalizedVelocity];
		if( [joystick_ isGameRotationEnabled] ){
            v = ccp([joystick_ getCurrentNormalizedVelocity].x + CC_DEGREES_TO_RADIANS([game_ rotation]),[joystick_ getCurrentNormalizedVelocity].y);

        }
        [self move:v];
	}
	
	if( [joystick_ isButtonPressed:JOYSTICK_UP] )
		[self jump];
	
	if( [joystick_ isButtonPressed:JOYSTICK_DOWN] )
		[self fire];
}

-(void) updateCollisions
{
	isTouchingLadder_ = NO;

	// Traverse the contact results.
	int found = 0;
	for (int32 i = 0; i < kMaxContactPoints && found < contactPointCount_; i++)
	{
		ContactPoint* point = contactPoints_ + i;
		b2Fixture *otherFixture = point->otherFixture;
     
        if( otherFixture ) {
		
			found++;
			b2Body* body = otherFixture->GetBody();
			
			BodyNode *node = (BodyNode*) body->GetUserData();
			
			
            if( [node respondsToSelector:@selector(touchedByHeroWithB2Body:)] )
                 [node performSelector:@selector(touchedByHeroWithB2Body:) withObject:(id)self];
            
            else if( [node respondsToSelector:@selector(touchedByHero)] )
				[node performSelector:@selector(touchedByHero)];
            
        }
	}	
}

#pragma mark Hero - Movements
-(void) move:(CGPoint)direction
{
	// override me
}


-(void) jump
{
	// override me
}

-(void) fire
{
	// override me
}

-(void) onGameOver:(BOOL)winner
{
	// override me
}

-(void) blinkHero
{
	CCBlink *blink = [CCBlink actionWithDuration:1.5f blinks:10];
	CCSequence *seq = [CCSequence actions:
					   blink,
					   [CCCallFuncN actionWithTarget:self selector:@selector(stopBlinking:)],
					   nil];
	isBlinking_ = YES;
	[self runAction:seq];
}

-(void) teleportTo:(CGPoint)point
{
	body_->SetTransform( b2Vec2( point.x / kPhysicsPTMRatio, point.y/kPhysicsPTMRatio), 0 );
}
	
-(void) stopBlinking:(id)sender
{
	isBlinking_ = NO;
}

-(void) updateFrames:(CGPoint)p
{
	// Override this method if you want to udpate the sprite frame after it has been moved
}

-(void) activateTurbo{

}

-(void) onExit
{
	[super onExit];
	
}

-(void) carCrashed{
    
}
-(void) carFixed{
    
}
@end
