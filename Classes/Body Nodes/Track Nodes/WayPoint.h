//
//  WayPoint.h
//  LevelSVG
//
//  Created by Ricardo Quesada on 03/01/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import "BodyNode.h"
#import "CarTopDown.h"
#import "AICarTopDown.h"

@interface Waypoint : BodyNode {
    int tagid;
    NSValue * waypointPosition;
}
@property (nonatomic,readwrite) int tagid;
// the waypoint object was touched by the hero. What should it do ?
-(void) touchedByHeroWithB2Body:(Cartopdown*)body;
// the waypoint object was touched by the AI. What should it do ?
-(void) touchedByAiWithB2Body:(Aicartopdown*)body;


@end
