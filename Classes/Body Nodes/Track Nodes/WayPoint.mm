//
//  Fruit.mm
//  LevelSVG
//
//  Created by Ricardo Quesada on 03/01/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import <Box2d/Box2D.h>
#import "cocos2d.h"
#import "SimpleAudioEngine.h"

#import "GameNode.h"
#import "GameConstants.h"
#import "Waypoint.h"
#import "CarTopDown.h"
#import "AiCarTopDown.h"
#import "GlobalSingleton.h"

//
// Fruit: a sensor with the shape of an apple
// When touched, it will increase points in 1
//

@implementation Waypoint
@synthesize tagid;
-(id) initWithBody:(b2Body*)body game:(GameNode*)game
{
	if( (self=[super initWithBody:body game:game]) ) {
         game_ = game;
        // bodyNode properties
		reportContacts_ = BN_CONTACT_NONE;
		preferredParent_ = BN_PREFERRED_PARENT_SPRITES_PNG;
		isTouchable_ = NO;
		visible_ = NO;
        
        
       // CCSprite* pin = [CCSprite spriteWithFile:@"ai_pin.png"];
     //   [pin setPosition:CGPointMake((body->GetPosition().x* kPhysicsPTMRatio+(box->GetVertex(2).x* kPhysicsPTMRatio)/2),(body->GetPosition().y* kPhysicsPTMRatio-box->GetVertex(0).y* kPhysicsPTMRatio/2))];
     //   [self addChild:pin];

        
		CGSize size = CGSizeZero;
        b2Fixture *fixture = body->GetFixtureList();
       
        
		fixture->SetSensor(true);
		 
		b2Shape::Type t = fixture->GetType();
         
		if( t ==  b2Shape::e_polygon ) {
			b2PolygonShape *box = dynamic_cast<b2PolygonShape*>(fixture->GetShape());
			if( box->GetVertexCount() == 4 ) {
				size.width = box->GetVertex(2).x * kPhysicsPTMRatio;
				size.height = -box->GetVertex(0).y * kPhysicsPTMRatio;
                
			//	b2Vec2 posi = box->m_centroid;	
            //  [self setTextureRect:CGRectMake(rect_.origin.x-size.width/2, rect_.origin.y-size.height/2, size.width/2, size.height/2)];	
            //  self.position = ccp( body_->GetPosition().x * kPhysicsPTMRatio+size.width/2, body_->GetPosition().y * kPhysicsPTMRatio+size.height/2 );
                
                
                CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"ai_pin.png"];
                [self setDisplayFrame:frame];
                
                //   [game_  addSpriteMark:(body_->GetPosition().x+box->m_centroid.x)*kPhysicsPTMRatio y:(body_->GetPosition().y -box->m_centroid.y)* kPhysicsPTMRatio];
               // [game_  addSpriteMark:(body_->GetPosition().x+box->m_centroid.x)* kPhysicsPTMRatio y:(body_->GetPosition().y-box->m_centroid.y)* kPhysicsPTMRatio];
                /* CCSprite* sprite = [CCSprite spriteWithSpriteFrame:frame];
                sprite.position = ccp( box->m_centroid.x*kPhysicsPTMRatio, box->m_centroid.y * kPhysicsPTMRatio );
                sprite.anchorPoint = ccp(0,0);
                [self addChild:sprite];               
                */
               } else
				CCLOG(@"LevelSVG: Platform with unsupported number of vertices: %d", box->GetVertexCount() );
		} else
			CCLOG(@"LevelSVG: Platform with unsupported shape type");
	}
	return self;
   
}

-(void) setParameters:(NSDictionary *)params
{
	[super setParameters:params];
    tagid = [[params objectForKey:@"tagid"] intValue];
   // [game_ setWayPointByTag:tagid atPosition:waypointPosition];				
  //  GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
   // [mySingleton setTotalWayPoints:[mySingleton totalWayPoints]+1];
   // NSLog(@"totalwaypoints %i", [mySingleton totalWayPoints]);
}

-(void) touchedByHeroWithB2Body:(Cartopdown*)body
{
    
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    
    if( [body isKindOfClass:[Cartopdown class]]) {
        if((int)tagid==(int)[body nextWayPoint]){
            //END RACE ANIMATION
            if((int)tagid==0 && [body myLap]==[mySingleton totalLaps]){
                int heroPos = [(Cartopdown*)[game_ hero] racePosition];
                //DisplayMessage race classification
                //set hero end on singleton to get updates from other racers on the hud race ended display
                [game_ displayRaceEnd];
                if(heroPos==1)[[SimpleAudioEngine sharedEngine] playEffect: @"finishLine.mp3"];
            }
            
            
            if((int)tagid==(int)[mySingleton totalWayPoints]-1){
                [body setNextWayPoint:0];
            }else{
                [body setNextWayPoint:(tagid+1)];
            }
            if((int)tagid==0 && [body myLap]<=[mySingleton totalLaps]){
                //if([body myLap]!=0) [game_ updateLap];
                [body setMyLap:([body myLap]+1)];
                [game_ updateLap];
                
            }
        }
    } 
    
}

-(void) touchedByAiWithB2Body:(Aicartopdown*)body
{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    
	 if( [body isKindOfClass:[Aicartopdown class]]) {
        if((int)tagid==(int)[body nextWayPoint]){
            //END RACE ANIMATION
            if((int)tagid==0 && [body myLap]==[mySingleton totalLaps]){
                int ai = 0;
                if([body carid]==1)ai = [(Aicartopdown*)[game_ ai1] racePosition];
                if([body carid]==2)ai = [(Aicartopdown*)[game_ ai2] racePosition];
                if([body carid]==3)ai = [(Aicartopdown*)[game_ ai3] racePosition];
                if([body carid]==4)ai = [(Aicartopdown*)[game_ ai4] racePosition];
                if([body carid]==5)ai = [(Aicartopdown*)[game_ ai5] racePosition];
                //Champ
                if(ai==1)[[SimpleAudioEngine sharedEngine] playEffect: @"finishLine.mp3"];
                
                //set singleton classification for each one that finished, 
                //if hero end, update display
                //DisplayMessage race classification
                // [game_ displayRaceEnd];
            }
            if((int)tagid==0 && [body myLap]<=[mySingleton totalLaps]){
                [body setMyLap:([body myLap]+1)];    
            }
            GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
            if((int)tagid==(int)[mySingleton totalWayPoints]-1){
                [body setNextWayPoint:0];
            }else{
                [body setNextWayPoint:(tagid+1)];
            }

        }
        
      //  NSLog(@"Ai Lap %i nextWayPoint %i",[body myLap],[body nextWayPoint]);
        
    }

    
    //[[SimpleAudioEngine sharedEngine] playEffect: @"pickup_coin.wav"];
}
@end