//
//  WayPointMarker.h
//  SillyRace
//
//  Created by Gil Beyruth on 7/4/11.
//  Copyright 2011 Sapus Media. All rights reserved.
//



#import "BodyNode.h"

@interface Waypointmarker : BodyNode {
    int tagid;
    NSValue* waypointPosition;
}
@property (nonatomic,readwrite) int tagid;


@end