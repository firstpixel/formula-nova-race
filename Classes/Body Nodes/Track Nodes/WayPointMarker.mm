//
//  WayPointMarker.mm
//  SillyRace
//
//  Created by Gil Beyruth on 7/4/11.
//  Copyright 2011 Sapus Media. All rights reserved.
//


#import <Box2d/Box2D.h>
#import "cocos2d.h"
#import "SimpleAudioEngine.h"

#import "GameNode.h"
#import "GameConstants.h"
#import "Waypointmarker.h"
#import "GlobalSingleton.h"

//
// Fruit: a sensor with the shape of an apple
// When touched, it will increase points in 1
//

@implementation Waypointmarker
@synthesize tagid;
-(id) initWithBody:(b2Body*)body game:(GameNode*)game
{
	
    if( (self=[super initWithBody:body game:game] ) ) {
        CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"ai_pin.png"];
        [self setDisplayFrame:frame];
        game_ = game;
		reportContacts_ = BN_CONTACT_NONE;
		preferredParent_ = BN_PREFERRED_PARENT_SPRITES_PNG;
		isTouchable_ = NO;
		visible_ = NO;
		// 1. destroy already created fixtures
		[self destroyAllFixturesFromBody:body];
		
		// 2. create new fixture
		b2CircleShape	shape;
		shape.m_radius = 0.25f;		// 0.5 meter of diameter
		b2FixtureDef	fd;
		fd.shape = &shape;
		fd.isSensor = true;			// it is a sensor
		body->CreateFixture(&fd);
        waypointPosition = [NSValue valueWithCGPoint:CGPointMake(body_->GetPosition().x,body_->GetPosition().y)];
        [game_ addSpriteMark:(body_->GetPosition().x)* kPhysicsPTMRatio y:(body_->GetPosition().y)* kPhysicsPTMRatio];
	}
	return self;
    
}

-(void) setParameters:(NSDictionary *)params
{
	[super setParameters:params];
    tagid = [[params objectForKey:@"tagid"] intValue];
    [game_ setWayPointByTag:tagid atPosition:waypointPosition];				
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setTotalWayPoints:[mySingleton totalWayPoints]+1];
}



@end