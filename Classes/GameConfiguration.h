//
//  GameConfiguration.h
//  LevelSVG
//
//  Created by Ricardo Quesada on 13/10/09.
//  Copyright 2009 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//


#import <Foundation/Foundation.h>

typedef enum
{
	kControlTypeDrag,
	kControlTypeHit,
    kControlTypeDragArrow,
	kControlTypeAccelerometer,
	
	kControlTypeDefault = kControlTypeDragArrow,
    
} ControlType;

typedef enum
{
	kControlAssistentOn,
	kControlAssistentOff,
	
	kControlAssistentDefault = kControlAssistentOn,
    
} ControlAssistent;

typedef enum
{
	kControlButton0,
	kControlButton1,
	kControlButton2,
	
	kControlButtonDefault = kControlButton1,
} ControlButton;

typedef enum {
	kControlDirection2Way,
	kControlDirection4Way,
	kControlDirection4WayCar,
	
	kControlDirectionDefault = kControlDirection4WayCar,
} ControlDirection;

@interface GameConfiguration : NSObject
{
	ControlType			controlType_;
	ControlAssistent	controlAssistent_;
	ControlDirection	controlDirection_;
	ControlButton		controlButton_;
	CGPoint				gravity_;
	BOOL				enableWireframe_;
    BOOL				drivingAssistent_;
};

@property (nonatomic,readwrite)	ControlType			controlType;
@property (nonatomic,readwrite)	ControlAssistent	controlAssistent;
@property (nonatomic,readwrite)	ControlDirection	controlDirection;
@property (nonatomic,readwrite)	ControlButton		controlButton;
@property (nonatomic,readwrite) CGPoint				gravity;
@property (nonatomic,readwrite) BOOL				enableWireframe;
@property (nonatomic,readwrite) BOOL				drivingAssistent;

// returns the singleton
+(id) sharedConfiguration;

@end