//
//  GameNode.h
//  LevelSVG
//
//  Created by Ricardo Quesada on 12/08/09.
//  Copyright 2009 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "Box2D.h"

#import "GLES-Render.h"
#import "Box2DCallbacks.h"
#import "GameConstants.h"

// forward declarations
@class HUD;
@class BodyNode;
@class Hero;
@class AI;
@class Aicartopdown;
@class Cartopdown;
@class BonusNode;
@class GhostCar;

// game state
typedef enum
{
	kGameStatePaused,
    kGameStateWarmUp,
	kGameStatePlaying,
	kGameStateGameOver,
} GameState;

#define kGameNodeFollowActionTag	1

// HelloWorld Layer
@interface GameNode : CCLayer
{
	// box2d world
	b2World		*world_;
	
	// game state
	GameState		gameState_;
	
	// the camera will be centered on the Hero
	// If you want to move the camera, you should move this value
	CGPoint		cameraOffset_;
	
	// game scores
	unsigned int	score_;
	// game lives
	unsigned int	lives_;
	
	// Hero weak ref
	Hero	*hero_;
	
    // Hero weak ref
	AI	*ai1_;
	AI	*ai2_;
	AI	*ai3_;
	AI	*ai4_;
	AI	*ai5_;
	
    GhostCar *ghostCar_;
    
    int time;
    int timeGhost, lastTimeGhost;
    int lastIndexGhost;
    NSMutableString* timeString;
    NSMutableArray* wayPointsList;
    
    NSMutableArray * ghostArray;
    NSMutableArray * ghostPlaying;
    NSMutableArray * ghostLapArray;
    NSMutableArray * ghostBestLap;

	// HUD weak ref
	HUD		*hud_;
   
	// Box2d: Used when dragging objects
	b2MouseJoint	* mouseJoint_;
	b2Body			* mouseStaticBody_;
	
	// box2d callbacks
	// In order to compile on SDK 2.2.x or older, they have to be pointers
	MyContactFilter			*m_contactFilter;
	MyContactListener		*m_contactListener;
	MyDestructionListener	*m_destructionListener;
    // box2d iterations. Can be configured by each level
	int	worldPositionIterations_;
	int worldVelocityIterations_;
    CCSprite* ghostSpriteCar;
	// GameNode is responsible for removing "removed" nodes
	unsigned int nukeCount;
	b2Body* nuke[kMaxNodesToBeRemoved];	
}
/** Time String */
@property (readwrite,nonatomic,retain) NSMutableString* timeString;

/** Box2d World */
@property (readwrite,nonatomic) b2World *world;

/** score of the game */
@property (readonly,nonatomic) unsigned int score;

/** lives of the hero */
@property (readonly,nonatomic) unsigned int lives;

/** game state */
@property (readonly,nonatomic) GameState gameState;

/** weak ref to hero */
@property (readwrite,nonatomic,assign) Hero *hero;

/** weak ref to AIArray */
@property (readwrite,nonatomic,assign) AI *ai1;
@property (readwrite,nonatomic,assign) AI *ai2;
@property (readwrite,nonatomic,assign) AI *ai3;
@property (readwrite,nonatomic,assign) AI *ai4;
@property (readwrite,nonatomic,assign) AI *ai5;

/** weak ref to HUD */
@property (readwrite, nonatomic, assign) HUD *hud;

/** offset of the camera */
@property (readwrite,nonatomic) CGPoint cameraOffset;

/** game timer */
@property (readwrite,nonatomic) int time;

/** ghost timer */
@property (readwrite,nonatomic) int timeGhost, lastTimeGhost;


// returns a Scene that contains the GameLevel and a HUD
+(id) scene;

// initialize game with level
-(id) init;

/** returns the SVGFileName to be loaded */
-(NSString*) SVGFileName;

-(void) update: (ccTime) dt;

// mouse (touches)
-(BOOL) mouseDown:(b2Vec2)p;
-(void) mouseMove:(b2Vec2)p;
-(void) mouseUp:(b2Vec2)p;

// game events
-(void) gameOver;
-(void) updateLap;
-(void) resetTimer;
-(void) displayRaceEnd;
-(void) wrongWayAlert:(float)angle;

-(void)setWayPointByTag:(int)tagid atPosition:(NSValue*)point;
-(NSValue*)getWayPointByTag:(int)tagid;



// creates the foreground and background graphics
-(void) initGraphics;

// adds the BodyNode to the scene graph
-(void) addBodyNode:(BodyNode*)node z:(int)zOrder;


-(void) displayVelocity:(float)velo;
-(void) updateSteeringWheel:(float)angle;

// adds the SkidMarks to the scene graph
-(void)initSkidMarks;
// adds the BodyNode to the scene graph
-(void) addSkidMarkAt:(CGPoint)posi angle:(float)angle;

-(void) addSmokeRightSkidmark:(CGPoint)posi angle:(float)angle;
-(void) addSmokeLeftSkidmark:(CGPoint)posi angle:(float)angle;
-(void) addSpriteMark:(float) x y:(float) y;

- (void)initTurboEffect;
- (void)turboEffectPosition:(CGPoint)posi rotation:(float)angle;
- (void)removeTurboEffect;

- (void)initCrashedEffect;
- (void)crashedEffectPosition:(CGPoint)posi rotation:(float)angle;
- (void)removeCrashedEffect;

-(void)initExplosionEffect;
-(void)removeExplosionEffect;

// schedule a b2Body to be removed
-(void) removeB2Body:(b2Body*)body;

// returns the content Rectangle of the Map
-(CGRect) contentRect;

-(void)debugAngle:(float)angle;
-(void)updateLoop;
@end
