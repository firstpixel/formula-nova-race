//
//  GameNode.mm
//  LevelSVG
//
//  Created by Ricardo Quesada on 12/08/09.
//  Copyright 2009 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//

//
// This class implements the game logic like:
//
//	- scores
//	- lives
//	- updates the Box2d world
//  - object creation
//  - renders background and sprites
//  - register touch event: supports dragging box2d's objects
//

// sound imports
#import "SimpleAudioEngine.h"


// Import the interfaces
#import "GameNode.h"
#import "SVGParser.h"
#import "GameConstants.h"
#import "Box2DCallbacks.h"
#import "GameConfiguration.h"
#import "HUD.h"
#import "BodyNode.h"
#import "AI.h"
#import "AICarTopDown.h"
#import "Hero.h"
#import "CarTopDown.h"
#import "Box2dDebugDrawNode.h"
#import "BonusNode.h"
#import "GlobalSingleton.h"
#import "GhostCar.h"
#import "GhostStatus.h"

@interface GameNode ()
-(void) initPhysics;
-(void) initGraphics;
-(void) updateSprites;
-(void) updateCamera;
-(void) removeB2Bodies;

@end

// HelloWorld implementation
@implementation GameNode

@synthesize world=world_;
@synthesize score=score_, lives=lives_;
@synthesize gameState=gameState_;
@synthesize hero=hero_;
@synthesize ai1=ai1_;
@synthesize ai2=ai2_;
@synthesize ai3=ai3_;
@synthesize ai4=ai4_;
@synthesize ai5=ai5_;
@synthesize hud=hud_;
@synthesize cameraOffset=cameraOffset_;
@synthesize time;
@synthesize timeGhost;
@synthesize lastTimeGhost;
@synthesize timeString;

#pragma mark GameNode -Initialization

+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'game' is an autorelease object.
	GameNode *game = [self node];

	// HUD
	HUD *hud = [HUD HUDWithGameNode:game];
	[scene addChild:hud z:10 tag:10];
	
	// link gameScene with HUD
	game.hud = hud;
 
    //reset waypoints singleton
    
	// add game as a child to scene
	[scene addChild: game];
    
    
	
	// return the scene
	return scene;
}

// initialize your instance here
-(id) init
{
	if( (self=[super init])) {
        // enable touches
		self.isTouchEnabled = YES;
        wayPointsList = [[NSMutableArray alloc] initWithCapacity:30];
        int i=0;
        while(i<30){
            [wayPointsList insertObject:@"" atIndex:i];
            i++;
        }
        ghostArray = [[[NSMutableArray alloc] init] retain];
        ghostPlaying = [[[NSMutableArray alloc] init] retain];
        ghostBestLap = [[[NSMutableArray alloc] init] retain];
        //ghostLapArray = [[[NSMutableArray alloc] initWithCapacity:[[GlobalSingleton sharedInstance] totalLaps]+1] retain];
        
        /*int g=0;
        while(g<[[GlobalSingleton sharedInstance] totalLaps]+1){
            [ghostLapArray insertObject:@"" atIndex:g];
            g++;
        }
         */
        
		GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
        [mySingleton setTotalLaps:3];
        [mySingleton setTotalWayPoints:0];
       
        score_ = 0;
		lives_ = 5;
		hero_ = nil;
		ai1_ = nil;
        ai2_ = nil;
        ai3_ = nil;
        ai4_ = nil;
        ai5_ = nil;
        time = 0;
        timeGhost = 0;
        lastTimeGhost = 0;
        lastIndexGhost = 0;
        // game state
		gameState_ = kGameStatePaused;
        
        if([[GlobalSingleton sharedInstance] gameType] == 3){
            ghostPlaying = [[[GlobalSingleton sharedInstance] getGhostBestLapForTrack:[[GlobalSingleton sharedInstance] selectedTrackInt]] mutableCopy];
        }
        
		
		// camera
		cameraOffset_ = CGPointZero;

		CGSize screenSize = [CCDirector sharedDirector].winSize;
		CCLOG(@"Screen width %0.2f screen height %0.2f",screenSize.width,screenSize.height);
		
		// init box2d physics
		[self initPhysics];
		
		// Init graphics
		[self initGraphics];
		
		// default physics settings
		SVGParserSettings settings;
		settings.defaultDensity = kPhysicsDefaultDensity;
		settings.defaultFriction = kPhysicsDefaultFriction;
		settings.defaultRestitution = kPhysicsDefaultRestitution;
		settings.PTMratio = kPhysicsPTMRatio;
		settings.defaultGravity = ccp( kPhysicsWorldGravityX, kPhysicsWorldGravityY );
		settings.bezierSegments = kPhysicsDefaultBezierSegments;
		
        // create box2d objects from SVG file in world
       
        if([[GlobalSingleton sharedInstance] gameType] == 1){
            [SVGParser parserWithSVGFilename:[self SVGFileName] b2World:world_ settings:&settings target:self selector:@selector(physicsCallbackWithBody:attribs:)];
        }else if([[GlobalSingleton sharedInstance] gameType] == 2 || [[GlobalSingleton sharedInstance] gameType] == 3){
            // create box2d objects from SVG file in world
            [SVGParser parserWithSVGFilename:[self SVGFileName] b2World:world_ settings:&settings target:self selector:@selector(physicsCallbackSingleWithBody:attribs:)];
            //reset ghost car data
            ghostCar_ = [GhostCar sharedInstance];
        }
        
		// Box2d iterations default values
		worldVelocityIterations_ = 6;
		worldPositionIterations_ = 6;
		
		// nodes to be removed
		nukeCount = 0;

		[self scheduleUpdateWithPriority:0];
	}
	return self;
}

-(void) carPosition:(double)dt{
    NSArray* carsArray;
    
    /****************************************************************/
    /*********************    GHOST CODE    *************************/
    /****************************************************************/
    
    [(Cartopdown *)hero_ setRacePosition:1];
    int lap = [(Cartopdown *)hero_ myLap];
    
    timeGhost += dt * 1000;
    // NSLog(@"SAVE old---------------- %i :%i  : %@ , %f", lap, lastTimeGhost, [ghostCar_ timeToString:lastTimeGhost], dt);
    // NSLog(@"SAVE new---------------- %i :%i  : %@ , %f", lap, timeGhost, [ghostCar_ timeToString:timeGhost], dt);
    int indexCount = (timeGhost/1000);
    
    if(indexCount == 0 && lastIndexGhost > 1 && lastIndexGhost != indexCount && lap > 1 ){
        if(lap <= [[GlobalSingleton sharedInstance] totalLaps] + 1){
            GhostStatus *ghostLastStatus_ = [[GhostStatus alloc] autorelease];
            ghostLastStatus_.car = [[GlobalSingleton sharedInstance] carTypeP1];
            ghostLastStatus_.time = lastTimeGhost;
            ghostLastStatus_.index = lastIndexGhost;
            ghostLastStatus_.position = hero_.position;
            ghostLastStatus_.rotation = hero_.rotation;
            unsigned long lastIndex = (ghostArray.count) - 1;
            if(lastIndex==lastIndexGhost){
                [ghostArray replaceObjectAtIndex:lastIndexGhost withObject:ghostLastStatus_];
            }else{
                [ghostArray insertObject:ghostLastStatus_ atIndex:lastIndexGhost];
            }
            NSMutableArray *ghostLastLapArray = [ghostArray mutableCopy];
            //GHOST RACE
            //[ghostLapArray insertObject:ghostLastLapArray atIndex:(lap-1)];
            GhostStatus *gLastLap =[ghostLastLapArray lastObject];
            GhostStatus *gBestLap =[ghostBestLap lastObject];
            
            if((gBestLap != nil && lap > 2 && gBestLap.time > gLastLap.time) || (lap == 2 && gLastLap!=nil) ){
                [ghostBestLap removeAllObjects];
                if(ghostBestLap!=nil) [ghostBestLap release];
                ghostBestLap = [[NSMutableArray arrayWithArray:ghostLastLapArray] retain];
                [[GlobalSingleton sharedInstance] saveGhostBestLap:ghostLastLapArray forTrack:[[GlobalSingleton sharedInstance] selectedTrackInt]];
                [[GlobalSingleton sharedInstance] unlockItem:[NSString stringWithFormat:@"ghost%i",[[GlobalSingleton sharedInstance] selectedTrackInt]]];
            }
        }
        
        if( lap == ([[GlobalSingleton sharedInstance] totalLaps]+1)){
            [[GlobalSingleton sharedInstance] saveGhostBestLap:ghostBestLap forTrack:[[GlobalSingleton sharedInstance] selectedTrackInt]];
            if(ghostBestLap!=nil) [ghostBestLap release];
            //GHOST RACE
            //[[GlobalSingleton sharedInstance] saveGhostRace:ghostLapArray forTrack:[[GlobalSingleton sharedInstance] selectedTrackInt] withCar:[[GlobalSingleton sharedInstance] carTypeP1]];
            NSArray* ghostCarBestLapArray = [[GlobalSingleton sharedInstance] getGhostBestLapForTrack:[[GlobalSingleton sharedInstance] selectedTrackInt]];
            GhostStatus* status = [ghostCarBestLapArray lastObject];
            NSLog(@"BEST LAP : %i",status.time);
            [ghostArray removeAllObjects];
            if(ghostArray!=nil){
                [ghostArray release];
            }
        } else {
            //clear array
            [ghostArray removeAllObjects];
        }
    }
    
    GhostStatus *ghostStatus_ = [[GhostStatus alloc] autorelease];
    ghostStatus_.car = [[GlobalSingleton sharedInstance] carTypeP1];
    ghostStatus_.time = timeGhost;
    ghostStatus_.index = indexCount;
    ghostStatus_.position = hero_.position;
    ghostStatus_.rotation = hero_.rotation;
    
    if(lastIndexGhost != indexCount && lap > 0 && lap < [[GlobalSingleton sharedInstance] totalLaps] + 1){
        [ghostArray insertObject:ghostStatus_ atIndex:indexCount];
    }

    
    /****************************************************************/
    /*********************    GHOST CODE END   **********************/
    /****************************************************************/
    
    
    
    if([[GlobalSingleton sharedInstance] gameType] == 1){
        carsArray = [NSArray arrayWithObjects:ai1_,ai2_,ai3_,ai4_,ai5_,hero_, nil];
        int total = (int)[carsArray count];

        //reset all positions to last places
        for(int i=0;i < total;i++){
            [[carsArray objectAtIndex:i] setRacePosition:(int)[carsArray count]];
        }

       for(int i=0;i < total;i++){
                for(int j=0;j < total;j++){
                        if(j!=i){
                           //lap is bigger
                            if([[carsArray objectAtIndex:i] myLap] > [[carsArray objectAtIndex:j] myLap]){
                                
                                [[carsArray objectAtIndex:i] setRacePosition:[[carsArray objectAtIndex:i] racePosition]-1];
                                 //NSLog(@"//lap is bigger index %i - %i pos: %i",i,j,[[carsArray objectAtIndex:i] racePosition]);
                                
                            }else if([[carsArray objectAtIndex:i] myLap] == [[carsArray objectAtIndex:j] myLap] 
                                     &&  [[carsArray objectAtIndex:i] nextWayPoint]==0){
                                //same lap 
                                if([[carsArray objectAtIndex:i] myLap] == [[carsArray objectAtIndex:j] myLap]){
                                    //same waypoint
                                    if([[carsArray objectAtIndex:i] nextWayPoint]==[[carsArray objectAtIndex:j] nextWayPoint]){
                                            //closest to nextWayPoint 
                                        if([[carsArray objectAtIndex:i] nextWayPointDistance]<[[carsArray objectAtIndex:j] nextWayPointDistance]){
                                                [[carsArray objectAtIndex:i] setRacePosition:[[carsArray objectAtIndex:i] racePosition]-1]; 
                                                //  NSLog(@" //closest to nextWayPoint  index %i - %i pos: %i",i,j,[[carsArray objectAtIndex:i] racePosition]);   
                                        }
                                    }else{
                                        //others are behind 
                                        [[carsArray objectAtIndex:i] setRacePosition:[[carsArray objectAtIndex:i] racePosition]-1]; 
                                    }
                                }
                            
                            }else{
                                //same lap
                                if([[carsArray objectAtIndex:i] myLap] == [[carsArray objectAtIndex:j] myLap]){
                                    //waypoint is bigger
                                    if([[carsArray objectAtIndex:i] nextWayPoint]>[[carsArray objectAtIndex:j] nextWayPoint]){
                                        [[carsArray objectAtIndex:i] setRacePosition:[[carsArray objectAtIndex:i] racePosition]-1]; 
                                      //  NSLog(@" //waypoint is bigger index %i  - %i pos: %i",i,j,[[carsArray objectAtIndex:i] racePosition]);
                                    } else
                                        //same waypoint
                                    if([[carsArray objectAtIndex:i] nextWayPoint]==[[carsArray objectAtIndex:j] nextWayPoint]){
                                        //closest to nextWayPoint 
                                        if([[carsArray objectAtIndex:i] nextWayPointDistance]<[[carsArray objectAtIndex:j] nextWayPointDistance]){
                                            [[carsArray objectAtIndex:i] setRacePosition:[[carsArray objectAtIndex:i] racePosition]-1]; 
                                          //  NSLog(@" //closest to nextWayPoint  index %i - %i pos: %i",i,j,[[carsArray objectAtIndex:i] racePosition]);
                                        
                                        }
                                    }
                                }
                            }
                    }
                }
        }
    } else if([[GlobalSingleton sharedInstance] gameType] == 2){
        carsArray= [NSArray arrayWithObjects:hero_, nil];
        //NSLog(@"Race to make your time");
    } else if([[GlobalSingleton sharedInstance] gameType] == 3){
       // NSLog(@"Race against your ghost");
        if((lastIndexGhost=nil || lastIndexGhost!=indexCount) && indexCount < ghostPlaying.count-1){
            GhostStatus* actualStatus = [ghostPlaying objectAtIndex: indexCount];
            GhostStatus* actualNextStatus = [ghostPlaying objectAtIndex: (indexCount+1)];
            
            if(indexCount == 0){
                [ghostSpriteCar setPosition:actualStatus.position];
                [ghostSpriteCar setRotation:actualStatus.rotation];
                while(actualNextStatus.rotation>360){
                    actualNextStatus.rotation = actualNextStatus.rotation -360;
                }
                NSLog(@"Position [%i] : (%f ,  %f) angle: %d", indexCount ,actualStatus.position.x,actualStatus.position.y, actualStatus.rotation);
                
                CCRotateTo* rotateTo = [CCRotateTo actionWithDuration:1 angle:actualNextStatus.rotation];
                CCMoveTo* moveTo = [CCMoveTo actionWithDuration:1 position:actualNextStatus.position];
                CCSpawn* spawn = [CCSpawn actions:rotateTo,moveTo, nil];
                [ghostSpriteCar runAction:spawn];
            }else if(indexCount == (ghostPlaying.count-2)){
                while(actualNextStatus.rotation>360){
                    actualNextStatus.rotation = actualNextStatus.rotation -360;
                }
                NSLog(@"Position [%i] : (%f ,  %f) angle: %d -- time: %i", indexCount ,actualStatus.position.x,actualStatus.position.y, actualStatus.rotation, actualStatus.time);
                NSLog(@"LAST POSITION [%i] : (%f ,  %f) angle: %d -- time: %i", (indexCount+1) ,actualNextStatus.position.x,actualNextStatus.position.y, actualNextStatus.rotation, actualNextStatus.time);
                int gLastTime = indexCount * 1000;
                float gTimeToEnd = (actualNextStatus.time - gLastTime);
                float gMilliTimeToEnd = gTimeToEnd/1000;
                NSLog(@"TIME TO END  [%f] - [%i]  [%f] ",gTimeToEnd, gLastTime,  gMilliTimeToEnd);
                CCRotateTo* rotateTo = [CCRotateTo actionWithDuration:gMilliTimeToEnd angle:actualNextStatus.rotation];
                CCMoveTo* moveTo = [CCMoveTo actionWithDuration:gMilliTimeToEnd position:actualNextStatus.position];
                CCSpawn* spawn = [CCSpawn actions:rotateTo,moveTo, nil];
                [ghostSpriteCar runAction:spawn];
            }else{
                while(actualNextStatus.rotation>360){
                    actualNextStatus.rotation = actualNextStatus.rotation -360;
                }
                NSLog(@"Position [%i] : (%f ,  %f) angle: %d", indexCount ,actualStatus.position.x,actualStatus.position.y, actualStatus.rotation);
                CCRotateTo* rotateTo = [CCRotateTo actionWithDuration:1 angle:actualNextStatus.rotation];
                CCMoveTo* moveTo = [CCMoveTo actionWithDuration:1 position:actualNextStatus.position];
                CCSpawn* spawn = [CCSpawn actions:rotateTo,moveTo, nil];
                [ghostSpriteCar runAction:spawn];
            }
            
            
        }
        carsArray= [NSArray arrayWithObjects:hero_, nil];
        
    } else if([[GlobalSingleton sharedInstance] gameType] == 4){
        carsArray= [NSArray arrayWithObjects:hero_, ghostCar_, nil];
        NSLog(@"Race against friends ghost");
    }

   // NSLog(@"HERO P %i",[[carsArray objectAtIndex:(int)[carsArray count]-1] racePosition]);
    [hud_ displayPosition];
    
    
    lastIndexGhost = indexCount;
    lastTimeGhost = timeGhost;
}

-(void)setGameStateToPlaying{
    gameState_ = kGameStatePlaying;
    //[self scheduleUpdateWithPriority:0];
}
-(void) onEnterTransitionDidFinish
{
	[super onEnterTransitionDidFinish];
	gameState_ = kGameStateWarmUp;
	
	CGRect rect = [self contentRect];
	CCFollow *action = [CCFollow actionWithTarget:hero_ worldBoundary:rect];
	[action setTag:kGameNodeFollowActionTag];
    
    [hud_ displayWarmUp];
    /*
    id action2 = [CCCallBlock actionWithBlock:^{
        gameState_ = kGameStatePlaying;
    }];
     */
    id action2 = [CCCallFunc actionWithTarget:self selector:@selector(setGameStateToPlaying)];
    //create sequence to have a delay, and then run the callblock action:
    id sequence = [CCSequence actions:[CCDelayTime actionWithDuration:4],action2, nil];
    //run action:
    [self runAction:sequence];
	[self runAction:action];
}

-(void) initGraphics
{
	CCLOG(@"LevelSVG: GameNode#initGraphics: override me");
}

-(NSString*) SVGFileName
{
	CCLOG(@"LevelSVG: GameNode:SVGFileName: override me");
	return nil;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	
	// physics stuff
	if( world_ )
		delete world_;
	
	// delete box2d callback objects
	if( m_contactListener )
		delete m_contactListener;
	if( m_contactFilter )
		delete m_contactFilter;
	if( m_destructionListener )
		delete m_destructionListener;
	
	// don't forget to call "super dealloc"
	[super dealloc];	
}

-(void) registerWithTouchDispatcher
{
	// Priorities: lower number, higher priority
	// Joystick: 10
	// GameNode (dragging objects): 50
	// HUD (dragging screen): 100
	[[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:10 swallowsTouches:YES];
}

-(void) initPhysics
{
	// Define the gravity vector.
	b2Vec2 gravity;
	gravity.Set(0.0f, -10.0f);
		
	// Construct a world object, which will hold and simulate the rigid bodies.
	world_ = new b2World(gravity);
    
    // Do we want to let bodies sleep?
	// This will speed up the physics simulation
	world_->SetAllowSleeping(true);
	
	world_->SetContinuousPhysics(true);
	
	// contact listener
	m_contactListener = new MyContactListener();
	world_->SetContactListener( m_contactListener );
	
	// contact filter
//	m_contactFilter = new MyContactFilter();
//	world_->SetContactFilter( m_contactFilter );
	
	// destruction listener
	m_destructionListener = new MyDestructionListener();
	world_->SetDestructionListener( m_destructionListener );
	
	// init mouse stuff
	mouseJoint_ = NULL;
	b2BodyDef	bodyDef;
	mouseStaticBody_ = world_->CreateBody(&bodyDef);	
}

-(CGRect) contentRect
{
	CCLOG(@"GameNode#contentRect");
	
	NSAssert( NO, @"You override this method in your Level class. It should return the rect that contains your map");
	return CGRectMake(0,0,0,0);
}

#pragma mark GameNode - MainLoop
-(void) update: (ccTime) dt
{
	// Only step the world if status is Playing or GameOver
	if( gameState_ != kGameStatePaused ) {
		
		//It is recommended that a fixed time step is used with Box2D for stability
		//of the simulation, however, we are using a variable time step here.
		//You need to make an informed choice, the following URL is useful
		//http://gafferongames.com/game-physics/fix-your-timestep/
		
		// Instruct the world to perform a single step of simulation. It is
		// generally best to keep the time step and iterations fixed.
		world_->Step(dt, worldVelocityIterations_, worldPositionIterations_ );
        
        time += dt * 1000;
       // timeString = [NSString stringWithString:[self timeToString:time]];
       // NSLog(@"TIME STRING %@",timeString);
	}

	// removed box2d bodies scheduled to be removed
	[self removeB2Bodies];
	 
	// update cocos2d sprites from box2d world
	[self updateSprites];
	
	// update camera
	[self updateCamera];
    [self carPosition:dt];
    [self updateLoop];
}

-(void) removeB2Body:(b2Body*)body
{
	NSAssert( nukeCount < kMaxNodesToBeRemoved, @"LevelSVG: Increase the kMaxNodesToBeRemoved in GameConstants.h");
	nuke[nukeCount++] = body;
}

-(void) removeB2Bodies
{
	// Sort the nuke array to group duplicates.
	std::sort(nuke, nuke + nukeCount);
	
	// Destroy the bodies, skipping duplicates.
	unsigned int i = 0;
	while (i < nukeCount)
	{
		b2Body* b = nuke[i++];
		while (i < nukeCount && nuke[i] == b)
		{
			++i;
		}

		// IMPORTANT: don't alter the order of the following commands, or it might crash.
		
		// 1. obtain a weak ref to the BodyNode
		BodyNode *node = (BodyNode*) b->GetUserData();
		
		// 2. destroy the b2body
		world_->DestroyBody(b);

		// 3. set the the body to NULL
		[node setBody:NULL];
		
		// 4. remove BodyNode
		[node removeFromParentAndCleanup:YES];
	}
	
	nukeCount = 0;
}

-(void)setWayPointByTag:(int)tagid atPosition:(NSValue*)point
{
    [wayPointsList replaceObjectAtIndex:tagid withObject:point]; 
    // NSLog(@"WAYPOINT[%i]:X %f Y:%f",tagid,[point CGPointValue].x,[point CGPointValue].y);
}

-(NSValue*)getWayPointByTag:(int)tagid{
    NSValue *val = [wayPointsList objectAtIndex:tagid];
    return val;
}

-(void) updateCamera
{
	if( hero_ ) {
        CGPoint pos = position_;
        //self.isRelativeAnchorPoint = YES;

        [self setPosition:ccp(pos.x+cameraOffset_.x,pos.y+cameraOffset_.y-70)];
        
        GameConfiguration *config = [GameConfiguration sharedConfiguration];
		ControlType control = [config controlType];
        if( control==kControlTypeDrag) {
            [self setRotation:269-hero_.rotation];
        }else if( control==kControlTypeHit){
            [self setRotation:179-hero_.rotation];
		}else if( control==kControlTypeDragArrow){
		}

        
        [self setAnchorPoint:ccp((hero_.position.x)/self.contentSize.width,(hero_.position.y)/self.contentSize.height)];
        //[(Cartopdown*)hero_ getVelocity]<130 &&
        float scaleFactor = 1/ fabs(([(Cartopdown*)hero_ getVelocity]/90));
        //NSLog(@"SCALE FACTOR: %f", scaleFactor);
        if(scaleFactor <= 0.6){
            self.scale = 0.6;
        }else if(scaleFactor>0.6 && scaleFactor<1.2){
            self.scale = scaleFactor;
        }else{
            self.scale = 1.2;
        }
        
        //NSLog(@"scale: %f", self.scale);
        [hud_ setScreenPosition:self.position];
	}
}

-(void) updateSprites
{
	for (b2Body* b = world_->GetBodyList(); b; b = b->GetNext())
	{
		BodyNode *node = (BodyNode*) b->GetUserData();
		
		//
		// Only update sprites that are meant to be updated by the physics engine
		//
		if( node && (node->properties_ & BN_PROPERTY_SPRITE_UPDATED_BY_PHYSICS) ) {
			//Synchronize the sprites' position and rotation with the corresponding body
			b2Vec2 pos = b->GetPosition();
			node.position = ccp( pos.x * kPhysicsPTMRatio, pos.y * kPhysicsPTMRatio);
			if( ! b->IsFixedRotation() )
				node.rotation = -1 * CC_RADIANS_TO_DEGREES(b->GetAngle());
		}	
	}	
}

-(void) gameOver
{
	gameState_ = kGameStateGameOver;
	[hud_ displayMessage:@"You Won!"];
	[hero_ onGameOver:YES];
}

-(void) displayVelocity:(float)velo
{
    [hud_ displayVelocity:velo];

}
-(void) displayRaceEnd{
    [hud_ displayRaceEnd];
}

-(void)debugAngle:(float)angle{
    [hud_ debugAngle:angle];
}

-(void)wrongWayAlert:(float)angle{
    [hud_ wrongWayAlert:angle];
}

-(void) updateLap{
    [hud_ onUpdateLap:time];
    [self resetTimer];
}

-(void) resetTimer{
    time=0;
    timeGhost = 0;
}

-(void)updateLoop {
    [hud_ updateLoop];
}

-(void) updateSteeringWheel:(float)angle
{
    
    [hud_ updateSteeringWheel:angle];
}



#pragma mark GameNode - Box2d Callbacks

// will be called for each created body in the parser
-(void) physicsCallbackWithBody:(b2Body*)body attribs:(NSString*)gameAttribs
{
    NSArray *values = [gameAttribs componentsSeparatedByString:@","];
    NSEnumerator *nse = [values objectEnumerator];
    
    if( ! values ) {
        CCLOG(@"LevelSVG: physicsCallbackWithBody: empty attribs");
        return;
    }
    
    BodyNode *node = nil;
    
    for( NSString *propertyValue in nse ) {
        NSArray *arr = [propertyValue componentsSeparatedByString:@"="];
        NSString *key = [arr objectAtIndex:0];
        NSString *value = [arr objectAtIndex:1];
        
        key = [key lowercaseString];
        
        if( [key isEqualToString:@"object"] ) {
            
            value = [value capitalizedString];
            Class klass = NSClassFromString( value );
            
            if( klass ) {
                // The BodyNode will be added to the scene graph at init time
                node = [[klass alloc] initWithBody:body game:self];
                
                [self addBodyNode:node z:0];
                [node release];
            } else {
                CCLOG(@"GameNode: WARNING: Don't know how to create class: %@", value);
            }
            
        } else if( [key isEqualToString:@"objectparams"] ) {
            // Format of parameters:
            // objectParams=direction:vertical;target:1;visible:NO;
            NSDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:10];
            NSArray *params = [value componentsSeparatedByString:@";"];
            for( NSString *param in params) {
                NSArray *keyVal = [param componentsSeparatedByString:@":"];
                [dict setValue:[keyVal objectAtIndex:1] forKey:[keyVal objectAtIndex:0]];
            }
            [node setParameters:dict];
            [dict release];
            
        } else
            CCLOG(@"Game Scene callback: unrecognized key: %@", key);
    }
}

//For single player
// will be called for each created body in the parser
-(void) physicsCallbackSingleWithBody:(b2Body*)body attribs:(NSString*)gameAttribs
{
    NSArray *values = [gameAttribs componentsSeparatedByString:@","];
    NSEnumerator *nse = [values objectEnumerator];
    
    if( ! values ) {
        CCLOG(@"LevelSVG: physicsCallbackWithBody: empty attribs");
        return;
    }
    
    BodyNode *node = nil;
    
    for( NSString *propertyValue in nse ) {
        NSArray *arr = [propertyValue componentsSeparatedByString:@"="];
        NSString *key = [arr objectAtIndex:0];
        NSString *value = [arr objectAtIndex:1];
        
        key = [key lowercaseString];
        
        if( [key isEqualToString:@"object"] ) {
            
            value = [value capitalizedString];
            NSLog(@"CLASS NAME : %@", value);
            Class klass = NSClassFromString( value );
            
            if( klass && ![value isEqual: @"Aicartopdown"]) {
                // The BodyNode will be added to the scene graph at init time
                node = [[klass alloc] initWithBody:body game:self];
                
                [self addBodyNode:node z:0];
                [node release];
            }else if ([value isEqual: @"Aicartopdown"]){
                CCLOG(@"GameNode: WARNING: Will remove Enemies class: %@", value);
                //destroy body
                world_->DestroyBody(body);
            } else {
                CCLOG(@"GameNode: WARNING: Don't know how to create class: %@", value);
            }
            
        } else if( [key isEqualToString:@"objectparams"] ) {
            // Format of parameters:
            // objectParams=direction:vertical;target:1;visible:NO;
            NSDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:10];
            NSArray *params = [value componentsSeparatedByString:@";"];
            for( NSString *param in params) {
                NSArray *keyVal = [param componentsSeparatedByString:@":"];
                [dict setValue:[keyVal objectAtIndex:1] forKey:[keyVal objectAtIndex:0]];
            }
            [node setParameters:dict];
            [dict release];
            
        } else
            CCLOG(@"Game Scene callback: unrecognized key: %@", key);
    }
}




-(void)addSpriteMark:(float) x y:(float) y{
    
   // NSLog(@"ADDED AT %f:%f",x,y);
}

// This is the default behavior
-(void) addBodyNode:(BodyNode*)node z:(int)zOrder
{
	CCLOG(@"LevelSVG: GameNode#addBodyNode override me");
}
// This is the default behavior
-(void)initSkidMarks
{
	CCLOG(@"LevelSVG: GameNode#initSkidMark override me");
}

// This is the default behavior
-(void) addSkidMarkAt:(CGPoint)posi angle:(float)angle
{
	CCLOG(@"LevelSVG: GameNode#addSkidMarkAt override me");
}
-(void) addSmokeRightSkidmark:(CGPoint)posi angle:(float)angle
{
	CCLOG(@"LevelSVG: GameNode#addSmokeSkidmark override me");
}
-(void) addSmokeLeftSkidmark:(CGPoint)posi angle:(float)angle
{
	CCLOG(@"LevelSVG: GameNode#addSmokeSkidmark override me");
}

-(void)initExplosionEffect
{
    CCLOG(@"LevelSVG: GameNode#initExplosionEffect override me");
}
-(void)removeExplosionEffect
{
    CCLOG(@"LevelSVG: GameNode#removeExplosionEffect override me");
}

- (void)initTurboEffect
{
    CCLOG(@"LevelSVG: GameNode#initTurboEffect override me");
}
- (void)turboEffectPosition:(CGPoint)posi rotation:(float)angle
{
    CCLOG(@"LevelSVG: GameNode#turboEffectPosition override me");
}
- (void)removeTurboEffect
{
    CCLOG(@"LevelSVG: GameNode#removeTurboEffect override me");
}

- (void)initCrashedEffect
{
    CCLOG(@"LevelSVG: GameNode#initCrashedEffect override me");
}
- (void)crashedEffectPosition:(CGPoint)posi rotation:(float)angle
{
    CCLOG(@"LevelSVG: GameNode#crashedEffectPosition override me");   
}
- (void)removeCrashedEffect
{
    CCLOG(@"LevelSVG: GameNode#removeCrashedEffect override me");   
}

#pragma mark GameNode - Touch Events Handler
- (BOOL) ccTouchBegan:(UITouch*)touch withEvent:(UIEvent*)event
{
	CGPoint touchLocation=[touch locationInView:[touch view]];
	touchLocation=[[CCDirector sharedDirector] convertToGL:touchLocation];
	CGPoint nodePosition = [self convertToNodeSpace: touchLocation];
	//	NSLog(@"pos: %f,%f -> %f,%f", touchLocation.x, touchLocation.y, nodePosition.x, nodePosition.y);
	return [self mouseDown: b2Vec2(nodePosition.x / kPhysicsPTMRatio ,nodePosition.y / kPhysicsPTMRatio)];	
}
- (void) ccTouchMoved:(UITouch*)touch withEvent:(UIEvent*)event
{
	CGPoint touchLocation=[touch locationInView:[touch view]];
	touchLocation=[[CCDirector sharedDirector] convertToGL:touchLocation];
	CGPoint nodePosition = [self convertToNodeSpace: touchLocation];
	
	[self mouseMove: b2Vec2(nodePosition.x/kPhysicsPTMRatio,nodePosition.y/kPhysicsPTMRatio)];
}
- (void) ccTouchEnded:(UITouch*)touch withEvent:(UIEvent*)event
{
	CGPoint touchLocation=[touch locationInView:[touch view]];
	touchLocation=[[CCDirector sharedDirector] convertToGL:touchLocation];
	CGPoint nodePosition = [self convertToNodeSpace: touchLocation];
	
	[self mouseUp: b2Vec2(nodePosition.x/kPhysicsPTMRatio,nodePosition.y/kPhysicsPTMRatio)];
}
#pragma mark GameNode - Touches (Mouse simulation)
//
// mouse code based on Box2d TestBed example: http://www.box2d.org
//
// 'button' is being pressed.
// Attach a mouseJoint if we are touching a box2d body
-(BOOL) mouseDown:(b2Vec2) p
{
	bool ret = false;
	if (mouseJoint_ != NULL)
		return false;
	// Make a small box.
	b2AABB aabb;
	b2Vec2 d;
	d.Set(0.001f, 0.001f);
	aabb.lowerBound = p - d;
	aabb.upperBound = p + d;
	// Query the world for overlapping shapes.
	MyQueryCallback callback(p);
	world_->QueryAABB(&callback, aabb);

	// only return yes if the fixture is touchable.
	if (callback.m_fixture )
	{
		b2Body *body = callback.m_fixture->GetBody();
		BodyNode *node = (BodyNode*) body->GetUserData();
		if( node && node.isTouchable ) {
			//
			// Attach touched body to static body with a mouse joint
			//
			body = callback.m_fixture->GetBody();
			b2MouseJointDef md;
			md.bodyA = mouseStaticBody_;
			md.bodyB = body;
			md.target = p;
			md.maxForce = 1000.0f * body->GetMass();
			mouseJoint_ = (b2MouseJoint*) world_->CreateJoint(&md);
			body->SetAwake(true);
			ret = true;
		}
	}
	return ret;
}

// 'button' is not being pressed any more. Destroy the mouseJoint
-(void) mouseUp:(b2Vec2)p
{
	if (mouseJoint_)
	{
		world_->DestroyJoint(mouseJoint_);
		mouseJoint_ = NULL;
	}	
}

// The mouse is moving: drag the mouseJoint
-(void) mouseMove:(b2Vec2)p
{	
	if (mouseJoint_)
		mouseJoint_->SetTarget(p);
}
@end
