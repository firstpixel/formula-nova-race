//
//  GlobalSingleton.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/23/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface GlobalSingleton : NSObject {
	NSUInteger board[2];  // c-style array
    int bannerType;
    int gameType;
	int gameKitState;
	BOOL audioOn;
    
    int carTypeP1;
    int carTypeP2;
    
    int player1Position;
    
    int totalWayPoints,totalLaps,gameLevel,selectedTrackInt;
    
    NSString* deviceType;
    NSString* selectedTrack;

    NSMutableString* player1Name;
    NSMutableString* player2Name;

    NSString* carColorP1;
    NSString* carColorP2;
}
@property (nonatomic, readwrite) int bannerType;
@property (nonatomic, retain) NSString *deviceType;
@property (nonatomic, retain) NSString *selectedTrack;
@property (nonatomic, retain) NSString *carColorP1;
@property (nonatomic, retain) NSString *carColorP2;
@property (nonatomic, readwrite) int carTypeP1; // 0 = Uno   1 = multiPlayer same Iphone   2 = multiPlayer bluetooth
@property (nonatomic, readwrite) int carTypeP2,totalWayPoints,totalLaps,gameLevel,selectedTrackInt; 


@property (nonatomic, readwrite) int gameType; // 0 = SinglePlayer   1 = multiPlayer same Iphone   2 = multiPlayer bluetooth
@property (nonatomic, readwrite) int gameKitState; // 0 = Start Game   1 = Picker   2 = Multiplayer   3 = Multiplayer Coin Toss   4 = Multiplayer reconnect
@property (nonatomic, readwrite) BOOL audioOn;


+ (GlobalSingleton *) sharedInstance;

//lock and unlock
-(void)unlockItem:(NSString*)item;
-(void)lockItem:(NSString*)item;
-(BOOL)isItemUnlocked:(NSString*)item;
/*
- (NSUInteger) getFieldValueAtPos:(NSUInteger)x;
- (void) setFieldValueAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal;
*/
-(void) clearData;

//Record
-(BOOL)saveRecordForTrack:(NSString*)track time:(int)time;
-(int)getRecordForTrack:(NSString*)track;

-(NSString*)timeToString:(int)t;

//Ghost Race
-(BOOL)saveGhostRace:(NSArray*)ghostRace forTrack:(int)track;
-(NSArray*)getGhostRaceForTrack:(int)track;
//Ghost Lap
-(BOOL)saveGhostBestLap:(NSArray*)ghostLap forTrack:(int)track;
-(NSArray*)getGhostBestLapForTrack:(int)track;

//preferences
-(void)intPref:(int)integ forKey:(NSString*)key;
-(int)getIntPrefForKey:(NSString*)key;

-(NSString*)getLeaderboardIDFromTrack;
-(NSString*)getChallengeIDFromTrack;

@end