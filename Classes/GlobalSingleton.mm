//
//  GlobalSingleton.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/23/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//
#import "cocos2d.h"
#import "GlobalSingleton.h"
#import "GhostStatus.h"
#import "MKStoreKit.h"

@implementation GlobalSingleton

static GlobalSingleton *_sharedInstance;

@synthesize bannerType,audioOn,gameType,gameKitState,carTypeP1,carTypeP2,carColorP1,carColorP2,totalWayPoints,totalLaps,selectedTrack,gameLevel,selectedTrackInt,deviceType;

- (id) init {
	self = [super init];
	if (self != nil){
		audioOn = YES;
		
		// custom initialization
		memset(board, 0, sizeof(board));
	}
	return self;
}

+ (GlobalSingleton *) sharedInstance
{
	if (!_sharedInstance)
	{
		_sharedInstance = [[GlobalSingleton alloc] init];
        [_sharedInstance unlockItem:@"car0"];
        [_sharedInstance unlockItem:@"track0"];
        [_sharedInstance setSelectedTrackInt:0];
        [_sharedInstance setBannerType:0];
        [_sharedInstance setSelectedTrack:@"usa"];
	}
	return _sharedInstance;
}

-(NSString*)getLeaderboardIDFromTrack
{
    NSString* leaderboard;
    switch([self selectedTrackInt]){
        case 0:
            //usa
            if([[MKStoreKit sharedKit] isProductPurchased:@"com.firstpixel.formulanova.full1"] == YES){
                leaderboard = @"865696";
            }else{
                leaderboard = @"8656960";
            }
            break;
        case 1:
            //australia
            if([[MKStoreKit sharedKit] isProductPurchased:@"com.firstpixel.formulanova.full1"] == YES){
                leaderboard = @"865656";
            }else{
                leaderboard = @"8656560";
            }
            break;
        case 2:
            //australiaReverse
            leaderboard = @"865666";
            break;
        case 3:
            //brazil
            leaderboard = @"865676";
            break;
        case 4:
            //brazilReverse
            leaderboard = @"865686";
            break;
        case 5:
            //italy
            leaderboard = @"865706";
            break;
        case 6:
            //italyReverse
            leaderboard = @"865716";
            break;
    }
    return leaderboard;
    
}

-(NSString*)getChallengeIDFromTrack
{
    NSString* leaderboard;
    switch([self selectedTrackInt]){
        case 0:
            //usa
            leaderboard = @"26082";
            break;
        case 1:
            //australia
            leaderboard = @"26042";
            break;
        case 2:
            //australiaReverse
            leaderboard = @"26052";
            break;
        case 3:
            //brazil
            leaderboard = @"26062";
            break;
        case 4:
            //brazilReverse
            leaderboard = @"26072";
            break;
        case 5:
            //italy
            leaderboard = @"26092";
            break;
        case 6:
            //italyReverse
            leaderboard = @"26102";
            break;
    }
    return leaderboard;
    
}

/*
- (NSUInteger) getFieldValueAtPos:(NSUInteger)x
{
	return board[x];
}

- (void) setFieldValueAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal
{
	board[x] = newVal;
}
*/
-(BOOL)saveRecordForTrack:(NSString*)track time:(int)time
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if([prefs integerForKey:track] > time || [prefs integerForKey:track] == nil){
        if( time > 1000 ){
            [prefs setInteger:time forKey:track];
            CCLOG(@"***********************************************");
            CCLOG(@" RECORD SAVED FOR %@! YOUR TIME NOW IS: %i",[track uppercaseString], time);
            CCLOG(@"***********************************************");
            
            return YES;
        }else{
            return NO;
        }
    }else{
        return NO;
    }
}

-(int)getRecordForTrack:(NSString*)track {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [prefs integerForKey:track];
}

-(BOOL)saveGhostRace:(NSArray*)ghostRace forTrack:(int)track{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSString *ghostRaceKey = [NSString stringWithFormat:@"com.firstpixel.FormulaNova.Race%i",track];
    if( ghostRace != nil ){
        NSData* gData = [NSKeyedArchiver archivedDataWithRootObject:ghostRace];
        [prefs setObject:gData forKey:ghostRaceKey];
        [prefs synchronize];
        GhostStatus * gLapStatus = [ghostRace lastObject];
        CCLOG(@"***********************************************");
        CCLOG(@" GHOST LAP RACE SAVED FOR TRACK %i WITH CAR: %i",track, gLapStatus.car);
        CCLOG(@"***********************************************");
        return YES;
    }else{
        return NO;
    }
}

-(NSArray *)getGhostRaceForTrack:(int)track {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *ghostRaceKey = [NSString stringWithFormat:@"com.firstpixel.FormulaNova.Race%i",track];
    NSArray * gArray = (NSArray*)[NSKeyedUnarchiver unarchiveObjectWithData:[prefs objectForKey:ghostRaceKey]];
    return gArray;
}

-(BOOL)saveGhostBestLap:(NSArray*)ghostLap forTrack:(int)track {

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *ghostLapKey = [NSString stringWithFormat:@"com.firstpixel.FormulaNova.Best%i",track];
    GhostStatus * gLapStatus = [ghostLap lastObject];
    NSData *gArrayData = [prefs objectForKey:ghostLapKey];
    GhostStatus * gStatus = nil;
    if(gArrayData != nil){
        NSArray * gArray = (NSArray*)[NSKeyedUnarchiver unarchiveObjectWithData:gArrayData];
        gStatus = [gArray lastObject];
    }
    if( (gStatus!=nil && gStatus.time > gLapStatus.time) || gArrayData == nil){
        if( gLapStatus.time > 5000 ){
            
            NSData* gData = [NSKeyedArchiver archivedDataWithRootObject:ghostLap];
            [prefs setObject:gData forKey:ghostLapKey];
            [prefs synchronize];
            CCLOG(@"***********************************************");
            CCLOG(@" GHOST BEST LAP SAVED FOR TRACK %i WITH CAR: %i",track, gLapStatus.car);
            CCLOG(@"***********************************************");
            return YES;
        }else{
            return NO;
        }
    }else{
        return NO;
    }

}

-(NSArray*)getGhostBestLapForTrack:(int)track {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *ghostLapKey = [NSString stringWithFormat:@"com.firstpixel.FormulaNova.Best%i",track];
    NSArray * gArray = (NSArray*)[NSKeyedUnarchiver unarchiveObjectWithData:[prefs objectForKey:ghostLapKey]];
    return gArray;
}


-(void)unlockItem:(NSString*)item{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:YES forKey:item];
}

-(void)lockItem:(NSString*)item{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:NO forKey:item];
}

-(BOOL)isItemUnlocked:(NSString*)item{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if([prefs boolForKey:item]){
        return YES;
    }else{
        return NO;
    }
}


-(void)intPref:(int)integ forKey:(NSString*)key{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:integ forKey:key];
}

//if no pref return -1, else return the preference
-(int)getIntPrefForKey:(NSString*)key{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
   return [prefs integerForKey:key];
}

-(NSString*)timeToString:(int)t{
    int minutes = t / 60000;
    int seconds = t % 60000 / 1000;
    int milliseconds = t % 1000;
    
    NSString* minutesString;
    NSString* secondsString;
    NSString* millisecondsString;
    
    minutesString = [NSString stringWithFormat:@"%d",minutes];
    if(seconds<=9){
        secondsString = [NSString stringWithFormat:@"0%d",seconds];
    }else{
        secondsString = [NSString stringWithFormat:@"%d",seconds];
    }
    if(milliseconds<=9){
        millisecondsString = [NSString stringWithFormat:@"00%d",milliseconds];
    }else if(milliseconds<=99){
        millisecondsString = [NSString stringWithFormat:@"0%d",milliseconds];
    }else{
        millisecondsString = [NSString stringWithFormat:@"%d",milliseconds];
    }
    // NSLog(@"TIMER TIME - %@ : %@ : %@ ",minutesString, secondsString, millisecondsString);
    return [NSString stringWithFormat:@"%@ : %@ : %@ ",minutesString, secondsString, millisecondsString];
    
}


-(void) clearData {
	
	audioOn = YES;
    
    totalWayPoints = 0;
    carTypeP1 = 0;
    carColorP1 = [NSMutableString stringWithString:@"red"];
}



- (id)retain

{
	
    return self;
	
}



- (unsigned int)retainCount

{
    return UINT_MAX;  //denotes an object that cannot be released
}



- (oneway void)release

{
	
    //do nothing
	
}



- (id)autorelease

{
	
    return self;
	
}




@end