//
//  HUD.h
//  LevelSVG
//
//  Created by Ricardo Quesada on 16/10/09.
//  Copyright 2009 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//

#import "cocos2d.h"


@protocol JoystickProtocol;
@class JumpButton;
@class GameNode;

//LEADERBOARDS ACHIEVMENTS
#pragma once 
#define MY_NEW_CAR @"1163402" 
#define DRIVING_LIKE_MY_GRANNY @"1163412" 
#define AUSTRALIAN_TROPHY @"1166052" 
#define AUSTRALIAN_REVERSE_TROPHY @"1166072" 
#define BRAZILIAN_TROPHY @"1166082" 
#define BRAZILIAN_REVERSE_TROPHY @"1166092" 
#define AMERICAN_TROPHY @"1166102" 
#define ITALY_TROPHY @"1166112" 
#define ITALIAN_REVERSE_TROPHY @"1166122" 
#define AUSTRALIA_FAST_AND_PERFECT @"1166132" 
#define AUSTRALIA_REVERSE_FAST_AND_PERFECT @"1166142" 
#define BRAZIL_FAST_AND_PERFECT @"1166152" 
#define BRAZIL_REVERSE_FAST_AND_PERFECT @"1166162" 
#define USA_FAST_AND_PERFECT @"1166172" 
#define ITALY_FAST_AND_PERFECT @"1166182" 
#define ITALY_REVERSE_FAST_AND_PERFECT @"1166192" 


@interface HUD : CCLayer {
	
	// game
	GameNode	*game_;
    CCMenu *topMenu;
    CCLayerColor *topMenuColorBar;
	// joystick and joysprite. weak ref
	CCNode<JoystickProtocol>	*joystick_;
	
	CCLabelBMFont	*laps_;
	CCLabelBMFont	*lives_;
    
    CCLabelBMFont	*velocity_;
    CCLabelBMFont   *heroposition_;
    
    CCSprite *_lapDisplay;
    CCAction *_lapDisplayAction;
    
    CCLabelBMFont *lastLap_;
    CCLabelBMFont *bestLap_;
    
    CCSprite *arrowDebug;
    CCSprite *arrowDebug2;
    
    CCAnimation *lapAnima;
    CCSpriteBatchNode *spriteSheet;
    
    CCSprite* myMarker;
    CCSprite* ai1Marker;
    CCSprite* ai2Marker;
    CCSprite* ai3Marker;
    CCSprite* ai4Marker;
    CCSprite* ai5Marker;
    CCSprite* warmupDisplay;
    CCSprite* messageSprite;
    CGPoint _screenPosition;
    BOOL newRecord;
    int lapCounted;
    int ghostBestTime;
    
    int raceBestLap;

}
@property (nonatomic, retain) CCSprite *lapDisplay;
@property (nonatomic, retain) CCAction *lapDisplayAction;

@property (nonatomic, readwrite) CGPoint screenPosition;

@property (nonatomic, retain) CCNode<JoystickProtocol> *joystick;

// creates and initializes a HUD
+(id) HUDWithGameNode:(GameNode*)game;

// initializes a HUD with a delegate
-(id) initWithGameNode:(GameNode*)game;

-(void) onUpdateLap:(int)t;

-(void) displayVelocity:(float)velo;
-(void) displayPosition;
-(void) updateSteeringWheel:(float)angle;

// display a message on the screen
-(void) displayMessage:(NSString*)message;

-(void)displayWarmUp;
-(void)displayRaceEnd;

-(void) debugAngle:(float)angle;
-(void) wrongWayAlert:(float)angle;
-(void) updateLoop;
-(void)removeWarmUp;

-(void)displayCCSpriteMessage:(NSString*)spriteName;
-(void)displayNewCarIcon;
-(void)displayNewTrackIcon;

//add for openfeint
- (bool)canReceiveCallbacksNow;
-(void)_oFSendChallenge;


-(void) buttonPause:(id)sender;
@end
