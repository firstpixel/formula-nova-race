//
//  HUD.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 16/10/09.
//  Copyright 2009 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//

//
// HUD: Head Up Display
//
// - Display score
// - Display lives
// - Display joystick, but it is not responsible for reading it
// - Display the menu button
// - Register a touch events: drags the screen
//
#import "HUD.h"
#import "GameConfiguration.h"
#import "JoystickCar.h"
#import "Joystick.h"
//#import "JoystickAccelerometer.h"
#import "JoystickDragArrow.h"
#import "GameNode.h"
#import "MenuScene.h"
#import "SelectCarScene.h"
#import "Hero.h"
#import "CarTopDown.h"
#import "AI.h"
#import "AICarTopDown.h"
#import "SoundMenuItem.h"
#import "GlobalSingleton.h"
#import "Cartopdown.h"
#import "SimpleAudioEngine.h"
#import "MKStoreKit.h"
#import "GameBuyNowScene.h"
#import "GameCenterManager.h"
#import "GhostStatus.h"

 
#import "Flurry.h"





@implementation HUD



@synthesize lapDisplay = _lapDisplay;
@synthesize lapDisplayAction = _lapDisplayAction;
@synthesize screenPosition = _screenPosition;
@synthesize joystick = joystick_;

+(id) HUDWithGameNode:(GameNode*)game
{
	return [[[self alloc] initWithGameNode:game] autorelease];
}
-(id) initWithGameNode:(GameNode*)aGame
{
	if( (self=[super init])) {
        raceBestLap = 500000;
        
        if([[GlobalSingleton sharedInstance] gameType] != 1 ){
            NSMutableArray* ghostInitial = [[[GlobalSingleton sharedInstance] getGhostBestLapForTrack:[[GlobalSingleton sharedInstance] selectedTrackInt]] mutableCopy];
            GhostStatus* initialStatus = [ghostInitial objectAtIndex:ghostInitial.count-1];
            ghostBestTime = initialStatus.time;
            NSLog(@"%i < %i",raceBestLap , ghostBestTime);
        }
        
		self.isTouchEnabled = YES;
        game_ = aGame;
        CGSize s = [[CCDirector sharedDirector] winSize];
		
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            NSLog(@"hud hd");
            //iPad retina screen
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud-hd.plist"];
            // weak ref
            spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"hud-hd.png" capacity:145];
            [self addChild:spriteSheet z:10];
        }else{
            //iPad screen
            
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud.plist"];
            // weak ref
            spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"hud.png" capacity:145];
            [self addChild:spriteSheet z:10];
        }

		// level control configuration:
		//  - 2-way, 4-way or car ?
		//  - d-pad or accelerometer ?
		//  - 0, 1 or 2 buttons ?
	
		GameConfiguration *config = [GameConfiguration sharedConfiguration];
		ControlType control = [config controlType];
		
        if( control==kControlTypeDrag) {
             joystick_ = [Joystick joystick];
            CCLOG(@"JOYSTICK DRAG");
        }else if( control==kControlTypeHit){
            joystick_ = [JoystickCar joystick];
		}else if( control==kControlTypeDragArrow){
            joystick_ = [JoystickDragArrow joystick];
		}
        
        /*else if( control==kControlTypeAccelerometer){
            joystick_ = [JoystickAccelerometer joystick];
		}*/
        [self addChild:joystick_];
            
				
		// The Hero is responsible for reading the joystick
		[[game_ hero] setJoystick:joystick_];		
		
		// enable button left/right only if using "Pad" controls
		
		[joystick_ setPadEnabled: NO];
		// pad + 4 direction is not implemented yet
		if( control==kControlTypeDrag || control==kControlTypeAccelerometer || control==kControlTypeDragArrow ) {
			[joystick_ setPadEnabled: YES];
			[joystick_ setPadPosition:ccp(124,34)];
		}
		
		topMenuColorBar = [CCLayerColor layerWithColor:ccc4(32,32,32,128) width:s.width height:40];
		[topMenuColorBar setPosition:ccp(0,s.height-40)];
		[self addChild:topMenuColorBar z:0];

		// Menu Button
		CCMenuItem *itemBack = [SoundMenuItem itemFromNormalSpriteFrameName:@"back_up.png" selectedSpriteFrameName:@"back_down.png" target:self selector:@selector(buttonRestart:)];
		CCMenuItem *itemPause = [SoundMenuItem itemFromNormalSpriteFrameName:@"pause_up.png" selectedSpriteFrameName:@"pause_down.png" target:self selector:@selector(buttonPause:)];
		
        topMenu = [CCMenu menuWithItems:itemBack,itemPause,nil];
        [topMenu alignItemsHorizontallyWithPadding:15.0f];
		[self addChild:topMenu z:1];
		[topMenu setPosition:ccp(50,s.height-20)];
		
		GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
        // Score Points
		laps_ = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"LAP %i/%i",0,[mySingleton totalLaps]] fntFile:@"lap-white.fnt"];
        //[laps_.texture setAliasTexParameters];
		[self addChild:laps_ z:1];
		[laps_ setPosition:ccp(s.width/2+15, s.height-20.5f)];

        //DISPLAY LAP
        NSMutableArray *lapAnimFrames = [NSMutableArray array];
        for(int i = 1; i <= 5; ++i) {
                [lapAnimFrames addObject:
                [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                [NSString stringWithFormat:@"laptime%d.png", i]]];
        }
        lapAnima = [CCAnimation animationWithSpriteFrames:lapAnimFrames delay:0.1f];
        self.lapDisplay = [CCSprite spriteWithSpriteFrameName:@"laptime1.png"];        
        _lapDisplay.position = ccp(s.width/2, s.height-100);
        self.lapDisplayAction = [CCAnimate actionWithAnimation:lapAnima restoreOriginalFrame:NO];
        [spriteSheet addChild:_lapDisplay z:1];
        
        //bestLap
        bestLap_ = [CCLabelBMFont labelWithString:@"0:00:000" fntFile:@"Microgramma.fnt"];
        [self addChild:bestLap_ z:20];
		[bestLap_ setPosition:ccp(s.width/2+0.5f+68, s.height-80.5f)];
        [bestLap_ setRotation:-6.0f];
        //lastLap
        lastLap_ = [CCLabelBMFont labelWithString:@"0:00:000" fntFile:@"Microgramma.fnt"];
        //[self addChild:lastLap_ z:3];
        [self addChild:lastLap_ z:20];
        [lastLap_ setPosition:ccp(s.width/2+0.5f+71, s.height-102.5f)];
        
        [_lapDisplay setVisible:NO];
        [bestLap_ setVisible:NO];
        [lastLap_ setVisible:NO];
        [lastLap_ setRotation:-6.0f];
        
		velocity_ = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%d Mph", 0] fntFile:@"lap-white.fnt"];
		[velocity_.texture setAliasTexParameters];
		[self addChild:velocity_ z:0];
		[velocity_ setPosition:ccp(s.width/2+40, 20.5f)];		
        
        heroposition_ = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"POS %d", 6] fntFile:@"lap-white.fnt"];
		[heroposition_.texture setAliasTexParameters];
		[self addChild:heroposition_ z:1];
		[heroposition_ setAnchorPoint:ccp(1,0.5f)];
		[heroposition_ setPosition:ccp(s.width-5.5f, s.height-20.5f)];	
        
        NSString* selectedTrack = [[mySingleton selectedTrack] stringByReplacingOccurrencesOfString:@"Reverse" withString:@""];
        NSString* track= [NSString stringWithFormat:@"%@-display.png",selectedTrack];
        
        CCSprite* displayTrack = [CCSprite spriteWithFile:track];        
        
        displayTrack.position = ccp(65, 205);
        [self addChild:displayTrack z:-10];

        myMarker = [CCSprite spriteWithSpriteFrameName:@"car_pin.png"];        
        myMarker.position = ccp(65, 200);
        [spriteSheet addChild:myMarker];
        
        ai1Marker = [CCSprite spriteWithSpriteFrameName:@"ai_pin.png"];        
        ai1Marker.position = ccp(65, 200);
        [spriteSheet addChild:ai1Marker];
        
        ai2Marker = [CCSprite spriteWithSpriteFrameName:@"ai_pin.png"];        
        ai2Marker.position = ccp(65, 200);
        [spriteSheet addChild:ai2Marker];
        
        ai3Marker = [CCSprite spriteWithSpriteFrameName:@"ai_pin.png"];        
        ai3Marker.position = ccp(65, 200);
        [spriteSheet addChild:ai3Marker];
        
        ai4Marker = [CCSprite spriteWithSpriteFrameName:@"ai_pin.png"];        
        ai4Marker.position = ccp(65, 200);
        [spriteSheet addChild:ai4Marker];
        
        ai5Marker = [CCSprite spriteWithSpriteFrameName:@"ai_pin.png"];        
        ai5Marker.position = ccp(65, 200);
        [spriteSheet addChild:ai5Marker];
        
        if([config controlType]==kControlTypeAccelerometer || [config controlType]==kControlTypeDragArrow){
            //ARROW
            arrowDebug = [CCSprite spriteWithSpriteFrameName:@"arrow.png"];
            [spriteSheet addChild:arrowDebug z:2];
            
            if([config controlType]==kControlTypeAccelerometer)[arrowDebug setPosition:ccp(s.width/2, s.height/2)];
            if([config controlType]==kControlTypeDragArrow){
                Cartopdown* heroCar = (Cartopdown*)[game_ hero];
                CGPoint point = [self convertToWorldSpace:heroCar.position];
                //NSLog(@"x: %f y: %f",point.x,point.y);
                [arrowDebug setPosition:point];
                
                
            }
		}
        
              
	}
	
	return self;
}
/*
 *
 *
 *  ACCELEROMETER
 *
 *
 *
 */




-(void)removeCCSpriteMessage{
    [spriteSheet removeChild:messageSprite cleanup:YES];
    messageSprite=nil;
}

-(void)displayCCSpriteMessage:(NSString*)spriteName{
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    //OLD IPOD DONT DISPLAY ALERTS
    if(![[mySingleton deviceType] isEqualToString:@"oldIphone"] ){
        
           if(messageSprite!=nil){
               [messageSprite stopAllActions];
               [spriteSheet removeChild:messageSprite cleanup:YES];
               messageSprite=nil;
           }
           CGSize s = [[CCDirector sharedDirector] winSize];
           messageSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithString:spriteName]];
           [spriteSheet addChild:messageSprite];
           messageSprite.position = ccp(s.width/2, s.height/2);
           messageSprite.scale = 5.2f;
           // id moveTo = [CCMoveTo actionWithDuration:0.8f position:ccp(s.width/2, s.height/2)];
           id scaleTo = [CCScaleTo actionWithDuration:0.2f scale:0.8f];
           id scaleBack = [CCScaleTo actionWithDuration:0.3f scale:1];
           id removeMessage = [CCCallFunc actionWithTarget:self selector:@selector(removeCCSpriteMessage)];
           id seq = [CCSequence actions: scaleTo, scaleBack, removeMessage,nil];
           [messageSprite runAction:seq];
    }
}


-(void)displayWarmUp{
    [self displayCCSpriteMessage:@"ms-getredy.png"]; 
    [[SimpleAudioEngine sharedEngine] playEffect: @"w.mp3"];
    CGSize s = [[CCDirector sharedDirector] winSize];
    
    NSMutableArray *warmUpFrames = [NSMutableArray array];
    for(int i = 0; i < 5; ++i) {
        
            [warmUpFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"racestart%d.png", i]]];
    }
    CCAnimation* warmupAnima = [CCAnimation animationWithSpriteFrames:warmUpFrames delay:1.0f];
    warmupDisplay = [CCSprite spriteWithSpriteFrameName:@"racestart0.png"];        
    warmupDisplay.position = ccp(s.width/2, s.height+60);
    CCAnimate* warmupDisplayAction = [CCAnimate actionWithAnimation:warmupAnima restoreOriginalFrame:NO];
    [spriteSheet addChild:warmupDisplay];
    
    id moveTo = [CCMoveTo actionWithDuration:0.1f position:ccp(s.width/2, s.height-50)];
    id moveBack = [CCMoveTo actionWithDuration:0.1f position:ccp(s.width/2, s.height+60)];
	id removeMe = [CCCallFunc actionWithTarget:self selector:@selector(removeWarmUp)];
    
    
    id seq = [CCSequence actions:moveTo, warmupDisplayAction, moveBack, removeMe,nil];
	
    [warmupDisplay runAction:seq];
}
-(void)removeWarmUp{
   [self displayCCSpriteMessage:@"ms-go.png"]; 
   [spriteSheet removeChild:warmupDisplay cleanup:YES];
}

//FIX FOR OPENFEINT
- (bool)canReceiveCallbacksNow{
	return YES;
}

-(void)_oFSendChallenge{
    //NSString* time1 = [NSString stringWithFormat:@"%i",raceBestLap];
    //NSData* data = [NSData dataWithBytes:time1 length:[time1 length]];
    
    // aqui deve ter um BT para enviar o challenge
   /* [OFChallengeService displaySendChallengeModal: @"your challenge id"
                                    challengeText: [NSString stringWithFormat: @"My time is %@, can you beat it?", raceBestLap]
                                    challengeData: data];
*/

}
/*
-(void)oFdisplayChallengeBt{

    if ([OpenFeint hasUserApprovedFeint]){
        if([OpenFeint isOnline]){
            [self _oFSendChallenge];        
        }
    }else{
      //  OFDelegate nilDelegate;
     //   OFDelegate sendChallengeDelegate (self, @selector(_oFSendChallenge));
        
      //  [OpenFeint presentUserFeintApprovalModal: sendChallengeDelegate deniedDelegate: nilDelegate];
        
    }
}
 */

-(void)oFupdateAchievements{
    Cartopdown* heroCar = (Cartopdown*)[game_ hero];
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    
    //My new car - do a clean race witout use fix and boost
    if([heroCar totalFixUsed]==0  && [heroCar totalBoostUsed]==0 ){
        //[OFAchievementService updateAchievement:MY_NEW_CAR andPercentComplete:100.0 andShowNotification:YES];
    }
    //Driving like my granny!
    if([heroCar totalCrashs]==0){
       // [OFAchievementService updateAchievement:DRIVING_LIKE_MY_GRANNY andPercentComplete:100.0 andShowNotification:YES];
    }
    
    //USA trophy
    if([heroCar racePosition]==1 && [mySingleton selectedTrackInt]==0){
       // [OFAchievementService updateAchievement:AMERICAN_TROPHY andPercentComplete:100.0 andShowNotification:YES];
    }
    //USA  Fast and Perfect
    if([heroCar racePosition]==1 && [heroCar totalCrashs]==0 && [mySingleton selectedTrackInt]==0){
       // [OFAchievementService updateAchievement:USA_FAST_AND_PERFECT andPercentComplete:100.0 andShowNotification:YES];
    }
    
    //Australian trophy
    if([heroCar racePosition]==1 && [mySingleton selectedTrackInt]==1){
       // [OFAchievementService updateAchievement:AUSTRALIAN_TROPHY andPercentComplete:100.0 andShowNotification:YES];
    }
    //Australia Fast and Perfect
    if([heroCar racePosition]==1 && [heroCar totalCrashs]==0 && [mySingleton selectedTrackInt]==1){
       // [OFAchievementService updateAchievement:AUSTRALIA_FAST_AND_PERFECT andPercentComplete:100.0 andShowNotification:YES];
    }
    //Australian Reverse trophy
    if([heroCar racePosition]==1 && [mySingleton selectedTrackInt]==2){
       // [OFAchievementService updateAchievement:AUSTRALIAN_REVERSE_TROPHY andPercentComplete:100.0 andShowNotification:YES];
    }
    //Australia Reverse Fast and Perfect
    if([heroCar racePosition]==1 && [heroCar totalCrashs]==0 && [mySingleton selectedTrackInt]==2){
       // [OFAchievementService updateAchievement:AUSTRALIA_REVERSE_FAST_AND_PERFECT andPercentComplete:100.0 andShowNotification:YES];
    }
    
    //Brazil trophy
    if([heroCar racePosition]==1 && [mySingleton selectedTrackInt]==3){
      //  [OFAchievementService updateAchievement:BRAZILIAN_TROPHY andPercentComplete:100.0 andShowNotification:YES];
    }
    //Brazil  Fast and Perfect
    if([heroCar racePosition]==1 && [heroCar totalCrashs]==0 && [mySingleton selectedTrackInt]==3){
      //  [OFAchievementService updateAchievement:BRAZIL_FAST_AND_PERFECT andPercentComplete:100.0 andShowNotification:YES];
    }
    //Brazil Reverse trophy
    if([heroCar racePosition]==1 && [mySingleton selectedTrackInt]==4){
       // [OFAchievementService updateAchievement:BRAZILIAN_REVERSE_TROPHY andPercentComplete:100.0 andShowNotification:YES];
    }
    //Brazil REVERSE Fast and Perfect
    if([heroCar racePosition]==1 && [heroCar totalCrashs]==0 && [mySingleton selectedTrackInt]==4){
       // [OFAchievementService updateAchievement:BRAZIL_REVERSE_FAST_AND_PERFECT andPercentComplete:100.0 andShowNotification:YES];
    }
    
   
   
    //Italy trophy
    if([heroCar racePosition]==1 && [mySingleton selectedTrackInt]==5){
       // [OFAchievementService updateAchievement:ITALY_TROPHY andPercentComplete:100.0 andShowNotification:YES];
    }
    //Italy  Fast and Perfect
    if([heroCar racePosition]==1 && [heroCar totalCrashs]==0 && [mySingleton selectedTrackInt]==5){
       // [OFAchievementService updateAchievement:ITALY_FAST_AND_PERFECT andPercentComplete:100.0 andShowNotification:YES];
    }
    //Italy Reverse trophy
    if([heroCar racePosition]==1 && [mySingleton selectedTrackInt]==6){
       // [OFAchievementService updateAchievement:ITALIAN_REVERSE_TROPHY andPercentComplete:100.0 andShowNotification:YES];
    }
    //Italy REVERSE Fast and Perfect
    if([heroCar racePosition]==1 && [heroCar totalCrashs]==0 && [mySingleton selectedTrackInt]==6){
       // [OFAchievementService updateAchievement:ITALY_REVERSE_FAST_AND_PERFECT andPercentComplete:100.0 andShowNotification:YES];
    }
}
-(void) displayRaceEnd{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
  //  [self oFdisplayChallengeBt];
    //[self oFupdateAchievements];
    
    CCSprite * backgroundPop = [CCSprite spriteWithSpriteFrameName:@"race-end.png"];
    CGSize s = [[CCDirector sharedDirector] winSize];
    backgroundPop.position = ccp(s.width/2, s.height/2);
    [self addChild:backgroundPop];
    Cartopdown* heroCar = (Cartopdown*)[game_ hero];
    if([[GlobalSingleton sharedInstance] gameType] == 1 ){
        if([heroCar racePosition]<=3){
            if([heroCar racePosition]==3){
                [self displayMessage:@"Good race! \nYou finished at third place."];
            }
            if([heroCar racePosition]==2){
                [self displayMessage:@"Awesome race! \nYou finished at second place."];
            }
            if([heroCar racePosition]==1){
                [self displayMessage:@"Congratulations! \nYou finished at first place."];
            }
            //UNLOCK TRACKS
            if([mySingleton selectedTrackInt]==0 && ![mySingleton isItemUnlocked:@"track1"]){
                [mySingleton unlockItem:@"track1"]; 
                id delayTrack = [CCDelayTime actionWithDuration:2.0f];
                id funcEndTrack = [CCCallFunc actionWithTarget:self selector:@selector(displayNewTrackIcon)];
                id seqTrack = [CCSequence actions:delayTrack, funcEndTrack,  nil];
                [self runAction:seqTrack];
            }
        }else{
            //finished at fourth , dont have trophy for this track
            if(([heroCar racePosition]==4 || [heroCar racePosition]==5 || [heroCar racePosition]==6) && 
               (([mySingleton selectedTrackInt]==0 && ![mySingleton isItemUnlocked:@"track1"]) ||
               ([mySingleton selectedTrackInt]==1 && ![mySingleton isItemUnlocked:@"track2"]) ||
               ([mySingleton selectedTrackInt]==2 && ![mySingleton isItemUnlocked:@"track3"]) ||
               ([mySingleton selectedTrackInt]==3 && ![mySingleton isItemUnlocked:@"track4"]) ||
               ([mySingleton selectedTrackInt]==4 && ![mySingleton isItemUnlocked:@"track5"]) ||
               ([mySingleton selectedTrackInt]==5 && ![mySingleton isItemUnlocked:@"track6"]))
               ){
                if([heroCar racePosition]==4){
                    [self displayMessage:@"Not too bad, you are fourth,\n can't unlock next track."];
                }
                if([heroCar racePosition]==5){
                    [self displayMessage:@"My Granny does better,\n you are fifth, can't unlock next track."];

                }
                if([heroCar racePosition]==6){
                    [self displayMessage:@"You are the last one,\n can't unlock next track."];
                }
            }else{
                if([heroCar racePosition]==4){
                    [self displayMessage:@"Not too bad, you are fourth,\n let's try again."];
                }
                if([heroCar racePosition]==5){
                    [self displayMessage:@"My Granny does better, you are fifth,\n let's try again."];
                    
                }
                if([heroCar racePosition]==6){
                    [self displayMessage:@"You are the last one,\n let's try again."];
                }  
            }
        }
    }else{
          NSLog(@"%i < %i",raceBestLap , ghostBestTime);
        if(raceBestLap!=0 && raceBestLap < ghostBestTime){
            [self displayMessage:@"Nice job,\n you have new best lap! :)"];
        }else{
            [self displayMessage:@"Too bad,\n you could not make it. :("];
        }
    }
    //UNLOCK CARS
    if([heroCar racePosition]<=3){
        CCSprite * trophy = [CCSprite spriteWithSpriteFrameName:@"trophy.png"];        
        trophy.position = ccp(s.width/2+ s.width/4, (s.height/3)*2);//ccp(364, 172);
        [self addChild:trophy];
        [[SimpleAudioEngine sharedEngine] playEffect: @"pickup_coin.wav"];
        //AUSTRALIA
        if([heroCar racePosition]==3){
            [mySingleton unlockItem:[NSString stringWithFormat:@"track%itrophy",[mySingleton selectedTrackInt]]];
        }
        
        if([heroCar racePosition]==2){
            [mySingleton unlockItem:[NSString stringWithFormat:@"track%itrophy",[mySingleton selectedTrackInt]]];
        }
        
        if([heroCar racePosition]==1){
            [mySingleton unlockItem:[NSString stringWithFormat:@"track%itrophy",[mySingleton selectedTrackInt]]];
        }
        
        if([mySingleton selectedTrackInt]==0 && ![mySingleton isItemUnlocked:@"car1"]){
            [mySingleton unlockItem:@"car1"];
            id delay = [CCDelayTime actionWithDuration:1.0f];
            id funcEnd = [CCCallFunc actionWithTarget:self selector:@selector(displayNewCarIcon)];
            id seq = [CCSequence actions:delay, funcEnd,  nil];
            [self runAction:seq];
        }
        
        if([[MKStoreKit sharedKit] isProductPurchased:@"com.firstpixel.formulanova.full1"] == YES){
            //UNLOCK TRACKS PACKAGE 1
            if([mySingleton selectedTrackInt]==1 && ![mySingleton isItemUnlocked:@"track2"]){
                [mySingleton unlockItem:@"track2"];
                id delayTrack = [CCDelayTime actionWithDuration:2.0f];
                id funcEndTrack = [CCCallFunc actionWithTarget:self selector:@selector(displayNewTrackIcon)];
                id seqTrack = [CCSequence actions:delayTrack, funcEndTrack,  nil];
                
                [self runAction:seqTrack];
            }
            if([mySingleton selectedTrackInt]==2 && ![mySingleton isItemUnlocked:@"track3"]){
                [mySingleton unlockItem:@"track3"];
                id delayTrack = [CCDelayTime actionWithDuration:2.0f];
                id funcEndTrack = [CCCallFunc actionWithTarget:self selector:@selector(displayNewTrackIcon)];
                id seqTrack = [CCSequence actions:delayTrack, funcEndTrack,  nil];
                
                [self runAction:seqTrack];
            }
            if([mySingleton selectedTrackInt]==3 && ![mySingleton isItemUnlocked:@"track4"]){
                [mySingleton unlockItem:@"track4"];
                id delayTrack = [CCDelayTime actionWithDuration:2.0f];
                id funcEndTrack = [CCCallFunc actionWithTarget:self selector:@selector(displayNewTrackIcon)];
                id seqTrack = [CCSequence actions:delayTrack, funcEndTrack,  nil];
                
                [self runAction:seqTrack];
            }
            if([mySingleton selectedTrackInt]==4 && ![mySingleton isItemUnlocked:@"track5"]){
                [mySingleton unlockItem:@"track5"];
                id delayTrack = [CCDelayTime actionWithDuration:2.0f];
                id funcEndTrack = [CCCallFunc actionWithTarget:self selector:@selector(displayNewTrackIcon)];
                id seqTrack = [CCSequence actions:delayTrack, funcEndTrack,  nil];
                
                [self runAction:seqTrack];
            }
            if([mySingleton selectedTrackInt]==5 && ![mySingleton isItemUnlocked:@"track6"]){
                [mySingleton unlockItem:@"track6"];
                id delayTrack = [CCDelayTime actionWithDuration:2.0f];
                id funcEndTrack = [CCCallFunc actionWithTarget:self selector:@selector(displayNewTrackIcon)];
                id seqTrack = [CCSequence actions:delayTrack, funcEndTrack,  nil];
                
                [self runAction:seqTrack];
            }
            
            //UNLOCK CARS PACKAGE AUSTRALIA REVERSE 1
            if([mySingleton selectedTrackInt]==1 && ![mySingleton isItemUnlocked:@"car2"]){
                [mySingleton unlockItem:@"car2"];
                id delay = [CCDelayTime actionWithDuration:1.0f];
                id funcEnd = [CCCallFunc actionWithTarget:self selector:@selector(displayNewCarIcon)];
                id seq = [CCSequence actions:delay, funcEnd,  nil];
                [self runAction:seq];
            }
            //BRAZIL
            if([mySingleton selectedTrackInt]==2 && ![mySingleton isItemUnlocked:@"car3"]){
                [mySingleton unlockItem:@"car3"];
                id delay = [CCDelayTime actionWithDuration:1.0f];
                id funcEnd = [CCCallFunc actionWithTarget:self selector:@selector(displayNewCarIcon)];
                id seq = [CCSequence actions:delay, funcEnd,  nil];
                [self runAction:seq];
            }
            //BRAZIL reverse
            if([mySingleton selectedTrackInt]==3 && ![mySingleton isItemUnlocked:@"car4"]){
                [mySingleton unlockItem:@"car4"];
                id delay = [CCDelayTime actionWithDuration:1.0f];
                id funcEnd = [CCCallFunc actionWithTarget:self selector:@selector(displayNewCarIcon)];
                id seq = [CCSequence actions:delay, funcEnd,  nil];
                [self runAction:seq];
            }
            //ITALY 
            if([mySingleton selectedTrackInt]==4 && ![mySingleton isItemUnlocked:@"car5"]){
                [mySingleton unlockItem:@"car5"];
                id delay = [CCDelayTime actionWithDuration:1.0f];
                id funcEnd = [CCCallFunc actionWithTarget:self selector:@selector(displayNewCarIcon)];
                id seq = [CCSequence actions:delay, funcEnd,  nil];
                [self runAction:seq];
            }
            //ITALY REVERSE
            if([mySingleton selectedTrackInt]==5 && ![mySingleton isItemUnlocked:@"car6"]){
                [mySingleton unlockItem:@"car6"];
                id delay = [CCDelayTime actionWithDuration:1.0f];
                id funcEnd = [CCCallFunc actionWithTarget:self selector:@selector(displayNewCarIcon)];
                id seq = [CCSequence actions:delay, funcEnd,  nil];
                [self runAction:seq];
            }
            
        }else{
        
        }
    }
}

-(void)displayNewCarIcon{
    CCSprite * newcar = [CCSprite spriteWithSpriteFrameName:@"new-car.png"];        
    newcar.position = ccp(190, 132);
    [self addChild:newcar];
    [[SimpleAudioEngine sharedEngine] playEffect: @"pickup_coin.wav"];
}
-(void)displayNewTrackIcon{
    CCSprite * newtrack = [CCSprite spriteWithSpriteFrameName:@"new-track.png"];        
    newtrack.position = ccp(190, 102);
    [self addChild:newtrack];
    [[SimpleAudioEngine sharedEngine] playEffect: @"pickup_coin.wav"];
}

-(void)wrongWayAlert:(float)angle{
    if(angle > (M_PI/1.3) || angle< (-M_PI/1.3)){
       // NSLog(@"--------------------------");
      //  NSLog(@"WRONG WAY");
      //  NSLog(@"--------------------------");
    }
}

-(void)audioToogler{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	if([mySingleton audioOn]==YES){
		
		[mySingleton setAudioOn:NO];
		SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];
		sae.backgroundMusicVolume = 0.0f;
		sae.effectsVolume = 0.0f;
	}else{
		[mySingleton setAudioOn:YES];
		SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];	
		sae.backgroundMusicVolume = 0.6f;	
		sae.effectsVolume = 0.2f;
	}
}

-(void) sendOpenFeintChallenge:(int)bestTime
{
    
}


-(void) onUpdateLap:(int)t
{
  
    
    
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    Cartopdown* hero = (Cartopdown*)[game_ hero];
    
    
    // NSLog(@"TIME STRING NEW LAP %@",newLap);
    id delay = [CCDelayTime actionWithDuration:1.3f];
    id delay2 = [CCDelayTime actionWithDuration:1.8f];
    id fade = [CCFadeIn actionWithDuration:1.0f];
    id scaleTo = [CCScaleTo actionWithDuration:0.1f scale:1.2f];
    id scaleBack = [CCScaleTo actionWithDuration:0.1f scale:1];
    id delayEnd = [CCDelayTime actionWithDuration:8.0f];
    id funcEnd = [CCCallFunc actionWithTarget:self selector:@selector(onUpdateLapEnd:)];
    id seq0 = [CCSequence actions:scaleTo, scaleBack,  nil];
    id seq1 = [CCSequence actions:delay,fade, scaleTo, scaleBack, delayEnd, nil];
    id seq2 = [CCSequence actions:delay2,fade, scaleTo, scaleBack,delayEnd, funcEnd,  nil];

    //Update lap display if less or equal totalLaps
    if([hero myLap]<=[mySingleton totalLaps]){
        NSString* numberLaps = [NSString  stringWithFormat:@"LAP %i / %i",[hero myLap],[mySingleton totalLaps]];
        [laps_ setString: numberLaps ];
        [laps_ runAction:seq0];
    }
    
    //Display time if lap major than 1
    if( [hero myLap] > 1 ){
        
        
        if(t<raceBestLap){
            raceBestLap=t;
        }
        
        // NSLog(@"LAP TIME %i",t);
        NSString* newLap = [NSString stringWithString:[mySingleton timeToString:t]];
        
        NSString* bestLap;
        
        [[GameCenterManager sharedManager] saveAndReportScore:t leaderboard:[mySingleton getLeaderboardIDFromTrack] sortOrder:GameCenterSortOrderLowToHigh];
        
        if(lapCounted < [hero myLap] && [mySingleton saveRecordForTrack:[mySingleton selectedTrack] time:t]){
            // NSLog(@"NEW RECORD  PLAY HORN!!");
            bestLap = [NSString stringWithString:[mySingleton timeToString:t]];
            [[SimpleAudioEngine sharedEngine] playEffect: @"carHorn.mp3"];
            [self displayCCSpriteMessage:@"ms-nicelap.png"];
            
             /* [OFHighScoreService setHighScore:t
             forLeaderboard:[mySingleton getLeaderboardIDFromTrack]
             onSuccess:OFDelegate()
             onFailure:OFDelegate()];
             */
        }else{
            // NSLog(@"ITS NOT A RECORD!!");
            
            bestLap = [NSString stringWithString:[mySingleton timeToString:[mySingleton getRecordForTrack:[mySingleton selectedTrack]]]];
            
        }
        lapCounted = [hero myLap];
        
        if((int)[hero myLap]==((int)[mySingleton totalLaps])){
            [self displayCCSpriteMessage:@"ms-finallap.png"];  
        }
       // [laps_ stopAllActions];
       
        
        [_lapDisplay setVisible:YES];

        
        //[_lapDisplay stopAllActions];
        [_lapDisplay runAction:_lapDisplayAction];
        
        
        
        [lastLap_ setOpacity:0.0f];
        [lastLap_ setVisible:YES];
        [lastLap_ setString: newLap];
        
        
        [bestLap_ setOpacity:0.0f];
        [bestLap_ setVisible:YES];
        [bestLap_ setString: bestLap];

       
        
        //[lastLap_ stopAllActions];
        [lastLap_ runAction:seq1];
        [bestLap_ runAction:seq2];
    }

    //		
}


-(void)updateLoop{
    GameConfiguration* config = [GameConfiguration sharedConfiguration];
//    if([config controlType]==kControlTypeAccelerometer){
//        [self debugAngle:[(JoystickAccelerometer*)joystick_ angleRadiansAccelerometer]];
//    }
    
    if([config controlType]==kControlTypeDragArrow){
         
        Cartopdown* heroCar = (Cartopdown*)[game_ hero];
        CGPoint screenPoint = CGPointMake(heroCar.position.x+_screenPosition.x, heroCar.position.y+_screenPosition.y);
        [arrowDebug setAnchorPoint:ccp(0.5, 1)];
        [arrowDebug setPosition:screenPoint];
        
        [(JoystickDragArrow*)joystick_ setPositionToDragFrom:screenPoint];
        [self debugAngle:[(JoystickDragArrow*)joystick_ angleRadians]];
        
    }
    
    
    myMarker.position = ccp([game_ hero].position.x/14.5,[game_ hero].position.y/14.5+138);
    ai1Marker.position = ccp([game_ ai1].position.x/14.5,[game_ ai1].position.y/14.5+138);
    ai2Marker.position = ccp([game_ ai2].position.x/14.5,[game_ ai2].position.y/14.5+138);
    ai3Marker.position = ccp([game_ ai3].position.x/14.5,[game_ ai3].position.y/14.5+138);
    ai4Marker.position = ccp([game_ ai4].position.x/14.5,[game_ ai4].position.y/14.5+138);
    ai5Marker.position = ccp([game_ ai5].position.x/14.5,[game_ ai5].position.y/14.5+138);
    
    if([[GlobalSingleton sharedInstance] bannerType]==0){
        //none
        CGSize s = [[CCDirector sharedDirector] winSize];
		[topMenu setPosition:ccp(50,s.height-20)];
		[topMenuColorBar setPosition:ccp(0,s.height-40)];
        [heroposition_ setPosition:ccp(s.width-5.5f, s.height-20.5f)];	
        [laps_ setPosition:ccp(s.width/2+15, s.height-20.5f)];
		
    }else if([[GlobalSingleton sharedInstance] bannerType]==1){
        //admob
        CGSize s = [[CCDirector sharedDirector] winSize];
		[topMenu setPosition:ccp(50,s.height-65)];
		[topMenuColorBar setPosition:ccp(0,s.height-85)];
        [heroposition_ setPosition:ccp(s.width-5.5f, s.height-65.5f)];	
        [laps_ setPosition:ccp(s.width/2+15, s.height-65.5f)];
    }else if([[GlobalSingleton sharedInstance] bannerType]==2){
        //iad
        CGSize s = [[CCDirector sharedDirector] winSize];
		[topMenu setPosition:ccp(50,s.height-50)];
		[topMenuColorBar setPosition:ccp(0,s.height-70)];
        [heroposition_ setPosition:ccp(s.width-5.5f, s.height-50.5f)];	
        [laps_ setPosition:ccp(s.width/2+15, s.height-50.5f)];

    }
}

-(void) onUpdateLapEnd:(id)sender
{
    [_lapDisplay stopAllActions];
    [lastLap_ stopAllActions];
    [bestLap_ stopAllActions];
    [_lapDisplay setVisible:NO];
    [bestLap_ setVisible:NO];
	[lastLap_ setVisible:NO];
}

-(void)debugAngle:(float)angle{
    float angleCocos = -1* CC_RADIANS_TO_DEGREES(angle);
    arrowDebug.rotation = angleCocos;
}

-(void)displayVelocity:(float)velo
{
    [velocity_ setString: [NSString stringWithFormat:@"%i Mph", (int)velo]];
	
}

-(void)displayPosition{
    Cartopdown* heroCar = (Cartopdown*)[game_ hero];
    [heroposition_ setString: [NSString stringWithFormat:@"POS %i", (int)[heroCar racePosition]]];
}

-(void) updateSteeringWheel:(float)angle
{
    [[joystick_ getChildByTag:1] setRotation:angle];
}


-(void) displayMessage:(NSString*)message
{
    
	CGSize s = [[CCDirector sharedDirector] winSize];
	
    CCLayerColor *color = [CCLayerColor layerWithColor:ccc4(32,32,32,128) width:s.width height:s.height];
    [color setPosition:ccp(0,0)];
    [self addChild:color z:0];
    
    CCLabelTTF *label = [CCLabelTTF labelWithString:message dimensions:CGSizeMake(300,40) hAlignment:kCCTextAlignmentCenter lineBreakMode:kCCLineBreakModeWordWrap fontName:@"Helvetica" fontSize:16];
    
	[self addChild:label];
	[label setPosition:ccp(s.width/2, s.height/2+20)];

	id sleep = [CCDelayTime actionWithDuration:3];
	id rot1 = [CCRotateBy actionWithDuration:0.025f angle:5];
	id rot2 = [CCRotateBy actionWithDuration:0.05f angle:-10];
	id rot3 = [rot2 reverse];
	id rot4 = [rot1 reverse];
	id seq = [CCSequence actions:rot1, rot2, rot3, rot4, nil];
	id repeat_rot = [CCRepeat actionWithAction:seq times:3];
	
	id big_seq = [CCSequence actions:sleep, repeat_rot, nil];
	id repeat_4ever = [CCRepeatForever actionWithAction:big_seq];
	[label runAction:repeat_4ever];
	
	//CCMenuItem *item1 = [CCMenuItemFont itemFromString:@"Play Again" target:self selector:@selector(playAgain:)];
	GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    
    [CCMenuItemFont setFontName:@"Helvetica"];
    CCMenuItemFont *item1;
    CCMenu *menu;
    
    //Store
    [[MKStoreKit sharedKit] startProductRequest];
    if([[MKStoreKit sharedKit] isProductPurchased:@"com.firstpixel.formulanova.full1"] == YES) {
        item1 = [CCMenuItemFont itemWithString:@"PLAY AGAIN" target:self selector:@selector(playAgain:)];
         menu = [CCMenu menuWithItems:item1, nil];
        [menu alignItemsVertically];
        [menu setPosition:ccp(s.width/2, s.height/5)];
        [self addChild:menu z:10];
	}else{
        if([mySingleton selectedTrackInt]==1 && [mySingleton isItemUnlocked:@"car1"]){
            //item1 = [CCMenuItemFont itemFromString:@"BUY PACKAGE 1" target:self selector:@selector(gameBuy:)];
            id sleepy = [CCDelayTime actionWithDuration:5];
            id buyFunc = [CCCallFunc actionWithTarget:self selector:@selector(buyNowSelector:)];
            id buySeq = [CCSequence actions:sleepy, buyFunc, nil];
            
            [self runAction:buySeq];
            
            
        }else{
            item1 = [CCMenuItemFont itemWithString:@"PLAY AGAIN" target:self selector:@selector(playAgain:)];
            menu = [CCMenu menuWithItems:item1, nil];
            [menu alignItemsVertically];
            [menu setPosition:ccp(s.width/2, s.height/5)];
            [self addChild:menu z:10];
        }
    }
}

-(void)removeFlurryTimedEvents{
    [Flurry endTimedEvent:@"TRACK_ITALY" withParameters:nil];
    [Flurry endTimedEvent:@"TRACK_ITALY_REVERSE" withParameters:nil];
    [Flurry endTimedEvent:@"TRACK_USA" withParameters:nil];
    [Flurry endTimedEvent:@"TRACK_BRAZIL" withParameters:nil];
    [Flurry endTimedEvent:@"TRACK_BRAZIL_REVERSE" withParameters:nil];
    [Flurry endTimedEvent:@"TRACK_ITALY" withParameters:nil];
    [Flurry endTimedEvent:@"TRACK_ITALY_REVERSE" withParameters:nil];
}

-(void) buyNowSelector:(id)sender{
    [self removeFlurryTimedEvents];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:0.5f scene:[[GameBuyNowScene class] scene] ] ];
}
-(void) playAgain:(id)sender
{
    [self removeFlurryTimedEvents];

	[[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:0.5f scene:[[SelectCarScene class] scene] ] ];
}
-(void) gameBuy:(id)sender{
    [self removeFlurryTimedEvents];

    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:0.5f scene:[[GameBuyNowScene class] scene] ] ];
}
-(void) mainMenu:(id)sender
{
    [self removeFlurryTimedEvents];

	[[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[MenuScene scene]]];
}

-(void) buttonRestart:(id)sender
{
    
    [[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[MenuScene scene]]];
}

-(void) buttonPause:(id)sender
{
    
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton unlockItem:@"gameIsPaused"];
	//[[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[MenuScene scene]]];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Paused" message:@"Press the button to..." delegate:self cancelButtonTitle:@"Resume" otherButtonTitles:nil];
    [alert show];
    
    [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    [CDAudioManager sharedManager].mute = TRUE;
    [[CCDirector sharedDirector] pause];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton lockItem:@"gameIsPaused"];
	
   [[CCDirector sharedDirector] resume];
    [CDAudioManager sharedManager].mute = FALSE;
    [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
}
- (void) dealloc
{
	self.lapDisplay = nil;
    self.lapDisplayAction = nil;
    [super dealloc];
    
}


#pragma mark Touch Handling

-(void) registerWithTouchDispatcher
{
	// Priorities: lower number, higher priority
	// Joystick: 10
	// GameNode (dragging objects): 50
	// HUD (dragging screen): 100
	[[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:100 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
   
	return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
}

-(void) ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event
{
}

// drag the screen
-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
	CGPoint touchLocation = [touch locationInView: [touch view]];	
	CGPoint prevLocation = [touch previousLocationInView: [touch view]];	
	
	touchLocation = [[CCDirector sharedDirector] convertToGL: touchLocation];
	prevLocation = [[CCDirector sharedDirector] convertToGL: prevLocation];


    
//drag scene	
	//CGPoint diff = ccpSub(touchLocation,prevLocation);
//	game_.cameraOffset = ccpAdd( game_.cameraOffset, diff );
}

@end
