/* cocos2d for iPhone
 *
 * http://www.cocos2d-iphone.org
 *
 * Copyright (C) 2009 Jason Booth
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the 'cocos2d for iPhone' license.
 *
 * You will find a copy of this license within the cocos2d for iPhone
 * distribution inside the "LICENSE" file.
 *
 */

#import "JoystickAccelerometer.h"
#import "cocos2d.h"


@implementation JoystickAccelerometer

@synthesize padPosition=padPosition_;
@synthesize myx,myy,angleRadiansAccelerometer;
+(id) joystick
{
	return [[[self alloc] init] autorelease];
}

-(id)init
{
	if( (self = [super init]) )
	{
        self.isTouchEnabled = YES;
        self.isAccelerometerEnabled = YES;
		
		CGSize winSize = [[CCDirector sharedDirector] winSize];
		  steeringWheel = [CCSprite spriteWithSpriteFrameName:@"steeringwheel.png"]; 
         [self addChild:steeringWheel z:10 tag:1];
         [steeringWheel setPosition:CGPointMake(90,70)];
        
        // buttons
		for( int i=0; i<JOYSTICK_A_MAX; i++) {
			
			NSString	*buttonName;
			CGPoint		pos;
			
			switch (i) {
				case JOYSTICK_A_UP:
					buttonName = @"accelerator.png";
					pos = ccp(winSize.width-37,37);
					break;
				case JOYSTICK_A_DOWN:
					buttonName = @"break.png";
					pos = ccp(winSize.width-37*3,37);
					break;
                default:
					NSAssert(NO, @"should not happen");
					break;
			}
			buttons_[i].sprite_ = [CCSprite spriteWithSpriteFrameName:buttonName];
			CGSize s = [buttons_[i].sprite_ contentSize];
			buttons_[i].sprite_.position = pos;
            
			[self addChild:buttons_[i].sprite_ z:10];
			// all buttons are enabled by default
			buttons_[i].enabled_ = YES;
			buttons_[i].isPressed_ = NO;
			buttons_[i].touch_ = nil;
			buttons_[i].bounds_ = CGRectMake( pos.x - s.width/2, pos.y - s.height/2, s.width, s.height);
        }
        padEnabled_ = YES;
        
	}
	return self;
}

- (void) dealloc
{
	// Anything to dealloc ? No.
	[super dealloc];
}

#pragma mark Joystick - TouchDispatcher



-(CGPoint) getCurrentNormalizedVelocity
{
    CGPoint	ret = CGPointZero;
    
    if( buttons_[JOYSTICK_A_UP].isPressed_ )
        ret.y = 1;
    else if( buttons_[JOYSTICK_A_DOWN].isPressed_ )
        ret.y = -1;
        
        
       ret.x = angleRadiansAccelerometer;
   
     
    ret = CGPointMake(ret.x, ret.y);
    // CCLOG(@"ret.x : %f",ret.x);
    
    return ret;
}


#define kFilterFactor 0.9
- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration{
	static float prevX=0, prevY=0;
	
	float accelX = acceleration.x * kFilterFactor + (1- kFilterFactor)*prevX;
	float accelY = acceleration.y * kFilterFactor + (1- kFilterFactor)*prevY;
	
	prevX = accelX;
	prevY = accelY;
	
	CGPoint v = ccp(  accelX, accelY);
   
    [self getAngleToAccelerometerPoint:v];
    myy = accelY;
    myx = -accelX;
    
  
}
-(void) getAngleToAccelerometerPoint:(CGPoint)point
{
    //CGSize s = [[CCDirector sharedDirector] winSize];
    float aAccelerometer = atan2(  -point.y  ,-point.x  );
    while(aAccelerometer<0)aAccelerometer+=2*M_PI;
    angleRadiansAccelerometer = aAccelerometer;
}


-(CGPoint) getCurrentVelocity
{
    return [self getCurrentNormalizedVelocity];
}    

-(void) setPadPosition:(CGPoint)pos{
    padPosition_ = pos;
    CGSize s = [spritePad_ contentSize];
    spritePad_.position = pos;
    padBounds = CGRectMake((pos.x-s.width/2), (pos.y-s.height/2), s.width, s.height);
    
}   
    
    
      /* 
    
-(CGPoint)getCurrentVelocity
{
	CGPoint ret = CGPointZero;
	if( padTouch )
		ret = CGPointMake(padCurPosition.x - padPosition_.x, padCurPosition.y - padPosition_.y);
	
	return ret;
}

    
    
    
-(CGPoint)getCurrentNormalizedVelocity
{
	CGPoint ret = CGPointZero;
	if( padTouch ) {
		ret = CGPointMake(padCurPosition.x - padPosition_.x, padCurPosition.y - padPosition_.y);
		ret = ccpNormalize(ret);
	}
	
	return ret;
}
*/
-(CGPoint)getCurrentDegreeVelocity
{
	CGPoint ret = CGPointZero;
	
	if( padTouch ) {
		float dx = padPosition_.x - padCurPosition.x;
		float dy = padPosition_.y - padCurPosition.y;
		CGPoint vel = [self getCurrentVelocity];
		vel.y = ccpLength(vel);
		vel.x = atan2f(-dy, dx) * (180/3.1415f);
		ret = vel;
	}
	return ret;
}

#pragma mark Joystick - Buttons
/*
-(BOOL) isButtonPressed:(unsigned int)buttonNumber
{
	return buttons_[buttonNumber].isPressed_;
}

-(BOOL) isButtonEnabled:(unsigned int)buttonNumber
{
	return buttons_[buttonNumber].enabled_;
}

-(void) setButton:(unsigned int)buttonNumber enabled:(BOOL)enabled
{
	if( enabled != buttons_[buttonNumber].enabled_ ) {
		buttons_[buttonNumber].enabled_ = enabled;
		buttons_[buttonNumber].sprite_.visible = enabled;
	}
}
 -(void) setPosition:(CGPoint)position forButton:(unsigned int)buttonNumber
 {
 buttons_[buttonNumber].sprite_.position = position;
 }
*/
-(BOOL) isButtonPressed:(unsigned int)buttonNumber
{
	return NO;
}

-(BOOL) isButtonEnabled:(unsigned int)buttonNumber
{
	return NO;
}

-(void) setButton:(unsigned int)buttonNumber enabled:(BOOL)enabled
{
}

-(void) setPosition:(CGPoint)position forButton:(unsigned int)buttonNumber
{
	// ignore
}


#pragma mark Joystick - Pad

-(BOOL) isPadEnabled
{
	return padEnabled_;
}

-(void) setPadEnabled:(BOOL)enabled
{
	if( enabled != padEnabled_ ) {
		padEnabled_ = enabled;
		spritePad_.visible = enabled;
	}
}
#pragma mark Joystick - touch delegate
-(void) registerWithTouchDispatcher
{
	// Priorities: lower number, higher priority
	// Joystick: 10
	// GameNode (dragging objects): 50
	// HUD (dragging screen): 100
	[[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:10 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
	CGPoint location = [[CCDirector sharedDirector] convertToGL:[touch locationInView:[touch view]]];
    
	// button ?
	for( int i=0; i < JOYSTICK_A_MAX;i++) {
		if( buttons_[i].enabled_ && CGRectContainsPoint(buttons_[i].bounds_ , location) )
		{
			buttons_[i].isPressed_ = YES;
			buttons_[i].touch_ = touch;
			buttons_[i].sprite_.color = (ccColor3B) {255,255,0};
			return YES;
		}
	}
	
	return NO;
}

-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
	for( int i=0;i < JOYSTICK_A_MAX; i++) {
		if( touch == buttons_[i].touch_ ) {
			buttons_[i].touch_ = nil;
			buttons_[i].isPressed_ = NO;
			buttons_[i].sprite_.color = (ccColor3B) {255,255,255};
			
			return;
		}
	}
}

-(void)ccTouchCancelled:(UITouch*)touch withEvent:(UIEvent*)event
{
	[self ccTouchEnded:touch withEvent:event];
}
@end
