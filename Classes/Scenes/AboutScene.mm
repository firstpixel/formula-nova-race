//
//  AboutNode.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 25/03/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//

#import "AboutScene.h"
#import "MenuScene.h"
#import "SoundMenuItem.h"


@implementation AboutScene

+(id) scene {
	CCScene *s = [CCScene node];
	id node = [AboutScene node];
	[s addChild:node];
	return s;
}

-(id) init {
	if( (self=[super init])) {
		
		CGSize size = [[CCDirector sharedDirector] winSize];
		CCSprite *background = [CCSprite spriteWithFile:@"about.gif"];
		background.position = ccp(size.width/2, size.height/2);
		[self addChild:background];
		
		// Menu: Back and Visit
		CGSize s = [[CCDirector sharedDirector] winSize];
		/*SoundMenuItem *visitButton = [SoundMenuItem itemFromNormalSpriteFrameName:@"visit-levelsvg-homepage-normal.png" selectedSpriteFrameName:@"visit-levelsvg-homepage-selected.png" target:self selector:@selector(visitHomepageCallback:)];	
		visitButton.position = ccp(s.width/2,30);*/

		SoundMenuItem *backButton = [SoundMenuItem itemFromNormalSpriteFrameName:@"back_up.png" selectedSpriteFrameName:@"back_down.png" target:self selector:@selector(backCallback:)];	
		backButton.position = ccp(5,s.height-5);
		backButton.anchorPoint = ccp(0,1);
        
		CCMenu *menu = [CCMenu menuWithItems:backButton, nil];
		menu.position = ccp(0,0);
		[self addChild: menu z:0];
        
        SoundMenuItem *soccerVirtualCupButton = [SoundMenuItem itemWithNormalSprite:[CCSprite spriteWithFile:@"link-soccervirtualcup.gif"] selectedSprite:[CCSprite spriteWithFile:@"link-soccervirtualcup.gif"] target:self selector:@selector(soccervirtualcupCallback:)];
        
        SoundMenuItem *formulaNovaButton = [SoundMenuItem itemWithNormalSprite:[CCSprite spriteWithFile:@"link-formulanova.gif"] selectedSprite:[CCSprite spriteWithFile:@"link-formulanova.gif"] target:self selector:@selector(formulanovaCallback:)];
       
        SoundMenuItem *facebookButton = [SoundMenuItem itemWithNormalSprite:[CCSprite spriteWithFile:@"link-facebook.gif"] selectedSprite:[CCSprite spriteWithFile:@"link-facebook.gif"] target:self selector:@selector(facebookCallback:)];
		
        SoundMenuItem *twitterButton = [SoundMenuItem itemWithNormalSprite:[CCSprite spriteWithFile:@"link-twitter.gif"] selectedSprite:[CCSprite spriteWithFile:@"link-twitter.gif"] target:self selector:@selector(twitterCallback:)];	
		
		CCMenu *menuGames = [CCMenu menuWithItems:soccerVirtualCupButton,formulaNovaButton,twitterButton,facebookButton, nil];
		menuGames.position = ccp(240,110);
		[menuGames alignItemsHorizontallyWithPadding:20.0f];
        [self addChild: menuGames z:0];
	}
	return self;
}

-(void)dealloc{
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [super dealloc];
}

-(void) firstpixelCallback: (id) sender {
	// Launches Safari and opens the requested web page
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.firstpixel.com/"]];
}

-(void) soccervirtualcupCallback: (id) sender {
	// Launches Safari and opens the requested web page
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/app/soccer-virtual-cup/id347634766?mt=8"]];
}

-(void) formulanovaCallback: (id) sender {
	// Launches Safari and opens the requested web page
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.formulanova.com/"]];
}

-(void) facebookCallback: (id) sender {
	// Launches Safari and opens the requested web page
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.facebook.com/pages/Firstpixel/134776246539448"]];
}

-(void) twitterCallback: (id) sender {
	// Launches Safari and opens the requested web page
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://twitter.com/#!/firstpixel"]];
}

-(void) backCallback:(ccTime)dt
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionProgressRadialCW transitionWithDuration:1.0f scene:[MenuScene scene]]];
}
@end
