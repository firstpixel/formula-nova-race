//
//  IntroNode.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 07/01/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import "IntroScene.h"
#import "MenuScene.h"
#import <sys/utsname.h>
#import "GlobalSingleton.h"
//
// This is an small Scene that makes the trasition smoother from the Defaul.png image to the menu scene
//

@implementation IntroScene

+(id) scene {
	CCScene *s = [CCScene node];
	id node = [IntroScene node];
	[s addChild:node];
	return s;
}

-(id) init {
	if( (self=[super init])) {
		// Load all the sprites/platforms now
		CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            NSLog(@"hud hd");
            //iPad retina screen
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud-hd.plist"];
            // weak ref
            spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"hud-hd.png" capacity:145];
            [self addChild:spriteSheet z:10];
        }else{
            //iPad screen
            
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud.plist"];
            // weak ref
            spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"hud.png" capacity:145];
            [self addChild:spriteSheet z:10];
        }
        
        
		CGSize size = [[CCDirector sharedDirector] winSize];
		CCSprite *background = [CCSprite spriteWithFile:@"Default.png"];
		background.rotation = -90;
		background.position = ccp(size.width/2, size.height/2);
		[self addChild:background];
		[self schedule:@selector(wait1second:) interval:1];
	}
	return self;
}

-(void)dealloc{
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [super dealloc];
}

- (NSString *)machine
{
	struct utsname systemInfo;
	uname(&systemInfo);
	return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}
- (NSString *) platformString
{
    NSString *platform = [self machine];
	if ([platform isEqualToString:@"i386"])             return @"oldIphone"; //on 32-bit Simulator
    if ([platform isEqualToString:@"x86_64"])           return @"newIphone"; //on 64-bit Simulator
    if ([platform isEqualToString:@"iPod1,1"])          return @"oldIphone"; //on iPod Touch
    if ([platform isEqualToString:@"iPod2,1"])          return @"oldIphone"; //   on iPod Touch Second Generation
    if ([platform isEqualToString:@"iPod3,1"])          return @"oldIphone"; //   on iPod Touch Third Generation
    if ([platform isEqualToString:@"iPod4,1" ])         return @"oldIphone"; //  on iPod Touch Fourth Generation
    if ([platform isEqualToString:@"iPod7,1"])          return @"newIphone"; //   on iPod Touch 6th Generation
    if ([platform isEqualToString:@"iPhone1,1"])        return @"oldIphone"; // on iPhone
    if ([platform isEqualToString:@"iPhone1,2"])        return @"oldIphone"; // on iPhone 3G
    if ([platform isEqualToString:@"iPhone2,1"])        return @"oldIphone"; // on iPhone 3GS
    if ([platform isEqualToString:@"iPad1,1"])          return @"oldIphone"; //   on iPad
    if ([platform isEqualToString:@"iPad2,1"])          return @"oldIphone"; //   on iPad 2
    if ([platform isEqualToString:@"iPad3,1"])          return @"oldIphone"; //   on 3rd Generation iPad
    if ([platform isEqualToString:@"iPhone3,1"])        return @"oldIphone"; // on iPhone 4 (GSM)
    if ([platform isEqualToString:@"iPhone3,3"])        return @"oldIphone"; // on iPhone 4 (CDMA/Verizon/Sprint)
    if ([platform isEqualToString:@"iPhone4,1"])        return @"oldIphone"; // on iPhone 4S
    if ([platform isEqualToString:@"iPhone5,1"])        return @"oldIphone"; // on iPhone 5 (model A1428, AT&T/Canada)
    if ([platform isEqualToString:@"iPhone5,2"])        return @"oldIphone"; // on iPhone 5 (model A1429, everything else)
    if ([platform isEqualToString:@"iPad3,4"])          return @"oldIphone"; // on 4th Generation iPad
    if ([platform isEqualToString:@"iPad2,5"])          return @"oldIphone"; // on iPad Mini
    if ([platform isEqualToString:@"iPhone5,3"])        return @"oldIphone"; // on iPhone 5c (model A1456, A1532 | GSM)
    if ([platform isEqualToString:@"iPhone5,4"])        return @"oldIphone"; // on iPhone 5c (model A1507, A1516, A1526 (China), A1529 | Global)
    if ([platform isEqualToString:@"iPhone6,1"])        return @"oldIphone"; // on iPhone 5s (model A1433, A1533 | GSM)
    if ([platform isEqualToString:@"iPhone6,2"])        return @"oldIphone"; // on iPhone 5s (model A1457, A1518, A1528 (China), A1530 | Global)
    if ([platform isEqualToString:@"iPad4,1"])          return @"newIphone"; // on 5th Generation iPad (iPad Air) - Wifi
    if ([platform isEqualToString:@"iPad4,2"])          return @"newIphone"; // on 5th Generation iPad (iPad Air) - Cellular
    if ([platform isEqualToString:@"iPad4,4"])          return @"oldIphone"; // on 2nd Generation iPad Mini - Wifi
    if ([platform isEqualToString:@"iPad4,5"])          return @"oldIphone"; // on 2nd Generation iPad Mini - Cellular
    if ([platform isEqualToString:@"iPad4,7"])          return @"newIphone"; // on 3rd Generation iPad Mini - Wifi (model A1599)
    if ([platform isEqualToString:@"iPhone7,1"])        return @"newIphone"; // on iPhone 6 Plus
    if ([platform isEqualToString:@"iPhone7,2"])        return @"newIphone"; // on iPhone 6
    if ([platform isEqualToString:@"iPhone8,1"])        return @"newIphone"; // on iPhone 6S
    if ([platform isEqualToString:@"iPhone8,2"])        return @"newIphone"; // on iPhone 6S Plus
    if ([platform isEqualToString:@"iPhone8,4"])        return @"newIphone"; // on iPhone SE
    
    NSLog(@"Platform: %@",platform);
    return platform;
}


-(void) wait1second:(ccTime)dt
{
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setDeviceType:[NSString stringWithString:[self platformString]]];
    CCLOG(@"%@ - %@",[[UIDevice currentDevice] name],[self platformString]);
    [[CCDirector sharedDirector] replaceScene: [CCTransitionProgressRadialCW transitionWithDuration:1.0f scene:[MenuScene scene]]];
    [self unschedule: @selector(wait1second:)];
}
@end
