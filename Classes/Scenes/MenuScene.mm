//
//  MenuNode.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 22/10/09.
//  Copyright 2009 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//


#import "MenuScene.h"
#import "SettingsScene.h"
#import "SelectTrackScene.h"
#import "SelectCarScene.h"
#import "GameBuyNowScene.h"
#import "ShoppingCartScene.h"
#import "AboutScene.h"
#import "SoundMenuItem.h"
#import "GlobalSingleton.h"
#import "MKStoreKit.h"
#import "GameConfiguration.h"
#import "AppDelegate.h"
#import "Flurry.h"


@implementation MenuScene

+(id) scene
{
	CCScene *scene = [CCScene node];
	id child = [MenuScene node];
	
	[scene addChild:child];
	return scene;
}

-(id) init
{
	if( (self=[super init]) )
	{
        if ([SimpleAudioEngine sharedEngine] != nil) {
            if([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]==NO){
                [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"menuLoungeLoop.mp3"];
                if ([[SimpleAudioEngine sharedEngine] willPlayBackgroundMusic]) {
                    [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:0.5f];
                }
                [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"menuLoungeLoop.mp3" loop:YES];
            }
        }
      //to display you have new challenges 
      //  [OFCurrentUser unviewedChallengesCount];
		
        //START CONFIGURATION DEFAULT
        [self initialConfiguration];
        
        CGSize s = [[CCDirector sharedDirector] winSize];
		CCSprite *background = [CCSprite spriteWithFile:@"menu-bg.png"];
		background.position = ccp(s.width/2, s.height/2);
		[self addChild:background z:-10];
		
        GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
        //setup default control
        
        GameConfiguration *config = [GameConfiguration sharedConfiguration];
        
        if([mySingleton getIntPrefForKey:@"com.firstpixel.formulanova.controlType"]!=0){
            if([mySingleton getIntPrefForKey:@"com.firstpixel.formulanova.controlType"]==1){
                [config setControlType:kControlTypeHit];
            }else if([mySingleton getIntPrefForKey:@"com.firstpixel.formulanova.controlType"]==2){
                [config setControlType:kControlTypeDragArrow];
            }
//            else if([mySingleton getIntPrefForKey:@"com.firstpixel.formulanova.controlType"]==3){
//               [config setControlType:kControlTypeDrag];
//            }
//            else if([mySingleton getIntPrefForKey:@"com.firstpixel.formulanova.controlType"]==3){
//                [config setControlType:kControlTypeAccelerometer];
//            }
        }else{
            [config setControlType:kControlTypeHit];
        }

        if([mySingleton getIntPrefForKey:@"com.firstpixel.formulanova.controlAssistent"]!=0){
            if([mySingleton getIntPrefForKey:@"com.firstpixel.formulanova.controlAssistent"]==1){
                [config setControlAssistent:kControlAssistentOn];
            }else if([mySingleton getIntPrefForKey:@"com.firstpixel.formulanova.controlAssistent"]==2){
                [config setControlAssistent:kControlAssistentOff];
            }
        }else{
            [config setControlAssistent:kControlAssistentOn];
            [mySingleton intPref:1 forKey:@"com.firstpixel.formulanova.controlAssistent"];
        }

        if([[MKStoreKit sharedKit] isProductPurchased:@"com.firstpixel.formulanova.full1"] == YES){
        //if(YES == YES){
            [CCMenuItemFont setFontSize:20];
            [CCMenuItemFont setFontName:@"HelveticaNeue"];
            CCMenuItemFont *play;

            play = [CCMenuItemFont itemWithString:@"PLAY" target:self selector:@selector(play:)];
            //CCMenuItemFont *playFriend = [CCMenuItemFont itemWithString:@"CHALLENGE A FRIEND" target:self selector:@selector(playFriend:)];
            CCMenuItemFont *playGhost = nil;
            if([[GlobalSingleton sharedInstance] isItemUnlocked:[NSString stringWithFormat:@"ghost%i",0]]){
                playGhost = [CCMenuItemFont itemWithString:@"CHALLENGE MYSELF" target:self selector:@selector(playGhost:)];
            }

            CCMenuItemFont *shopping = [CCMenuItemFont itemWithString:@"STORE" target:self selector:@selector(shopping:)];
            CCMenuItemFont *settings = [CCMenuItemFont itemWithString:@"SETTINGS" target:self selector:@selector(settings:)];
            CCMenuItemFont *leaderboard = [CCMenuItemFont itemWithString:@"LEADERBOARD" target:self selector:@selector(startLeaderboard:)];
            CCMenuItemFont *about = [CCMenuItemFont itemWithString:@"ABOUT" target:self selector:@selector(about:)];
            
            CCMenu *menu = nil;
            if(playGhost!=nil){
                menu = [CCMenu menuWithItems:play, playGhost, shopping, settings, leaderboard, about,  nil];
            }else{
                menu = [CCMenu menuWithItems:play, settings, leaderboard, about,  nil];
            }[menu alignItemsVerticallyWithPadding: 10];
            [menu setPosition:ccp(s.width/2, s.height/2 - 40)];
            [self addChild:menu];
        }else{
            
            [CCMenuItemFont setFontSize:22];
            [CCMenuItemFont setFontName:@"HelveticaNeue"];
            CCMenuItemFont *play;
           
            play = [CCMenuItemFont itemWithString:@"PLAY" target:self selector:@selector(play:)];
            //CCMenuItemFont *playFriend = [CCMenuItemFont itemWithString:@"CHALLENGE A FRIEND" target:self selector:@selector(playFriend:)];
            CCMenuItemFont *playGhost = nil;
            if([[GlobalSingleton sharedInstance] isItemUnlocked:[NSString stringWithFormat:@"ghost%i",0]]){
                playGhost = [CCMenuItemFont itemWithString:@"CHALLENGE MYSELF" target:self selector:@selector(playGhost:)];
            }
            CCMenuItemFont *shopping = [CCMenuItemFont itemWithString:@"STORE" target:self selector:@selector(shopping:)];
            CCMenuItemFont *settings = [CCMenuItemFont itemWithString:@"SETTINGS" target:self selector:@selector(settings:)];
            CCMenuItemFont *leaderboard = [CCMenuItemFont itemWithString:@"LEADERBOARD" target:self selector:@selector(startLeaderboard:)];
            CCMenuItemFont *about = [CCMenuItemFont itemWithString:@"ABOUT" target:self selector:@selector(about:)];
            
            CCMenu *menu = nil;
            if(playGhost!=nil){
                menu = [CCMenu menuWithItems:play, playGhost, shopping, settings, leaderboard, about,  nil];
            }else{
                menu = [CCMenu menuWithItems:play, shopping, settings, leaderboard, about,  nil];
            }
            [menu alignItemsVerticallyWithPadding: 12];
            [menu setPosition:ccp(s.width/2, s.height/2 - 20)];
            [self addChild:menu];
        }
        
	}
	return self;
}

-(void) startLeaderboard:(id)sender
{
   /***
   OPEN GAME CENTER LEADERBOARD 
    ***/
    RootViewController *rootController =(RootViewController*)[[(AppDelegate*)
                                                               [[UIApplication sharedApplication]delegate] window] rootViewController];
    
    [[GameCenterManager sharedManager] presentLeaderboardsOnViewController:rootController withLeaderboard:@"865696"];
    
    
    //TODO Add this to the response of the buyer
    /*RootViewController *rootController =(RootViewController*)[[(AppDelegate*)
                                                               [[UIApplication sharedApplication]delegate] window] rootViewController];
    [(RootViewController*)rootController removeBanner];
     */
}

-(void) gamebuy:(id)sender
{
    [[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[GameBuyNowScene scene]]];
}

-(void) play:(id)sender
{
    [[GlobalSingleton sharedInstance] setGameType:1];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[SelectCarScene scene]]];
}

-(void) playFriend:(id)sender
{
    [[GlobalSingleton sharedInstance] setGameType:2];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[SelectCarScene scene]]];
}

-(void) playGhost:(id)sender
{
    [[GlobalSingleton sharedInstance] setGameType:3];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[SelectCarScene scene]]];
}

-(void) shopping:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[ShoppingCartScene scene]]];
}

-(void) settings:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[SettingsScene scene] withColor:ccWHITE]];
}

-(void) about:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionFlipY transitionWithDuration:1 scene:[AboutScene scene]]];	
}

-(void)dealloc{
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [super dealloc];
    }

-(void)initialConfiguration{
    [Flurry logEvent:@"CONTROL_HIT" timed:NO];
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    GameConfiguration *config = [GameConfiguration sharedConfiguration];
    config.controlType = kControlTypeHit;
    [mySingleton intPref:1 forKey:@"com.firstpixel.formulanova.controlType"];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[SelectCarScene scene] withColor:ccWHITE]];
    [Flurry logEvent:@"DRIVING_ASSISTENT_ON" timed:NO];
    config.controlAssistent = kControlAssistentOn;
    [mySingleton intPref:1 forKey:@"com.firstpixel.formulanova.controlAssistent"];
}

@end
