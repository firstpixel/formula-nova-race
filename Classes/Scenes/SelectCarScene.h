//
//  SelectCarScene.h
//  SillyRace
//
//  Created by Gil Beyruth on 7/18/11.
//  Copyright 2011 Sapus Media. All rights reserved.
//


#import "cocos2d.h"


@interface SelectCarScene : CCLayer {
    NSString* carColor;
    int carType;
    CCSpriteBatchNode *spriteSheet;
    
}
+(id) scene;
-(void) selectTrackScene:(id)sender;

-(void) selectColorRed;
-(void) selectColorBlue;
-(void) selectColorYellow;
-(void) selectColorGray;
-(void) selectColorWhite;

@end
