//
//  SelectCarScene.cpp
//  SillyRace
//
//  Created by Gil Beyruth on 7/18/11.
//  Copyright 2011 Sapus Media. All rights reserved.
//

#import "SelectCarScene.h"
#import "SelectTrackScene.h"
#import "MenuScene.h"
#import "SoundMenuItem.h"
#import "GlobalSingleton.h"
#import "SimpleAudioEngine.h"
#import "MKStoreKit.h"
#import "Flurry.h"
@implementation SelectCarScene

+(id) scene
{
	CCScene *scene = [CCScene node];
	id child = [SelectCarScene node];
	
	[scene addChild:child];
	return scene;
}

-(id) init
{
	if( (self=[super init]) )
	{
        if ([SimpleAudioEngine sharedEngine] != nil) {
            if([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]==NO){
               // NSLog(@"START PLAYING AUDIO " );
                [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"menuLoungeLoop.mp3"];
                if ([[SimpleAudioEngine sharedEngine] willPlayBackgroundMusic]) {
                    [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:0.5f];
                }
                [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"menuLoungeLoop.mp3" loop:YES];
            }
        }
        
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud-hd.plist"];
            // weak ref
            spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"hud-hd.png" capacity:145];
            [self addChild:spriteSheet z:10];
        }else{
            //iPad screen
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud.plist"];
            // weak ref
            spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"hud.png" capacity:145];
            [self addChild:spriteSheet z:10];
        }
		
		CGSize s = [[CCDirector sharedDirector] winSize];
		CCSprite *background = [CCSprite spriteWithFile:@"select-car.gif"];
		background.position = ccp(s.width/2, s.height/2);
		[self addChild:background z:-10];
		
        GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
        if([mySingleton carColorP1]==nil)[mySingleton setCarColorP1:@"red"];
        if([mySingleton carTypeP1]==0)[mySingleton setCarTypeP1:0];
        
        //rotating car
        CCSprite *carRotating = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"car%i_%@.png",[mySingleton carTypeP1],[mySingleton carColorP1]]];
		carRotating.position = ccp(384, 80);
        [spriteSheet addChild:carRotating z:1 tag:101];
        CCRotateBy* rotateBy = [CCRotateBy actionWithDuration:2 angle:359];
        CCRepeatForever* repeat = [CCRepeatForever actionWithAction:rotateBy];
        [carRotating runAction:repeat];
        
        //Display car 
        CCSprite *carDisplay = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"car%i_display.png",[mySingleton carTypeP1]]];
		carDisplay.position = ccp(150, 124);
        [spriteSheet addChild:carDisplay];
        
        //Engine car 
        CCSprite *carEngine = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"car_engine_%i.png",[mySingleton carTypeP1]]];
		carEngine.position = ccp(344, 190);
        [spriteSheet addChild:carEngine];
                
        //Color Button 
        //red
        CCSprite *red = [CCSprite spriteWithSpriteFrameName:@"bt_red.png"];
        CCSprite *redSelected = [CCSprite spriteWithSpriteFrameName:@"bt_red.png"];
        CCMenuItem *redMenu = [CCMenuItemSprite itemFromNormalSprite:red selectedSprite:redSelected target:self selector:@selector(selectColorRed:)];
		
        //blue
        CCSprite *blue = [CCSprite spriteWithSpriteFrameName:@"bt_blue.png"];
        CCSprite *blueSelected = [CCSprite spriteWithSpriteFrameName:@"bt_blue.png"];
        CCMenuItem *blueMenu = [CCMenuItemSprite itemFromNormalSprite:blue selectedSprite:blueSelected target:self selector:@selector(selectColorBlue:)];
        
        //yellow
        CCSprite *yellow = [CCSprite spriteWithSpriteFrameName:@"bt_yellow.png"];
        CCSprite *yellowSelected = [CCSprite spriteWithSpriteFrameName:@"bt_yellow.png"];
        CCMenuItem *yellowMenu = [CCMenuItemSprite itemFromNormalSprite:yellow selectedSprite:yellowSelected target:self selector:@selector(selectColorYellow:)];
		
        //gray
        CCSprite *gray = [CCSprite spriteWithSpriteFrameName:@"bt_grey.png"];
        CCSprite *graySelected = [CCSprite spriteWithSpriteFrameName:@"bt_grey.png"];
        CCMenuItem *grayMenu = [CCMenuItemSprite itemFromNormalSprite:gray selectedSprite:graySelected target:self selector:@selector(selectColorGray:)];
       
        //white
        CCSprite *white = [CCSprite spriteWithSpriteFrameName:@"bt_white.png"];
        CCSprite *whiteSelected = [CCSprite spriteWithSpriteFrameName:@"bt_white.png"];
        CCMenuItem *whiteMenu = [CCMenuItemSprite itemFromNormalSprite:white selectedSprite:whiteSelected target:self selector:@selector(selectColorWhite:)];
       
        //CCMenuItem *item0 = [CCMenuItemFont itemFromString:@"Select Track" target:self selector:@selector(selectTrackScene:)];
		CCMenu *menuColor = [CCMenu menuWithItems: redMenu, blueMenu, yellowMenu, grayMenu, whiteMenu, nil];
		
		[menuColor alignItemsHorizontallyWithPadding:1];
		[menuColor setPosition:ccp(380,128)];
		[self addChild:menuColor];
        
        //MENU BUTTON
        CCMenuItem *item0;
        CCMenuItem *item2;
        //Botton menu payed
        if([mySingleton carTypeP1]!=0){
            //first Item
            CCSprite *previousItem = [CCSprite spriteWithSpriteFrameName:@"previous.png"];
            CCSprite *previousItemSelected = [CCSprite spriteWithSpriteFrameName:@"previous-over.png"];
            item0 = [CCMenuItemSprite itemWithNormalSprite:previousItem selectedSprite:previousItemSelected target:self selector:@selector(previousCarScene:)];
        }
        
        CCMenuItem *item1;
        CCMenu *menu;
        int isSelect = 0;
        //*****************************
        //check if has opened the item
		//*****************************
        if([mySingleton isItemUnlocked:[NSString stringWithFormat:@"car%i",[mySingleton carTypeP1]]]){
            CCSprite *selectItem = [CCSprite spriteWithSpriteFrameName:@"select.png"];
            CCSprite *selectItemSelected = [CCSprite spriteWithSpriteFrameName:@"select-over.png"];

            item1 = [CCMenuItemSprite itemWithNormalSprite:selectItem selectedSprite:selectItemSelected target:self selector:@selector(selectTrackScene:)];
            isSelect = 1;
        }else{
            CCSprite *selectItem = [CCSprite spriteWithSpriteFrameName:@"locked.png"];
            CCSprite *selectItemSelected = [CCSprite spriteWithSpriteFrameName:@"locked-over.png"];
            item1 = [CCMenuItemSprite itemFromNormalSprite:selectItem selectedSprite:selectItemSelected target:self selector:@selector(displayUnlockTipBuy:)];
        }
		
        if([mySingleton carTypeP1]!=6){
            CCSprite *nextItem = [CCSprite spriteWithSpriteFrameName:@"next.png"];
            CCSprite *nextItemSelected = [CCSprite spriteWithSpriteFrameName:@"next-over.png"];
            item2 = [CCMenuItemSprite itemFromNormalSprite:nextItem selectedSprite:nextItemSelected target:self selector:@selector(nextCarScene:)];
            
        }
		//CCMenuItem *item0 = [CCMenuItemFont itemFromString:@"Select Track" target:self selector:@selector(selectTrackScene:)];
		 if([mySingleton carTypeP1]==0){
            menu = [CCMenu menuWithItems: item1, item2, nil];
            [menu setPosition:ccp(414,25)];
		}else if([mySingleton carTypeP1]==6){
            menu = [CCMenu menuWithItems: item0, item1, nil];
            [menu setPosition:ccp(354,25)];
        }else {
            menu = [CCMenu menuWithItems: item0, item1, item2, nil];
            [menu setPosition:ccp(384,25)];
        }
        
        if(isSelect){
            [menu alignItemsHorizontallyWithPadding:30]; 
        }else{
            [menu alignItemsHorizontallyWithPadding:10];
        }
        
		[self addChild:menu];
		
		// back button
		SoundMenuItem *backButton = [SoundMenuItem itemFromNormalSpriteFrameName:@"back_up.png" selectedSpriteFrameName:@"back_down.png" target:self selector:@selector(backCallback:)];	
		backButton.position = ccp(5,s.height-5);
		backButton.anchorPoint = ccp(0,1);
		
		CCMenu *menuBack = [CCMenu menuWithItems:backButton, nil];
		menuBack.position = ccp(0,0);
		[self addChild:menuBack z:0];
        
        if([[GlobalSingleton sharedInstance] bannerType]==2){
            //iad
            backButton.position = ccp(5,s.height-35);
        }
		[Flurry logEvent:[NSString stringWithFormat:@"CAR_SCREEN_%i",[mySingleton carTypeP1]] timed:NO];
        
	}
	
	return self;
}

-(void)dealloc{
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [super dealloc];
}

-(void)changeRotatingCar{
    [spriteSheet removeChildByTag:101 cleanup:YES];
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    CCSprite *carRotating = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"car%i_%@.png",[mySingleton carTypeP1],[mySingleton carColorP1]]];
    carRotating.position = ccp(384, 80);
    [spriteSheet addChild:carRotating z:1 tag:101];
    CCRotateBy* rotateBy = [CCRotateBy actionWithDuration:2 angle:360];
    CCRepeatForever* repeat = [CCRepeatForever actionWithAction:rotateBy];
    [carRotating runAction:repeat];
    [[SimpleAudioEngine sharedEngine] playEffect: @"paint.mp3"];
}

-(void) selectColorRed:(id)sender{
    
    [Flurry logEvent:@"CAR_COLOR_RED" timed:NO];
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setCarColorP1:@"red"];
    [self changeRotatingCar];
}

-(void) selectColorBlue:(id)sender{
    [Flurry logEvent:@"CAR_COLOR_BLUE" timed:NO];
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setCarColorP1:@"blue"];
    [self changeRotatingCar];
}

-(void) selectColorYellow:(id)sender{
    [Flurry logEvent:@"CAR_COLOR_YELLOW" timed:NO];
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setCarColorP1:@"yellow"];
    [self changeRotatingCar];
}

-(void) selectColorGray:(id)sender{
    [Flurry logEvent:@"CAR_COLOR_GRAY" timed:NO];
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setCarColorP1:@"gray"];
    [self changeRotatingCar];
}

-(void) selectColorWhite:(id)sender{
    [Flurry logEvent:@"CAR_COLOR_WHITE" timed:NO];
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setCarColorP1:@"white"];
    [self changeRotatingCar];
}

-(void) displayUnlockTipBuy:(id)sender
{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    if([[MKStoreKit sharedKit] isProductPurchased:@"com.firstpixel.formulanova.full1"]!=YES){
        if([mySingleton carTypeP1]==1){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Cameron Turbo" message:@"Must finish first to third place at Australia Track." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [[CCDirector sharedDirector] presentViewController:alertController animated:YES completion:nil];
            
        }else
        if([mySingleton carTypeP1]==2){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"P. Carrera"
                                                                              message:@"Must finish first to third place at Australia Track Reverse, and must have package 1. To buy package 1, go to main menu and click on 'Package 1 (Buy Now)'"
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [[CCDirector sharedDirector] presentViewController:alertController animated:YES completion:nil];
        }else if([mySingleton carTypeP1]==3){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Vip Turbo"
                                                                                     message:@"Must finish first to third place at Brazilian Track, and must have package 1. To buy package 1, go to main menu and click on 'Package 1 (Buy Now)'"
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [[CCDirector sharedDirector] presentViewController:alertController animated:YES completion:nil];
        }else if([mySingleton carTypeP1]==4){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"F. Concept"
                                                                                     message:@"Must finish first to third place at Brazilian Track Reverse, and must have package 1. To buy package 1, go to main menu and click on 'Package 1 (Buy Now)'"
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [[CCDirector sharedDirector] presentViewController:alertController animated:YES completion:nil];
        }else if([mySingleton carTypeP1]==5){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Turbo Concept"
                                                                                     message:@"Must finish first to third place at American Track, and must have package 1. To buy package 1, go to main menu and click on 'Package 1 (Buy Now)'"
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [[CCDirector sharedDirector] presentViewController:alertController animated:YES completion:nil];
        }else if([mySingleton carTypeP1]==6){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"El Diablo"
                                                                                     message:@"Must finish first to third place at Italian Track, and must have package 1. To buy package 1, go to main menu and click on 'Package 1 (Buy Now)'"
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [[CCDirector sharedDirector] presentViewController:alertController animated:YES completion:nil];
        }
    }else{
    //ALREADY BOUGHT
        if([mySingleton carTypeP1]==1){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Cameron Turbo" message:@"Must finish first to third place at Australia Track." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [[CCDirector sharedDirector] presentViewController:alertController animated:YES completion:nil];
        }else
            if([mySingleton carTypeP1]==2){
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"P. Carrera"
                                                                                         message:@"Must finish first to third place at Australia Track Reverse."
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                
                [[CCDirector sharedDirector] presentViewController:alertController animated:YES completion:nil];
            }else if([mySingleton carTypeP1]==3){
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Vip Turbo"
                                                                                         message:@"Must finish first to third place at Brazilian Track."
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                
                [[CCDirector sharedDirector] presentViewController:alertController animated:YES completion:nil];
            }else if([mySingleton carTypeP1]==4){
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"F. Concept"
                                                                                         message:@"Must finish first to third place at Brazilian Track Reverse."
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                
                [[CCDirector sharedDirector] presentViewController:alertController animated:YES completion:nil];
            }else if([mySingleton carTypeP1]==5){
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Turbo Concept"
                                                                                         message:@"Must finish first to third place at American Track."
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                
                [[CCDirector sharedDirector] presentViewController:alertController animated:YES completion:nil];
            }else if([mySingleton carTypeP1]==6){
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"El Diablo"
                                                                                         message:@"Must finish first to third place at Italian Track."
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                
                [[CCDirector sharedDirector] presentViewController:alertController animated:YES completion:nil];
            }
    }
    
}

-(void) selectTrackScene:(id)sender
{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [Flurry logEvent:[NSString stringWithFormat:@"SELECTED_CAR_TYPE_%i_COLOR_%@",[mySingleton carTypeP1], [mySingleton carColorP1]] timed:NO];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionSplitRows transitionWithDuration:0.5 scene:[SelectTrackScene scene]]];
}

-(void) nextCarScene:(id)sender
{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setCarTypeP1:[mySingleton carTypeP1]+1];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionSlideInR transitionWithDuration:0.5 scene:[SelectCarScene scene]]];
}

-(void) previousCarScene:(id)sender
{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton carTypeP1]!=0)[mySingleton setCarTypeP1:[mySingleton carTypeP1]-1];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionSlideInL transitionWithDuration:0.5 scene:[SelectCarScene scene]]];
}

-(void) backCallback:(id)sender
{
    
    [[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:0.5 scene:[MenuScene scene]]];
}
@end
