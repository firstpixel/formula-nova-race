//
//  SelectLevelNode.h
//  LevelSVG
//
//  Created by Ricardo Quesada on 25/03/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//

#import "cocos2d.h"
#import "SimpleAudioEngine.h"

@interface SelectTrackScene : CCLayer {

    CCMenuItem *levelEasy;
    CCMenuItem *levelMedium;
    CCMenuItem *levelHard;
    
    
}
+(id) scene;

-(void)selectLevelEasy:(id)sender;
-(void)selectLevelMedium:(id)sender;
-(void)selectLevelHard:(id)sender;
-(void)trackToString;
-(void)backCallback:(id)sender;
-(void)loadingScreen:(id)sender;
@end
