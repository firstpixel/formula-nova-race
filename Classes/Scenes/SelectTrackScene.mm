//
//  SelectLevelScene.mm
//  LevelSVG
//
//  Created by Ricardo Quesada on 25/03/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//

#import "SelectTrackScene.h"
#import "TrackAustralia.h"
#import "TrackAustraliaReverse.h"
#import "TrackBrazil.h"
#import "TrackBrazilReverse.h"
#import "TrackUSA.h"
#import "TrackItaly.h"
#import "TrackItalyReverse.h"
#import "SelectCarScene.h"
#import "SoundMenuItem.h"
#import "GlobalSingleton.h"
#import "MKStoreKit.h"

@implementation SelectTrackScene

+(id) scene
{
	CCScene *scene = [CCScene node];
	id child = [SelectTrackScene node];
	
	[scene addChild:child];
	return scene;
}
//[[GlobalSingleton sharedInstance] unlockItem:[NSString stringWithFormat:@"ghost%i",[[GlobalSingleton sharedInstance] selectedTrackInt]]];
-(id) init
{
	if( (self=[super init]) )
	{
		if ([SimpleAudioEngine sharedEngine] != nil) {
            if([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]==NO){
               // NSLog(@"START PLAYING AUDIO " );
                [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"menuLoungeLoop.mp3"];
                if ([[SimpleAudioEngine sharedEngine] willPlayBackgroundMusic]) {
                    [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:0.5f];
                }
                [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"menuLoungeLoop.mp3" loop:YES];
            }
        }
        
        GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
        [self trackToString];
       
		CGSize s = [[CCDirector sharedDirector] winSize];
		CCSprite *background = [CCSprite spriteWithFile:@"select-track.gif"];
		background.position = ccp(s.width/2, s.height/2);
		[self addChild:background z:-10];
		 
        //yellow
        CCSprite *easy = [CCSprite spriteWithSpriteFrameName:@"easy.png"];
        CCSprite *easySelected = [CCSprite spriteWithSpriteFrameName:@"easy-selected.png"];
        levelEasy = [CCMenuItemSprite itemWithNormalSprite:easy selectedSprite:easySelected target:self selector:@selector(selectLevelEasy:)];
        [levelEasy  activate];
        [levelEasy  selected];
		
        //gray
        CCSprite *medium = [CCSprite spriteWithSpriteFrameName:@"medium.png"];
        CCSprite *mediumSelected = [CCSprite spriteWithSpriteFrameName:@"medium-selected.png"];
        levelMedium = [CCMenuItemSprite itemWithNormalSprite:medium selectedSprite:mediumSelected target:self selector:@selector(selectLevelMedium:)];
        
        //white
        CCSprite *hard = [CCSprite spriteWithSpriteFrameName:@"hard.png"];
        CCSprite *hardSelected = [CCSprite spriteWithSpriteFrameName:@"hard-selected.png"];
        levelHard = [CCMenuItemSprite itemWithNormalSprite:hard selectedSprite:hardSelected target:self selector:@selector(selectLevelHard:)];
        
        CCMenu *menuLevel = [CCMenu menuWithItems: levelEasy, levelMedium, levelHard, nil];
		[menuLevel alignItemsHorizontallyWithPadding:1];
		[menuLevel setPosition:ccp(380,138)];
		[self addChild:menuLevel];
        
        //Clear track name
        NSString* cleanSelectedTrack = [[mySingleton selectedTrack] stringByReplacingOccurrencesOfString:@"Reverse" withString:@""];
        
        //Display car 
        CCSprite *descriptionSprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@-turbo-fix.png",cleanSelectedTrack]];
        descriptionSprite.position = ccp(375, 78);
        [self addChild:descriptionSprite];
        
        NSRange textRange;
        textRange =[[mySingleton selectedTrack] rangeOfString:@"Reverse"];
        CCSprite *trackDisplaySprite;
        if(textRange.location != NSNotFound)
        {
           trackDisplaySprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@-display-track-reverse.png",cleanSelectedTrack]];
            //Does contain the substring
        }else{
            trackDisplaySprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@-display-track.png",cleanSelectedTrack]];
        }
        trackDisplaySprite.position = ccp(150, 90);
        [self addChild:trackDisplaySprite];
        
        //NSLog(@"TROPHY %i",[mySingleton selectedTrackInt]);
        
        if([mySingleton isItemUnlocked:[NSString stringWithFormat:@"track%itrophy",[mySingleton selectedTrackInt]]]){
           // NSLog(@"TROPHY UNLOCKED!");
            CCSprite *trophy = [CCSprite spriteWithSpriteFrameName:@"trophy.png"];
            trophy.position = ccp(30,150);
            [self addChild:trophy]; 
        }
        
        /******************************************************/
        /***********          MENU SELECT           ***********/
        /******************************************************/
        
        //MENU BUTTON
        CCMenuItem *item0;
        CCMenuItem *item2;
        //Botton menu payed
        if([mySingleton selectedTrackInt]!=0){
            //first Item
            CCSprite *previousItem = [CCSprite spriteWithSpriteFrameName:@"previous.png"];
            CCSprite *previousItemSelected = [CCSprite spriteWithSpriteFrameName:@"previous-over.png"];
            item0 = [CCMenuItemSprite itemWithNormalSprite:previousItem selectedSprite:previousItemSelected target:self selector:@selector(previousTrackScene:)];
        }
        CCMenuItem *item1;
        CCMenu *menu;
        int isSelect = 0;
        
        //*****************************
        //check if has opened the item
		//*****************************
        if([[GlobalSingleton sharedInstance] gameType] == 3) {
            if([mySingleton isItemUnlocked:[NSString stringWithFormat:@"track%i",[mySingleton selectedTrackInt]]]
               && [mySingleton isItemUnlocked:[NSString stringWithFormat:@"ghost%i",[mySingleton selectedTrackInt]]]){
                CCSprite *selectItem = [CCSprite spriteWithSpriteFrameName:@"select.png"];
                CCSprite *selectItemSelected = [CCSprite spriteWithSpriteFrameName:@"select-over.png"];
                item1 = [CCMenuItemSprite itemWithNormalSprite:selectItem selectedSprite:selectItemSelected target:self selector:@selector(loadingScreen:)];
                isSelect = 1;
            }else{
                CCSprite *selectItem = [CCSprite spriteWithSpriteFrameName:@"locked.png"];
                CCSprite *selectItemSelected = [CCSprite spriteWithSpriteFrameName:@"locked-over.png"];
                item1 = [CCMenuItemSprite itemWithNormalSprite:selectItem selectedSprite:selectItemSelected target:self selector:@selector(displayUnlockGhostTip:)];
            }
            
            if([mySingleton selectedTrackInt]!=6){
                CCSprite *nextItem = [CCSprite spriteWithSpriteFrameName:@"next.png"];
                CCSprite *nextItemSelected = [CCSprite spriteWithSpriteFrameName:@"next-over.png"];
                item2 = [CCMenuItemSprite itemWithNormalSprite:nextItem selectedSprite:nextItemSelected target:self selector:@selector(nextTrackScene:)];
            }
            
        
        }else{
            if([mySingleton isItemUnlocked:[NSString stringWithFormat:@"track%i",[mySingleton selectedTrackInt]]]){
                CCSprite *selectItem = [CCSprite spriteWithSpriteFrameName:@"select.png"];
                CCSprite *selectItemSelected = [CCSprite spriteWithSpriteFrameName:@"select-over.png"];
                item1 = [CCMenuItemSprite itemWithNormalSprite:selectItem selectedSprite:selectItemSelected target:self selector:@selector(loadingScreen:)];
                isSelect = 1;
            }else{
                CCSprite *selectItem = [CCSprite spriteWithSpriteFrameName:@"locked.png"];
                CCSprite *selectItemSelected = [CCSprite spriteWithSpriteFrameName:@"locked-over.png"];
                item1 = [CCMenuItemSprite itemWithNormalSprite:selectItem selectedSprite:selectItemSelected target:self selector:@selector(displayUnlockTip:)];
            }
            
            if([mySingleton selectedTrackInt]!=6){
                CCSprite *nextItem = [CCSprite spriteWithSpriteFrameName:@"next.png"];
                CCSprite *nextItemSelected = [CCSprite spriteWithSpriteFrameName:@"next-over.png"];
                item2 = [CCMenuItemSprite itemWithNormalSprite:nextItem selectedSprite:nextItemSelected target:self selector:@selector(nextTrackScene:)];
            }
            
        }
        
        // CCMenuItem *item0 = [CCMenuItemFont itemFromString:@"Select Track" target:self selector:@selector(selectTrackScene:)];
        if([mySingleton selectedTrackInt]==0){
            menu = [CCMenu menuWithItems: item1, item2, nil];
            [menu setPosition:ccp(414,25)];
		}else if([mySingleton selectedTrackInt]==6){
            menu = [CCMenu menuWithItems: item0, item1, nil];
            [menu setPosition:ccp(354,25)];
        }else {
            menu = [CCMenu menuWithItems: item0, item1, item2, nil];
            [menu setPosition:ccp(384,25)];
        }
      
        if(isSelect){
            [menu alignItemsHorizontallyWithPadding:20]; 
        }else{
            [menu alignItemsHorizontallyWithPadding:10];
        }
        
		[self addChild:menu];
		
        // record
        if( [mySingleton getRecordForTrack:[mySingleton selectedTrack]] != nil ){
            CCLabelBMFont* trackRecord = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"LAP  %@", [mySingleton timeToString:[mySingleton getRecordForTrack:[mySingleton selectedTrack]]]] fntFile:@"lap-white.fnt"];
            [trackRecord.texture setAliasTexParameters];
            [self addChild:trackRecord z:1];
            [trackRecord setPosition:ccp(115, 195)];		
        }
		
		// back button
		SoundMenuItem *backButton = [SoundMenuItem itemFromNormalSpriteFrameName:@"back_up.png" selectedSpriteFrameName:@"back_down.png" target:self selector:@selector(backCallback:)];	
		backButton.position = ccp(5,s.height-5);
		backButton.anchorPoint = ccp(0,1);
        
		menu = [CCMenu menuWithItems:backButton, nil];
		menu.position = ccp(0,0);
		[self addChild: menu z:0];
        
        if([[GlobalSingleton sharedInstance] bannerType]==2){
            //iad
            backButton.position = ccp(5,s.height-35);
        }
	}
	return self;
}

-(void) trackToString{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    switch([mySingleton selectedTrackInt]){
        case 0:
            [mySingleton setSelectedTrack:@"usa"];
            break;
        case 1:
            [mySingleton setSelectedTrack:@"australia"];
            break;
        case 2:
            [mySingleton setSelectedTrack:@"australiaReverse"];
            break;
        case 3:
            [mySingleton setSelectedTrack:@"brazil"];
            break;
        case 4:
            [mySingleton setSelectedTrack:@"brazilReverse"];
            break;
       case 5:
            [mySingleton setSelectedTrack:@"italy"];
            break;
        case 6:
            [mySingleton setSelectedTrack:@"italyReverse"];
            break; 
    }
}

-(void) setTrackScene:(Class)klass
{
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    [SimpleAudioEngine end];
	[[CCDirector sharedDirector] replaceScene: [CCTransitionFlipY transitionWithDuration:1 scene:[klass scene]]];	
   // [[CCDirector sharedDirector] replaceScene:[CCTransitionSplitRows transitionWithDuration:1 scene:[klass scene]]];
}

-(void)selectLevelEasy:(id)sender{
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    [levelEasy selected];
    [levelMedium  unselected];
    [levelHard  unselected];
    [mySingleton setGameLevel:0];
}
-(void)selectLevelMedium:(id)sender{
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    [levelEasy unselected];
    [levelMedium  selected];
    [levelHard  unselected];
    [mySingleton setGameLevel:1];
}
-(void)selectLevelHard:(id)sender{
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    [levelEasy unselected];
    [levelMedium unselected];
    [levelHard selected];
    [mySingleton setGameLevel:2];
}

-(void) selectTrack:(id)sender
{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [self trackToString];
    switch([mySingleton selectedTrackInt]){
        case 0:
            [self setTrackScene:[TrackUSA class]];
            break;
        case 1:
            [self setTrackScene:[TrackAustralia class]];
            break;
        case 2:
            [self setTrackScene:[TrackAustraliaReverse class]];
            break;
        case 3:
            [self setTrackScene:[TrackBrazil class]];
            break;
        case 4:
            [self setTrackScene:[TrackBrazilReverse class]];
            break;
        case 5:
            [self setTrackScene:[TrackItaly class]];
            break;
        case 6:
            [self setTrackScene:[TrackItalyReverse class]];
            break;
        }
}

-(void) nextTrackScene:(id)sender
{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setSelectedTrackInt:[mySingleton selectedTrackInt]+1];
	[[CCDirector sharedDirector] replaceScene: [CCTransitionSlideInR transitionWithDuration:0.5 scene:[SelectTrackScene scene]]];
}

-(void) previousTrackScene:(id)sender
{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton selectedTrackInt]!=0)[mySingleton setSelectedTrackInt:[mySingleton selectedTrackInt]-1];
	[[CCDirector sharedDirector] replaceScene: [CCTransitionSlideInL transitionWithDuration:0.5 scene:[SelectTrackScene scene]]];
}

-(void) displayUnlockGhostTip:(id)sender
{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    if(![[MKStoreKit sharedKit] isProductPurchased:@"com.firstpixel.formulanova.full1"]){
        if([mySingleton selectedTrackInt]==1){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Australia"
                                                            message:@"Must have one lap on the american race."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }else if([mySingleton selectedTrackInt]==2){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Australia Reverse"
                                                            message:@"Must have one lap on the australian race."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }else if([mySingleton selectedTrackInt]==3){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Brazil"
                                                            message:@"Must have one lap on the australian race reverse, and must have package 1. To buy package 1, go to main menu and click on 'Package 1 (Buy Now)."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }else if([mySingleton selectedTrackInt]==4){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Brazil Reverse"
                                                            message:@"Must have one lap on the brazilian race, and must have package 1. To buy package 1, go to main menu and click on 'Package 1 (Buy Now)."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }else if([mySingleton selectedTrackInt]==5){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Italy"
                                                            message:@"Must have one lap on the brazilian race reverse, and must have package 1. To buy package 1, go to main menu and click on 'Package 1 (Buy Now)."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }else if([mySingleton selectedTrackInt]==6){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Italy Reverse"
                                                            message:@"Must have one lap on the italian race, and must have package 1. To buy package 1, go to main menu and click on 'Package 1 (Buy Now)."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        
    }else{
        if([mySingleton selectedTrackInt]==1){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Australia"
                                                            message:@"Must have one lap on the american race."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }else
            if([mySingleton selectedTrackInt]==2){
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Australia Reverse"
                                                                message:@"Must have one lap on the australian race."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }else if([mySingleton selectedTrackInt]==3){
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Brazil"
                                                                message:@"Must have one lap on the australian race reverse."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }else if([mySingleton selectedTrackInt]==4){
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Brazil Reverse"
                                                                message:@"Must have one lap on the brazilian race."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }else if([mySingleton selectedTrackInt]==5){
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Italy"
                                                                message:@"Must have one lap on the brasilian race reverse."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }else if([mySingleton selectedTrackInt]==6){
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Italy Reverse"
                                                                message:@"Must have one lap on the italian race."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
    }
    /*
     //UPDATA NEW TRACK CANADA
     else if([mySingleton carTypeP1]==5){
     UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Canada"
     message:@"Must finish at any place the italian race reverse."
     delegate:self
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [alert show];
     [alert release];
     }else if([mySingleton carTypeP1]==6){
     UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Canada Reverse"
     message:@"Must finish at any place the canadian race."
     delegate:self
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [alert show];
     [alert release];
     }
     */
}


-(void) displayUnlockTip:(id)sender
{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    if(![[MKStoreKit sharedKit] isProductPurchased:@"com.firstpixel.formulanova.full1"]){
        if([mySingleton selectedTrackInt]==1){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Australia"
                                                            message:@"Must finish from first to third place the american race reverse, and must have package 1. To buy package 1, go to main menu and click on 'Package 1 (Buy Now)."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }else if([mySingleton selectedTrackInt]==2){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Australia Reverse"
                                                            message:@"Must finish from first to third place the australian race."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }else if([mySingleton selectedTrackInt]==3){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Brazil"
                                                            message:@"Must finish from first to third place the australian race reverse, and must have package 1. To buy package 1, go to main menu and click on 'Package 1 (Buy Now)."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }else if([mySingleton selectedTrackInt]==4){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Brazil Reverse"
                                                            message:@"Must finish from first to third place the brazilian race, and must have package 1. To buy package 1, go to main menu and click on 'Package 1 (Buy Now)."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }else if([mySingleton selectedTrackInt]==5){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Italy"
                                                            message:@"Must finish from first to third place the brazilian race reverse, and must have package 1. To buy package 1, go to main menu and click on 'Package 1 (Buy Now)."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }else if([mySingleton selectedTrackInt]==6){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Italy Reverse"
                                                            message:@"Must finish from first to third place the italian race, and must have package 1. To buy package 1, go to main menu and click on 'Package 1 (Buy Now)."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        
    }else{
        if([mySingleton selectedTrackInt]==1){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Australia"
                                                            message:@"Must finish from first to third place the american race reverse."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }else
            if([mySingleton selectedTrackInt]==2){
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Australia Reverse"
                                                                message:@"Must finish from first to third place the australian race."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }else if([mySingleton selectedTrackInt]==3){
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Brazil"
                                                                message:@"Must finish from first to third place the australian race reverse."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }else if([mySingleton selectedTrackInt]==4){
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Brazil Reverse"
                                                                message:@"Must finish from first to third place the brazilian race."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }else if([mySingleton selectedTrackInt]==5){
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Italy"
                                                                message:@"Must finish from first to third place the brasilian race reverse."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }else if([mySingleton selectedTrackInt]==6){
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Italy Reverse"
                                                                message:@"Must finish from first to third place the italian race."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
    }
    /*
     //UPDATA NEW TRACK CANADA
     else if([mySingleton carTypeP1]==5){
     UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Canada"
     message:@"Must finish at any place the italian race reverse."
     delegate:self
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [alert show];
     [alert release];
     }else if([mySingleton carTypeP1]==6){
     UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Track Canada Reverse"
     message:@"Must finish at any place the canadian race."
     delegate:self
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [alert show];
     [alert release];
     }
     */
}

-(void)startDelayLoading:(ccTime)ts{
    [self selectTrack:self];
	[self unschedule: @selector(startDelayLoading:)];
}

-(void)loadingScreen:(id)sender{
        CCSprite * loading = [CCSprite spriteWithFile:@"loading.gif"];
		CGSize s = [[CCDirector sharedDirector] winSize];
        loading.position = ccp(s.width/2, s.height/2);
		[self addChild:loading z:5000];
        [self schedule: @selector(startDelayLoading:) interval: 1];
}

-(void) backCallback:(id)sender
{
	[[CCDirector sharedDirector] replaceScene:[CCTransitionRotoZoom transitionWithDuration:1 scene:[SelectCarScene scene]]];
}
@end
