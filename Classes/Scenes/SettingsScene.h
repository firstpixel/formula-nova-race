//
//  SettingsScene.h
//  LevelSVG
//
//  Created by Ricardo Quesada on 22/10/09.
//  Copyright 2009 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//


#import "cocos2d.h"

@interface SettingsScene : CCLayer {

    CCMenuItem *_drivingOnItem; 
    CCMenuItem *_drivingOffItem;
    CCSpriteBatchNode *spriteSheet;
}

+(id) scene;

@end
