//
//  SettingsScene.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 22/10/09.
//  Copyright 2009 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//


#import "SettingsScene.h"
#import "MenuScene.h"
#import "GameConfiguration.h"
#import "SoundMenuItem.h"
#import "GlobalSingleton.h"
#import "SelectCarScene.h"
#import "Flurry.h"
@implementation SettingsScene

+(id) scene
{
	CCScene *scene = [CCScene node];
	id child = [SettingsScene node];
	
	[scene addChild:child];
	return scene;
}

-(id) init
{
	if( (self=[super init]) )
	{
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            NSLog(@"hud hd");
            //iPad retina screen
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud-hd.plist"];
            // weak ref
            spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"hud-hd.png" capacity:145];
            [self addChild:spriteSheet z:10];
        }else{
            //iPad screen
            
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud.plist"];
            // weak ref
            spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"hud.png" capacity:145];
            [self addChild:spriteSheet z:10];
        }
        
        
        [Flurry logEvent:@"SETTINGS" timed:YES];
		[super init];
		CGSize s = [[CCDirector sharedDirector] winSize];
		
        //add bg
        CCSprite* bg = [CCSprite spriteWithFile:@"start-game.gif"];
        bg.position = ccp(s.width/2,s.height/2);
        [self addChild:bg];
        
        //add sub bg
        CCSprite* whiteBg = [CCSprite spriteWithFile:@"select-control-bg.png"];
        whiteBg.position = ccp(155,155);
        [self addChild:whiteBg];
        
        //Add icons to the screen
        CCSprite* iconClick = [CCSprite spriteWithFile:@"icon-touch.gif"];
        //iconClick.position = ccp(130,210);
        iconClick.position = ccp(s.width/2-100,s.height/2+20);
        [self addChild:iconClick];
        /*
        CCSprite* iconTilt = [CCSprite spriteWithFile:@"icon-accelerometer.gif"];
        iconTilt.position = ccp(130,160);
        [self addChild:iconTilt];
        */
        CCSprite* iconDrag = [CCSprite spriteWithFile:@"icon-drag.gif"];
        //iconDrag.position = ccp(130,160);
        iconDrag.position = ccp(s.width/2-100,s.height/2-30);
        [self addChild:iconDrag];

        //Add menu to the screen
        SoundMenuItem *item0 = [SoundMenuItem itemWithNormalSprite:[CCSprite spriteWithFile:@"click-turn.gif"] selectedSprite:[CCSprite spriteWithFile:@"click-turn-selected.gif"] target:self selector:@selector(controlHitCallback:)];
		
        SoundMenuItem *item1 = [SoundMenuItem itemWithNormalSprite:[CCSprite spriteWithFile:@"drag-rotation.gif"] selectedSprite:[CCSprite spriteWithFile:@"drag-rotation-selected.gif"] target:self selector:@selector(controlDragArrowCallback:)];

        GameConfiguration *config = [GameConfiguration sharedConfiguration];
        
        
		if( config.controlType == kControlTypeHit ){
			[Flurry logEvent:@"CONTROL_HIT" timed:NO];
            
            [item0 selected];
         	[item1 unselected];
            
        }else if( config.controlType == kControlTypeDragArrow ){
            [Flurry logEvent:@"CONTROL_DRAGARROW" timed:NO];
            
			[item0 unselected];
         	[item1 selected];
        }
/*
		CCMenuItemToggle *item3 = [CCMenuItemToggle itemWithTarget:self selector:@selector(controlFPS:) items:
								   [CCMenuItemFont itemFromString: @"Show FPS: OFF"],
								   [CCMenuItemFont itemFromString: @"Show FPS: ON"],
								   nil];

		CCMenuItemToggle *item4 = [CCMenuItemToggle itemWithTarget:self selector:@selector(controlWireframe:) items:
								   [CCMenuItemFont itemFromString: @"Wireframe: OFF"],
								   [CCMenuItemFont itemFromString: @"Wireframe: ON"],
								   nil];
*/
		CCMenu *menu1 = [CCMenu menuWithItems:
					  item0, item1, //item2, item3, item4,
						nil];
		[menu1 alignItemsVerticallyWithPadding:34.0];
		[menu1 setPosition:ccp(s.width/2+30,s.height/2)];
		[self addChild: menu1];
		
		
		//add top menu -  back button
		SoundMenuItem *backButton = [SoundMenuItem itemFromNormalSpriteFrameName:@"back_up.png" selectedSpriteFrameName:@"back_down.png" target:self selector:@selector(backCallback:)];	
		backButton.position = ccp(5,s.height-5);
		backButton.anchorPoint = ccp(0,1);
		CCMenu *menu = [CCMenu menuWithItems:backButton, nil];
		menu.position = ccp(0,0);
		[self addChild: menu z:0];
        _drivingOnItem = [[CCMenuItemImage itemWithNormalImage:@"bt-drivingassistent-on.gif"
                                            selectedImage:@"bt-drivingassistent-on.gif" target:nil selector:nil] retain];
        _drivingOffItem = [[CCMenuItemImage itemWithNormalImage:@"bt-drivingassistent.gif"
                                             selectedImage:@"bt-drivingassistent.gif" target:nil selector:nil] retain];
        CCMenuItemToggle *toggleItem = [CCMenuItemToggle itemWithTarget:self 
                                                               selector:@selector(drivingAssinstentButtonTapped:) items:_drivingOnItem, _drivingOffItem, nil];
        CCMenu *toggleMenu = [CCMenu menuWithItems:toggleItem, nil];
        toggleMenu.position = ccp(s.width/2-17,s.height/2-120);
        [self addChild:toggleMenu];
        
        if( config.controlAssistent == kControlAssistentOn ){
			[toggleItem setSelectedIndex:0];
        }else if( config.controlAssistent == kControlAssistentOff ){
			[toggleItem setSelectedIndex:1];
        }
        
        
		return self;
	}
	return self;
}

- (void)drivingAssinstentButtonTapped:(id)sender {  
    CCMenuItemToggle *toggleItem = (CCMenuItemToggle *)sender;
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
	GameConfiguration *config = [GameConfiguration sharedConfiguration];
    
    if (toggleItem.selectedItem == _drivingOnItem) 
    {
        [Flurry logEvent:@"DRIVING_ASSISTENT_ON" timed:NO];
        [config setControlAssistent:kControlAssistentOn];
        [mySingleton intPref:1 forKey:@"com.firstpixel.formulanova.controlAssistent"];
    } 
    else if (toggleItem.selectedItem == _drivingOffItem) 
    {
        [Flurry logEvent:@"DRIVING_ASSISTENT_OFF" timed:NO];
        [config setControlAssistent:kControlAssistentOff];
        [mySingleton intPref:2 forKey:@"com.firstpixel.formulanova.controlAssistent"];
    }  
}


-(void)dealloc{
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [_drivingOnItem release];
    _drivingOnItem = nil;
    [_drivingOffItem release];
    _drivingOffItem = nil;
    [super dealloc];
}


-(void) backCallback:(id)sender
{
    [Flurry endTimedEvent:@"SETTINGS" withParameters:nil];
    
	[[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[MenuScene scene] withColor:ccWHITE]];
}

-(void) controlFPS:(id)sender
{
	CCMenuItemToggle *item = (CCMenuItemToggle*) sender;	
	if( item.selectedIndex == 0 )
		[[CCDirector sharedDirector] setDisplayStats:NO];
    
	else
		[[CCDirector sharedDirector] setDisplayStats:YES];
    
}

-(void) controlWireframe:(id)sender
{
	CCMenuItemToggle *item = (CCMenuItemToggle*) sender;	
	GameConfiguration *config = [GameConfiguration sharedConfiguration];
	if( item.selectedIndex == 0 )
		config.enableWireframe = NO;
	else
		config.enableWireframe = YES;
        
}



-(void) controlHitCallback:(id)sender
{
    [Flurry logEvent:@"CONTROL_HIT" timed:NO];
    [Flurry endTimedEvent:@"SETTINGS" withParameters:nil];
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
	GameConfiguration *config = [GameConfiguration sharedConfiguration];
	config.controlType = kControlTypeHit;
    [mySingleton intPref:1 forKey:@"com.firstpixel.formulanova.controlType"];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[SelectCarScene scene] withColor:ccWHITE]];

}


-(void) controlDragArrowCallback:(id)sender
{
    [Flurry logEvent:@"CONTROL_DRAGARROW" timed:NO];
    [Flurry endTimedEvent:@"SETTINGS" withParameters:nil];
    
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
	GameConfiguration *config = [GameConfiguration sharedConfiguration];
	config.controlType = kControlTypeDragArrow;
    [mySingleton intPref:2 forKey:@"com.firstpixel.formulanova.controlType"];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[SelectCarScene scene] withColor:ccWHITE]];
    
}
-(void) controlDragCallback:(id)sender
{
    [Flurry logEvent:@"CONTROL_DRAG" timed:NO];
    [Flurry endTimedEvent:@"SETTINGS" withParameters:nil];
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    GameConfiguration *config = [GameConfiguration sharedConfiguration];
    config.controlType = kControlTypeDrag;
    [mySingleton intPref:3 forKey:@"com.firstpixel.formulanova.controlType"];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[SelectCarScene scene] withColor:ccWHITE]];
    
}

-(void) controlAccelerometerCallback:(id)sender
{
    [Flurry logEvent:@"CONTROL_ACCELEROMETER" timed:NO];
    [Flurry endTimedEvent:@"SETTINGS" withParameters:nil];
    
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    GameConfiguration *config = [GameConfiguration sharedConfiguration];
    config.controlType = kControlTypeAccelerometer;
    [mySingleton intPref:4 forKey:@"com.firstpixel.formulanova.controlType"];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[SelectCarScene scene] withColor:ccWHITE]];
    
}
@end
