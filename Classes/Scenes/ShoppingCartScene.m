//
//  ShoppingCartScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 12/19/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import "ShoppingCartScene.h"
#import "MenuScene.h"
#import "GlobalSingleton.h"
#import "MKStoreKit.h"
#import "SoundMenuItem.h"
#import "GameConstants.h"
#import "AppDelegate.h"
#import "RootViewController.h"

@implementation ShoppingCartScene

+(id) scene
{
	CCScene *scene = [CCScene node];
	id child = [ShoppingCartScene node];
	
	[scene addChild:child];
	return scene;
}


- (id) init {
    self = [super init];
    if (self != nil) {
        CGSize s = [[CCDirector sharedDirector] winSize];
		
        CCSprite * bg = [CCSprite spriteWithFile:@"start-game.gif"];
        [bg  setPosition:ccp(s.width/2, s.height/2)];
        [self addChild:bg z:0];



            [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitProductPurchasedNotification
                                                          object:nil
                                                           queue:[[NSOperationQueue alloc] init]
                                                      usingBlock:^(NSNotification *note) {
                                                          NSLog(@"Purchased product with id: %@", [note object]);
                                                          NSLog(@"%@", [[MKStoreKit sharedKit] valueForKey:@"purchaseRecord"]);
                                                          [[GlobalSingleton sharedInstance] unlockItem:@"com.firstpixel.formulanova.banner"];
                                                          //TODO Add this to the response of the buyer
                                                          RootViewController *rootController =(RootViewController *)[[(AppDelegate*)
                                                                                                                     [[UIApplication sharedApplication]delegate] window] rootViewController];
                                                          [(RootViewController *)rootController removeBanner];
                                                          messageLoading = [CCLabelTTF labelWithString:@"Purchased package 1" fontName:@"Arial" fontSize:17];
                                                      }];
        
        
        [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitProductsAvailableNotification
                                                          object:nil
                                                           queue:[[NSOperationQueue alloc] init]
                                                      usingBlock:^(NSNotification *note) {
                                                          
                                                          NSLog(@"Products available: %@  %@", [[MKStoreKit sharedKit] availableProducts], [[MKStoreKit sharedKit] availableProducts][0]);
                                                      }];
        
        
        [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitRestoredPurchasesNotification
                                                          object:nil
                                                           queue:[[NSOperationQueue alloc] init]
                                                      usingBlock:^(NSNotification *note) {
                                                          messageLoading = [CCLabelTTF labelWithString:@"Restored Purchases" fontName:@"Arial" fontSize:17];
                                                          NSLog(@"Restored Purchases");
                                                          [[GlobalSingleton sharedInstance] unlockItem:@"com.firstpixel.formulanova.banner"];
                                                          //TODO Add this to the response of the buyer
                                                          RootViewController *rootController =(RootViewController *)[[(AppDelegate*)
                                                                                                                      [[UIApplication sharedApplication]delegate] window] rootViewController];
                                                          [(RootViewController *)rootController removeBanner];
                                                      }];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitRestoringPurchasesFailedNotification
                                                          object:nil
                                                           queue:[[NSOperationQueue alloc] init]
                                                      usingBlock:^(NSNotification *note) {
                                                          
                                                          NSString* message = [NSString stringWithFormat:@"Failed restoring purchases with error: %@", [note object]];
                                                          messageLoading = [CCLabelTTF labelWithString:message fontName:@"Arial" fontSize:17];
                                                          NSLog(@"Failed restoring purchases with error: %@", [note object]);
                                                      }];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitProductPurchaseFailedNotification
                                                          object:nil
                                                           queue:[[NSOperationQueue alloc] init]
                                                      usingBlock:^(NSNotification *note) {
                                                          NSString* message = [NSString stringWithFormat:@"Failed purchases with error: %@", [note object]];
                                                          messageLoading = [CCLabelTTF labelWithString:message fontName:@"Arial" fontSize:17];
                                                          NSLog(@"Failed purchases with error: %@", [note object]);
                                                      }];
        
        
            [[MKStoreKit sharedKit] startProductRequest];
        
        
        
        
        
        
        
        
        
        SoundMenuItem *backButton = [SoundMenuItem itemFromNormalSpriteFrameName:@"back_up.png" selectedSpriteFrameName:@"back_down.png" target:self selector:@selector(backCallback:)];	
		backButton.position = ccp(5,s.height-5);
		backButton.anchorPoint = ccp(0,1);
        
		CCMenu *topMenu = [CCMenu menuWithItems:backButton, nil];
		topMenu.position = ccp(0,0);
		[self addChild:topMenu z:0];
        

    //------
    
        [CCMenuItemFont setFontSize:20];
        [CCMenuItemFont setFontName:@"HelveticaNeue"];

        CCMenuItemFont *buyPackage1 = [CCMenuItemFont itemWithString:@"BUY PACKAGE 1" target:self selector:@selector(buyPackage1Action)];
        buyPackage1.color = ccBLACK;
        
        CCMenuItemFont *restoreAll = [CCMenuItemFont itemWithString:@"RESTORE ALL" target:self selector:@selector(restoreAllAction)];
        restoreAll.color = ccBLACK;
        
        CCMenu *menu = nil;
        menu = [CCMenu menuWithItems:buyPackage1, restoreAll,  nil];
        [menu alignItemsVerticallyWithPadding: 20];
        [menu setPosition:ccp(s.width/2, s.height/2 - 50)];
        [self addChild:menu];
        
    //------
        
        
		
    }
    return self;
}
-(void)buyPackage1Action{
    [self displayLoading];
    if( ![[MKStoreKit sharedKit] isProductPurchased:@"com.firstpixel.formulanova.full1"]){
        NSLog(@"PURCHASING...");
        [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:@"com.firstpixel.formulanova.full1"];
    }else{
        NSLog(@"RESTORING...");
        [[MKStoreKit sharedKit] restorePurchases];
    }
    
}

-(void)restoreAllAction{
    NSLog(@"RESTORING...");
    [self displayLoading];
    [[MKStoreKit sharedKit] restorePurchases];
    
}


-(void)displayLoading{
    CGSize s = [[CCDirector sharedDirector] winSize];
    messageLoading = [CCLabelTTF labelWithString:@"Loading Store...\n if it takes too long, \ncheck your internet connection." fontName:@"Arial" fontSize:17];// [CCLabelBMFont labelWithString:@"LOADING STORE...\nIf it takes too long,\n check your internet\n connection." fntFile:@"Microgramma.fnt"];
    messageLoading.color = ccRED;
    [messageLoading setPosition:ccp(s.width/2, s.height/2 +30)];
    [self addChild:messageLoading z:2];
}



-(void)backCallback:(id)sender{
    [[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[MenuScene scene]]];
}


- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView: [touch view]];
	location = CGPointMake(location.y, location.x);

}
@end
