//
//  TrackItalyReverse.h
//  Gil Beyruth
//
//  Created by Gil Beyruth on 06/06/11.
//  Copyright 2011 Firstpixel. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


//
// TrackItalyReverse:
//
// Details:
//
// It uses 1 batch node for the sprites
// The box2d objects are rendered using 1 big image (see level7.png)
//
// How to create a similar level ?
//	1. Open Inkscape and create a new document of 480x320. Actually it can be of any size, but it is useful as a reference.
//		-> Inkscape -> File -> Document Properties -> Custom size: width=480, height=320
//	2. Create 1 layer:
//		-> physics:objects
//	3. Design the world
//	4. Once you finish it, duplicate that layer
//		-> Inkscape -> Layer -> Duplicate current Layer
//	5. Rename the new layer to: "graphics"
//	6. Select the "graphics" layer and "paint it".
//	7. Disable the "physics:objects" layer
//		-> Inkscape -> Layer -> Layers -> click on the "eye" of "physics:objects" layer
//	8. Export the image as bitmap
//		-> Inkscape -> File -> Page
//	9. The new exported image (level1.png) will be used as the background image
//
//
// IMPORTANT: gravity and controls are read from the svg file
//

#import "TrackItalyReverse.h"
#import "BodyNode.h"
#import "Box2dDebugDrawNode.h"
#import "GameConfiguration.h"
#import "CCParticleSystem.h"
#import "CCParticleExamples.h"
#import "GlobalSingleton.h"
#import "Flurry.h"
#import "GhostStatus.h"

@implementation TrackItalyReverse
@synthesize skidIndexTag;
@synthesize skidSmokeTag;



-(void) initGraphics
{
    [Flurry logEvent:@"TRACK_ITALY_REVERSE" timed:YES];
    
    
	// sprites    
    CGFloat scale = [[UIScreen mainScreen] scale];
    if (scale > 1.0){
        //iPad retina screen
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud-hd.plist"];
        // weak ref
        spritesBatchNode_ = [CCSpriteBatchNode batchNodeWithFile:@"hud-hd.png" capacity:145];
        [self addChild:spritesBatchNode_ z:10];
    }else{
        //iPad screen
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud.plist"];
        // weak ref
        spritesBatchNode_ = [CCSpriteBatchNode batchNodeWithFile:@"hud.png" capacity:145];
        [self addChild:spritesBatchNode_ z:10];
    }
    
    
     
    // [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA4444];
    
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGB565];
	// The physics world is drawn using 1 big image
    //
    // TIP: The correct postion can be obtained from Inkscape
	//[background setPosition:ccp(-564.00f,-1360.00f)];
    /*
     g6    g7    g8     g9   g10   g11
     //////////////////////////
     g5 //          //          // g12
     //    1     //     2    //
     g4 //          //          // g13
     //////////////////////////
     g3 //          //          // g14
     //    3     //     4    //
     g2 //          //          // g15
     //////////////////////////
     g1   g20   g19    g18   g17   g16
     
     g = grass around the track
     */
    [self setupBatchNode];
    
    // CCSprite *italy1 = [CCSprite spriteWithFile:@"italy1.gif"];
    CCSprite *italy1 = [CCSprite spriteWithSpriteFrameName:@"italy1.gif"];
    [italy1 setPosition:ccp(516.00f,1536.00f)];
    [self addChild:italy1 z:-10];
    
    //CCSprite *italy2 = [CCSprite spriteWithFile:@"italy2.gif"];
    CCSprite *italy2 = [CCSprite spriteWithSpriteFrameName:@"italy2-reverse.gif"];
    [italy2 setPosition:ccp(1540.00f,1536.00f)];
    [self addChild:italy2 z:-10];
    
    
    //CCSprite *italy3 = [CCSprite spriteWithFile:@"italy3.gif"];
    CCSprite *italy3 = [CCSprite spriteWithSpriteFrameName:@"italy3.gif"];
    [italy3 setPosition:ccp(516.00f,512.00f)];
    [self addChild:italy3 z:-10];
    
    //CCSprite *italy4 = [CCSprite spriteWithFile:@"italy4.gif"];
    CCSprite *italy4 = [CCSprite spriteWithSpriteFrameName:@"italy4.gif"];
    [italy4 setPosition:ccp(1540.00f,512.00f)];
    [self addChild:italy4 z:-10];
    
    
    /* GRASS */
    CCSprite *grass1 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass1 setPosition:ccp(-252.0f,-252.0f)];
    [self addChild:grass1 z:-9];
    
    CCSprite *grass2 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass2 setPosition:ccp(-252.0f,256)];
    [self addChild:grass2 z:-9];
    
    CCSprite *grass3 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass3 setPosition:ccp(-252.0f,768.0f)];
    [self addChild:grass3 z:-9];
    
    CCSprite *grass4 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass4 setPosition:ccp(-252.0f,1280)];
    [self addChild:grass4 z:-9];
    
    CCSprite *grass5 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass5 setPosition:ccp(-252.0f,1792)];
    [self addChild:grass5 z:-9];
    
    CCSprite *grass6 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass6 setPosition:ccp(-252.0f,2304)];
    [self addChild:grass6 z:-9];
    
    CCSprite *grass7 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass7 setPosition:ccp(256.0f,2304)];
    [self addChild:grass7 z:-9];
    
    CCSprite *grass8 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass8 setPosition:ccp(768.0f,2304)];
    [self addChild:grass8 z:-9];
    
    CCSprite *grass9 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass9 setPosition:ccp(1280,2304)];
    [self addChild:grass9 z:-9];
    
    CCSprite *grass10 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass10 setPosition:ccp(1792,2304)];
    [self addChild:grass10 z:-9];
    
    CCSprite *grass11 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass11 setPosition:ccp(2304,2304)];
    [self addChild:grass11 z:-9];
    
    //////
    CCSprite *grass12 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass12 setPosition:ccp(2304,1792)];
    [self addChild:grass12 z:-9];
    
    CCSprite *grass13 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass13 setPosition:ccp(2304,1280)];
    [self addChild:grass13 z:-9];
    
    CCSprite *grass14 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass14 setPosition:ccp(2304,768.0f)];
    [self addChild:grass14 z:-9];
    
    CCSprite *grass15 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass15 setPosition:ccp(2304,256.0f)];
    [self addChild:grass15 z:-9];
    
    CCSprite *grass16 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass16 setPosition:ccp(2304,-252.0f)];
    [self addChild:grass16 z:-9];
    
    CCSprite *grass17 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass17 setPosition:ccp(1792,-252.0f)];
    [self addChild:grass17 z:-9];
    
    
    CCSprite *grass18 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass18 setPosition:ccp(1280,-252.0f)];
    [self addChild:grass18 z:-9];
    
    
    CCSprite *grass19 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass19 setPosition:ccp(768.0f,-252.0f)];
    [self addChild:grass19 z:-9];
    
    
    CCSprite *grass20 = [CCSprite spriteWithSpriteFrameName:@"grass.png"];
    [grass20 setPosition:ccp(256.0f,-252.0f)];
    [self addChild:grass20 z:-9];
    
    
        
	// Restore 32-bit texture format
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];
	
	// TIP: Disable this node in release mode
	// Box2dDebug draw in front of background
    if( [[GameConfiguration sharedConfiguration] enableWireframe] ) 
    {
		Box2dDebugDrawNode *b2node = [Box2dDebugDrawNode nodeWithWorld:world_];
		[self addChild:b2node z:30];
	}
	
   // The size of the map is the size of the background image
	[self setContentSize:CGSizeMake(2448.0f,2448.0f)];
    
    [self initSkidMarks];
   // [self initTurboEffect];
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton gameType] == 1){
        [mySingleton setTotalLaps:4];
    }else if([mySingleton gameType] == 2 || [mySingleton gameType] == 3 || [mySingleton gameType] == 4){
        [mySingleton setTotalLaps:3];
    }
    
    
    //GHOST CAR
    if([[GlobalSingleton sharedInstance] gameType] == 3){
        NSMutableArray* ghostInitial = [[[GlobalSingleton sharedInstance] getGhostBestLapForTrack:[[GlobalSingleton sharedInstance] selectedTrackInt]] mutableCopy];
        GhostStatus* initialStatus = [ghostInitial objectAtIndex:0];
        ghostSpriteCar = [CCSprite spriteWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"car%i_gray.png",initialStatus.car]]];
        [self addChild:ghostSpriteCar z:20 tag:929292];
        [ghostSpriteCar setOpacity:80];
    }
    
    sae = [SimpleAudioEngine sharedEngine];
    [sae preloadEffect:@"backgroundLoop2.mp3"];
    backgroundLoop = [[sae soundSourceForFile:@"backgroundLoop2.mp3"] retain];
    backgroundLoop.looping = YES;
    backgroundLoop.gain = 0.6f;
    [backgroundLoop play];
  
}

- (void)setupBatchNode {
    NSString *spritesPvrCcz1 = @"italy1.pvr.ccz";
    NSString *spritesPlist1 = @"italy1.plist"; 
    NSString *spritesPvrCcz2 = @"italy2-reverse.pvr.ccz";
    NSString *spritesPlist2 = @"italy2-reverse.plist"; 
    NSString *spritesPvrCcz3 = @"italy3.pvr.ccz";
    NSString *spritesPlist3 = @"italy3.plist"; 
    NSString *spritesPvrCcz4 = @"italy4.pvr.ccz";
    NSString *spritesPlist4 = @"italy4.plist";
    
    NSString *spritesPvrCcz5 = @"grass-ipadhd.pvr.ccz";
    NSString *spritesPlist5 = @"grass-ipadhd.plist";
    
    /*if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        spritesPvrCcz1 = @"italy1-hd.pvr.ccz";
        spritesPlist1 = @"italy1-hd.plist";
        spritesPvrCcz2 = @"italy2-reverse-hd.pvr.ccz";
        spritesPlist2 = @"italy2-reverse-hd.plist";
        spritesPvrCcz3 = @"italy3-hd.pvr.ccz";
        spritesPlist3 = @"italy3-hd.plist";
        spritesPvrCcz4 = @"italy4-hd.pvr.ccz";
        spritesPlist4 = @"italy4-hd.plist";
    }
    */
    _batchNode1 = [CCSpriteBatchNode batchNodeWithFile:spritesPvrCcz1];
    [self addChild:_batchNode1 z:-1];
    _batchNode2 = [CCSpriteBatchNode batchNodeWithFile:spritesPvrCcz2];
    [self addChild:_batchNode2 z:-1];
    _batchNode3 = [CCSpriteBatchNode batchNodeWithFile:spritesPvrCcz3];
    [self addChild:_batchNode3 z:-1];
    _batchNode4 = [CCSpriteBatchNode batchNodeWithFile:spritesPvrCcz4];
    [self addChild:_batchNode4 z:-1];
    _batchNode5 = [CCSpriteBatchNode batchNodeWithFile:spritesPvrCcz5];
    [self addChild:_batchNode5 z:-1];
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesPlist1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesPlist2];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesPlist3];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesPlist4];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesPlist5];
    
    
    
}

-(void) registerWithTouchDispatcher
{
	// Priorities: lower number, higher priority
	// Joystick: 10
	// GameNode (dragging objects): 50
	// HUD (dragging screen): 100
	[[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:10 swallowsTouches:YES];
}


-(CGRect) contentRect
{
		return CGRectMake(-200, -200, contentSize_.width, contentSize_.height);
}

- (NSString*) SVGFileName
{
	//return @"australia-teste.svg";
    return @"italy-reverse.svg";
}

// This is the default behavior
- (void) addBodyNode:(BodyNode*)node z:(int)zOrder
{
	switch (node.preferredParent) 
    {
		case BN_PREFERRED_PARENT_SPRITES_PNG:
            [spritesBatchNode_ addChild:node z:zOrder];
            break;
		default:
			NSAssert(NO,@"default unsupported");
			break;
	}
}


//EFFECTS
- (void) initSkidMarks
{
    skidIndexTag=0;
    //OLD IPOD TOUCHS COMPABILITY
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    int maxSkidMark;
    if(![[mySingleton deviceType] isEqualToString:@"oldIphone"]){
        maxSkidMark = kMaxSkidmarkIphone6;
    }else{
        maxSkidMark = kMaxSkidmarkIpod;
    }
    while (skidIndexTag<maxSkidMark)
    {
        skidMark = [CCSprite spriteWithFile:@"skidmark.png"];
        [skidMark setPosition:CGPointMake(0,0)];
        [skidMark setScaleY:-1.5f];
        [skidMark setScaleX:1.3f];
        [skidMark setOpacity:32];
        [self addChild:skidMark z:-1 tag:skidIndexTag];
        skidIndexTag++;
    }
    skidSmokeRight = [CCParticleSystemQuad particleWithFile:@"carTireSmoke.plist"];
    [self addChild:skidSmokeRight z:1000];
    skidSmokeLeft = [CCParticleSystemQuad particleWithFile:@"carTireSmoke.plist"];
    [self addChild:skidSmokeLeft z:1001];
}
- (void) addSkidMarkAt:(CGPoint)posi angle:(float)angle
{
    //OLD IPOD TOUCHS COMPABILITY
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    if(![[mySingleton deviceType] isEqualToString:@"oldIphone"]){
        if(skidIndexTag>=kMaxSkidmarkIphone6)skidIndexTag=0;
    }else{
        //OLD IPOD TOUCHS COMPABILITY
        if(skidIndexTag>=kMaxSkidmarkIpod)skidIndexTag=0;
    }
    CCSprite* skid = (CCSprite*)[self getChildByTag:skidIndexTag];
    [skid setPosition:posi];
    [skid setAnchorPoint:ccp(0.5f,0.0f)];
    [skid setRotation:angle];
    [skid setAnchorPoint:ccp(0.5f,0.5f)];
    skidIndexTag++;
}
- (void)addSmokeRightSkidmark:(CGPoint)posi angle:(float)angle
{
    //OLD IPOD TOUCHS COMPABILITY
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    if(![[mySingleton deviceType] isEqualToString:@"oldIphone"] ){
        [skidSmokeRight setPosition:posi];
        if([skidSmokeRight active]==NO)
        {
            [skidSmokeRight setRotation:angle];
            [skidSmokeRight  resetSystem];
        }
    }
}
- (void)addSmokeLeftSkidmark:(CGPoint)posi angle:(float)angle
{
    //OLD IPOD TOUCHS COMPABILITY
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    if(![[mySingleton deviceType] isEqualToString:@"oldIphone"] ){
        [skidSmokeLeft setPosition:posi];
        if([skidSmokeLeft active]==NO)
        {
            [skidSmokeLeft setRotation:angle];
            [skidSmokeLeft  resetSystem];
        }    
    }
}

-(void)initExplosionEffect
{
    [self removeExplosionEffect];
    carExplosion = [CCParticleSystemQuad particleWithFile:@"carExplosion.plist"]; 
    carExplosion.position = [(CCSprite*)hero_ position];
    [self addChild:carExplosion z:100 tag:1004]; 
}
-(void)removeExplosionEffect
{
    [self removeChildByTag:1004 cleanup:YES];
}
-(void)initTurboEffect{
    [self removeTurboEffect];
    carTurbo = [CCParticleSystemQuad particleWithFile:@"carTurbo.plist"]; 
    [self addChild:carTurbo z:1 tag:1005]; 
}
- (void)turboEffectPosition:(CGPoint)posi rotation:(float)angle
{
    [carTurbo setPosition:posi];
    [carTurbo setRotation:angle];
    
    if([carTurbo active]==NO)
    {
        [carTurbo resetSystem];
    }
}
-(void)removeTurboEffect
{
   [self removeChildByTag:1005 cleanup:YES];
}

-(void)initCrashedEffect
{
    [self removeCrashedEffect];
    carCrashedSmoke = [CCParticleSystemQuad particleWithFile:@"carSmoke.plist"]; 
    [self addChild:carCrashedSmoke z:100 tag:1006]; 
    carCrashedGlass = [CCParticleSystemQuad particleWithFile:@"carGlass.plist"]; 
    [carCrashedGlass setRotation:[hero_ rotation]];
    [carCrashedGlass setPosition:[(CCSprite*)hero_ position]];
    [self addChild:carCrashedGlass z:110 tag:1007]; 
    carCrashedSparkles = [CCParticleSystemQuad particleWithFile:@"carHitSparkles.plist"];
    [carCrashedSparkles setRotation:[hero_ rotation]];
    [carCrashedSparkles setPosition:[(CCSprite*)hero_ position]];
    [self addChild:carCrashedSparkles z:120 tag:1008]; 
    
}
- (void)crashedEffectPosition:(CGPoint)posi rotation:(float)angle
{
    [carCrashedSmoke setPosition:posi];
    [carCrashedSmoke setRotation:angle];
    if([carCrashedSmoke active]==NO)
    {
        [carCrashedSmoke  resetSystem];
    }
}
-(void)removeCrashedEffect
{
    [self removeChildByTag:1006 cleanup:YES];
    [self removeChildByTag:1007 cleanup:YES];
    [self removeChildByTag:1008 cleanup:YES];
}


- (void) dealloc
{
    [sae stopBackgroundMusic];
    [backgroundLoop release];
	[SimpleAudioEngine end];
	sae = nil;
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
	[super dealloc];
}

@end