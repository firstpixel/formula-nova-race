//
//  TrackUSA.h
//  Gil Beyruth
//
//  Created by Gil Beyruth on 06/06/11.
//  Copyright 2011 Firstpixel. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import "GameNode.h"
#import "SimpleAudioEngine.h"


@interface TrackUSA : GameNode
{
	CCSpriteBatchNode *spritesBatchNode_;
    CCSprite *skidMark;
    CCParticleSystemQuad *skidSmokeRight;
    CCParticleSystemQuad *skidSmokeLeft;
    CCParticleSystemQuad *carTurbo;
    CCParticleSystemQuad *carCrashedSmoke;
    CCParticleSystemQuad *carExplosion;
    CCParticleSystemQuad *carCrashedGlass;
    CCParticleSystemQuad *carCrashedSparkles;
    int skidIndexTag;
    int skidSmokeTag;
    
    SimpleAudioEngine *sae;
    CDSoundSource *backgroundLoop;
    
    CCSpriteBatchNode *_batchNode1;
    CCSpriteBatchNode *_batchNode2;
    CCSpriteBatchNode *_batchNode3;
    CCSpriteBatchNode *_batchNode4;
    CCSpriteBatchNode *_batchNode5;
}


- (void)setupBatchNode;
- (void) initSkidMarks;
- (void) addSkidMarkAt:(CGPoint)posi angle:(float)angle;
- (void) addSmokeLeftSkidmark:(CGPoint)posi angle:(float)angle;
- (void) addSmokeRightSkidmark:(CGPoint)posi angle:(float)angle;

- (void)initTurboEffect;
- (void)turboEffectPosition:(CGPoint)posi rotation:(float)angle;
- (void)removeTurboEffect;

- (void)initCrashedEffect;
- (void)crashedEffectPosition:(CGPoint)posi rotation:(float)angle;
- (void)removeCrashedEffect;

- (void)initExplosionEffect;
- (void)removeExplosionEffect;


@property (nonatomic, readwrite) int skidIndexTag;
@property (nonatomic, readwrite) int skidSmokeTag;
@end
